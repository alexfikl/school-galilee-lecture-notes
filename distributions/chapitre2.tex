% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Distributions}

\section{Définition}

\begin{notation}
Soit $T$ une forme linéaire définie par:
\[
\deffunction{T}{\mathcal{C}^\infty_0(\Omega)}{\mathbb{C}}{\phi}{\dip{T, \phi}}
\]
\end{notation}

\begin{definition}[Définition d'une distribution]\label{def:dist}
On appelle ``distribution'' sur $\Omega$ toute forme linéaire sur $\mathcal{C}^\infty_0(\Omega)$
vérifiant: $\forall K \subset \Omega$ compact de $\mathbb{R}^d, \exists k \in \mathbb{N}, c > 0$
telle que pour toute fonction $\phi \in \mathcal{C}^\infty_0(\Omega)$ avec $\supp{\phi}
\subset K$,
on a:
\begin{eqnarray} \label{eq:dist}
|\dip{T, \phi}| \leq c \sum_{|\alpha| \leq k} \sup_{x \in \mathbb{R}^d}
    |\pd*{^\alpha\phi}{x}|
\end{eqnarray}
\end{definition}

\begin{remark}
La condition~\eqref{eq:dist} signifie que $T$ est ``continue'' sur
$\mathcal{C}^\infty_0(\Omega)$.
\end{remark}

\begin{proposition}
Soit $T: \mathcal{C}^\infty_0(\Omega) \to \mathbb{C}$ une forme linéaire. Il y a équivalence entre:
\begin{enumerate}[(i)]
    \item $T$ est une distribution.
    \item $\forall \phi_n \in \mathcal{C}^\infty_0(\Omega)$ vérifiant:
    \begin{enumerate}[a)]
        \item  $\exists K \subset \Omega$ compact de $\mathbb{R}^d$, indépendant de $n,
        \forall n \in \mathbb{N}$ tel que $\supp{\phi_n} \subset K$.
        \item $\pd*{^\alpha \phi_n}{x} \to 0$ uniformément $\forall \alpha \in \mathbb{N}^d$.
    \end{enumerate}
    on a:
    \[
    \dip{T, \phi_n} \xrightarrow{n \to \infty} 0
    \]
\end{enumerate}
\end{proposition}

\begin{notation}
$\dist$ est l'espace de distributions sur $\Omega$.
\end{notation}

\begin{remark}
Dans la définition~\eqref{eq:dist}, l'entier $k$ dépend du compact $K$.
\end{remark}

\begin{definition}[Distribution d'ordre $k$]\label{def:distk}
On dit qu'une forme linéaire sur $\mathcal{C}^\infty_0(\Omega)$ est une distribution d'ordre
$\leq k \in \mathbb{N}$ si et seulement si $\forall K \subset \Omega$ compact,
$\exists c > 0$ et $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$ avec $\supp{\phi} \subset
K$, on a:
\begin{eqnarray}\label{eq:distk}
|\dip{T, \phi}| \leq c \sum_{|\alpha| \leq k} \sup_{x \in \mathbb{R}^d} |\pd*{^\alpha \phi}{x}|
\end{eqnarray}
\end{definition}

\begin{remark}
Ici $k$ est indépendante du compact $K$.
\end{remark}

\begin{notation}
$\distk$ est l'espace de distributions d'ordre au plus $k$. On a $\distk
\subset \dist$.
\end{notation}

\begin{notation}
On pose $\distf = \bigcup \distk$ l'espace de distributions d'ordre fini.
\end{notation}

On dit que $T$ est une distribution d'ordre $k$ si $T \in \distk$ et $T
\notin \distk[k - 1]$.

\section{Interprétation sur \texorpdfstring{$\mathcal{C}^k_0(\Omega)$}{Ck0(Omega)}}

\begin{definition}
Une forme linéaire continue sur $\mathcal{C}^k_0(\Omega)$ est une application linéaire:
\[
T: \mathcal{C}^k_0(\Omega) \to \mathbb{C}
\]
telle que $\forall K \subset \Omega$ compact, $\exists c > 0, \forall \phi
\in \mathcal{C}^k_0(\Omega)$ avec $\supp{\phi} \subset K$, on a:
\begin{eqnarray}\label{eq:distck}
|\dip{T, \phi}| \leq c \sum_{|\alpha| \leq k} \sup_{x \in \mathbb{R}^d} |\pd*{^\alpha \phi}{x}|
\end{eqnarray}
\end{definition}

On voit que dans la définition~\ref{def:distk} on suppose que $\phi \in
\mathcal{C}^\infty_0(\Omega)$ avec $k$ fixée par l'ordre de la distribution mais ici on
suppose que $\phi \in \mathcal{C}^k_0(\Omega)$ et $k$ est fixée par l'espace.

\begin{notation}
$(\mathcal{C}^k_0(\Omega))'$ est l'espace de formes linéaires de $\mathcal{C}^k_0(\Omega)$.
\end{notation}

Soit $\tilde{T} \in (\mathcal{C}^k_0(\Omega))'$ vérifiant~\eqref{eq:distck}. Comme $\mathcal{C}^\infty_0(\Omega)$
est un sous-espace de $\mathcal{C}^k_0(\Omega)$ on peut considérer la restriction de $\tilde{T}$
à $\mathcal{C}^k_0(\Omega)$:
\[
\tilde{T}|_{\mathcal{C}^\infty_0(\Omega)} = T: \mathcal{C}^\infty_0(\Omega) \longrightarrow \mathbb{C} \in \distk
\]

\eqref{eq:distck} est vérifiée par $\phi \in \mathcal{C}^\infty_0(\Omega)$ donc $T \in \distk$.
On a donc une application:
\[
\deffunction{\Phi}{(\mathcal{C}^k_0(\Omega))'}{\distk}{\tilde{T}}{T = \tilde{T}|_{\mathcal{C}^\infty_0(\Omega)}}
\]

\begin{theorem}
L'application $\Phi$ est bijective.
\end{theorem}

\begin{proof}
Il suffit de voir que si $\tilde{T} \in (\mathcal{C}^k_0(\Omega))$ est telle que
$T = \tilde{T}|_{\mathcal{C}^k_0(\Omega)} = 0$ alors $\tilde{T} = 0$.
\end{proof}

\begin{lemma}
Soit $\phi \in \mathcal{C}^k_0(\Omega)$. Il existe une suite $(\phi_n) \in \mathcal{C}^\infty_0(\Omega)$
telle que:
\begin{enumerate}[a)]
    \item $\exists K \subset \Omega$ compact, indépendant de $n$, avec
    $\supp{\phi_n} \subset K$.
    \item $\pd*{^\alpha (\phi_n - \phi)}{x} \to 0$ uniformément sur $\Omega, \forall
    \alpha$ avec $|\alpha| \leq k$.
\end{enumerate}
\end{lemma}

\begin{remark}
Désormais, on identifiera $\distk$ et $(\mathcal{C}^k_0(\Omega))'$.
\end{remark}

\section{Exemples de distributions}

\subsection{Fonctions localement intégrables}

Soit $f \in \lploc{1}$. On définit $T_{f}: \mathcal{C}^\infty_0(\Omega) \to \mathbb{C}$ par:
\[
\dip{T_f, \phi} = \int_{\Omega} f(x) \phi(x) \dx
\]

Si $K = \supp{\phi} \subset \Omega$ on a $\phi = \phi \ind{K}$. Donc
\[
\begin{aligned}
\dip{T_f, \phi}     & =    & \int_{\Omega} f \phi(x) \ind{K} \dx \\
|\dip{T_f, \phi}|   & \leq & \int_{\Omega} |f| |\phi(x)| \ind{K} \dx \\
                & \leq & c_K \sup_{x} |\phi(x)|
\end{aligned}
\]
Où:
\[
c_K = \int_{\Omega} |f| \ind{K} \dx.
\]

Donc $ T_{f} \in \distk[0]$ et on a $\forall K \subset \Omega$ compact,
$\exists c_K$ et $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$  avec $\supp{\phi} \subset K$:
\[
|\dip{T_f, \phi}| \leq c_K \sup_{x} |\phi(x)|
\]

\begin{property}
L'application:
\[
\deffunction{\Phi}{\lploc{1}}{\dist[0]}{f}{T_f}
\]
est injective. On peut donc considérer $ \lploc{1}$ comme un sous-espace
de $\dist$.
\end{property}

\begin{proof}
Soit $f \in \lploc{1}$ avec $T_f = 0$. On veut montrer que $f = 0$ presque
partout. Or:
\[
\begin{aligned}
T_f = 0 & ~\Leftrightarrow\! &
    \forall \phi \in \mathcal{C}^\infty_0(\Omega), & \dip{T_f, \phi} = 0 \\
& ~\Leftrightarrow\! &
    \forall \phi \in \mathcal{C}^\infty_0(\Omega), & \int_{\Omega} f(x) \phi(x) \dx = 0.
\end{aligned}
\]

Soit $\rho \in \mathcal{C}^\infty_0(\mathcal{B}(0, 1))$, $\rho \geq 0, \int \rho(x)
\dx = 1$. Posons:
\[
\rho_{\epsilon} = \frac{1}{\epsilon^d} \rho\left(\frac{x}{\epsilon}\right)
\]

Soit $\theta \in \mathcal{C}^\infty_0(\Omega)$
\[
\rho_{\epsilon} * (\theta f)(x) = \int \rho_{\epsilon}(x - y) \theta(y) f(y) \dx[y]
\]
or $y \mapsto \rho_{\epsilon} (x - y) \theta(y) = \phi(y)$ dans
$\mathcal{C}^\infty_0(\Omega)$ à $x$ fixé. Donc:
\[
\int f(y) \phi(y) \dx = 0
\]

Donc, $\forall x $ fixé, $\rho_{\epsilon} * (\theta f)(x) = 0$. Or $\forall
\theta f \in L^1(\mathbb{R}^d)$ et on sait alors que:
\[
\|\rho_{\epsilon} (\theta f)(x) - (\theta f)(x)\|_{L^1} \xrightarrow{\epsilon \to 0} 0
\]

Donc $(\theta f) = 0, \forall \theta \in \mathcal{C}^\infty_0(\Omega)$. Donc $f = 0$ presque
partout.
\end{proof}

\subsection{Masse de Dirac en un point}

Soient $\Omega$ un ouvert de $\mathbb{R}^d , x_0 \in \Omega$. $\forall \phi \in
\mathcal{C}^\infty_0(\Omega)$, on définit:
\[
\deffunction{\dirac[x_0]}{\mathcal{C}^\infty_0(\Omega)}{\mathbb{C}}{\dip{\dirac[x_0], \phi}}{\phi(x_0)}
\]

\begin{property}
$\dirac[x_0] $ est dans $\dist$.
\end{property}

\begin{proof}
Supposons $x_0 = 0$ (on se ramène à ce cas par translation). Alors:
\[
|\dip{\dirac, \phi}| = |\phi(0)| \leq \sup_{x} |\phi(x)|
\]
Donc la définition de $\distk[0]$ est vérifiée par $\dirac$.
\end{proof}

\begin{remark}
Il n'existe pas de $f \in \lploc{1}$ telle que $\dirac = T_f$.
\end{remark}

\begin{proof}
Sinon, on aurait:
\[
\forall \phi \in \mathcal{C}^\infty_0(\Omega),\quad
\dip{\dirac, \phi} = \dip{T_f, \phi}
\]

Soit:
\[
\phi(0) = \int_{\Omega} f(x) \phi(x) \dx,\quad \forall \phi \in \mathcal{C}^\infty_0(\Omega)
\]

Soit $\theta \in  \mathcal{C}^\infty_0(\mathcal{B}(0, 1)), \theta(0) = 1$.
Prenons pour $\epsilon \in ]0, 1[, \theta(x) = \theta(\frac{x}{\epsilon})$ on a:
\[
\begin{aligned}
1 & =
\int_{\Omega} f(x) \theta(\frac{x}{\epsilon}) \dx  \\
& \leq
    \sup{|\theta|} \underbrace{
        \int_{\Omega} f(x) \ind{\{ x < \epsilon \}} \dx
    }_{\xrightarrow{\epsilon \to 0} 0}
\end{aligned}
\]

Absurde par le~\Cref{thm:tcd}.
\end{proof}

\section{Distributions positives ou nulles}

\begin{definition}
Une distribution positive ou nulle sur $\Omega$ est une distribution
$T \in \dist$ telle que $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$, $ \phi \geq 0 $ on
a $\dip{T, \phi} \in \mathbb{R}_+$.
\end{definition}

$T = \dirac[x_0] $ est une distribution positive ou nulle.

\begin{property}
Toute distribution positive ou nulle est dans $\distk[0]$.
\end{property}

\begin{proof}
Soit $K \subset \Omega$ un compact. Soit $\theta \in \mathcal{C}^k_0(\Omega), \theta \geq 0$,
$\theta \equiv 1$ sur $K$ et $\theta \in [0, 1], \forall x$.

Soit $\phi \in \mathcal{C}^k_0(\Omega)$ à valeurs réelles avec $\supp{\phi} \subset K$. Posons
$M_\phi = \sup |\phi(x)|$.

On a $M_\phi \geq \phi(x), \forall x \in \Omega$, d'où $M_\phi \theta(x) \geq
\phi(x) \theta(x) = \phi(x)$ car $\theta \equiv 1$ sur $\supp{\phi}$. Donc
$M_\phi \theta(x) - \phi(x) \geq 0$. Comme $M_\phi \geq -\phi(x)$, on a aussi
$M_\phi \theta(x) + \phi(x) \geq 0$. Comme $T$ est positive ou nulle:
\[
\begin{aligned}
\dip{T, M_\phi \theta(x) - \phi(x)} & \geq 0 \\
\dip{T, M_\phi \theta(x) + \phi(x)} & \geq 0
\end{aligned}
\]

Donc:
\[
\begin{aligned}
M_\phi \dip{T, \theta} & \geq \dip{T, \phi} \\
M_\phi \dip{T, \theta} & \geq -\dip{T, \phi}
\end{aligned}
\]

Donc $|\dip{T, \phi}| \leq \dip{T, \theta} \sup{|\phi(x)|}$. Donc $\forall K \subset
\Omega$ compact, $\exists c > 0$ et $\forall \phi \in \mathcal{C}^k_0(\Omega)$ avec $\supp{\phi}
\subset K$ on a: $|\dip{T, \phi}| \leq c \sup{|\phi(x)|}$ où $c = \dip{T, \theta}$
lorsque $\phi$ est à valeurs réelles.

Si $\phi$ est à valeurs complexes, on écrit $\phi = \phi_1 + i \phi_2$ avec
$\phi_1 = \Re{\phi}$, $\phi_2 = \Im{\phi}$ et:
\[
\begin{aligned}
|\dip{T, \phi}| & =     |\dip{T, \phi_1} + i \dip{T, \phi_2}| \\
            & \leq  |\dip{T, \phi_1}| + |\dip{T, \phi_2}| \\
            & \leq  c (\sup{|\phi_1|} + \sup{|\phi_2|}) \\
            & \leq  2c \sup{|\phi|}
\end{aligned}
\]

Donc $T \in \distk[0]$.

\begin{remark}
On n'a pas utilisé l'hypothèse que $T \in \dist$. La propriété est vraie si
on suppose que $T$ est une forme linéaire $T : \mathcal{C}^k_0(\Omega) \to \mathbb{C}$ telle que
$\phi \leq 0 \implies \dip{T, \phi} \leq 0$.
\end{remark}
\end{proof}

\subsection{Dérivée de la masse de Dirac}

Soient $\Omega $ ouvert de $\mathbb{R}^d, x_0 \in \Omega, \alpha \in \mathbb{N}^d$.

\begin{definition}
On appelle dérivée d'ordre $\alpha$ de la masse de Dirac en $x_0$ et on
note:
\[
\dirac[x_0]^{(\alpha)} = \pd{^\alpha}{x^\alpha} \dirac[x_0]
\]
la forme linéaire sur $\mathcal{C}^\infty_0(\Omega)$ donnée pour tout $\phi \in \mathcal{C}^\infty_0(\Omega)$
par:
\[
\dip{\dirac[x_0]^{(\alpha)}, \phi} = (-1)^{|\alpha|} \phi^{(\alpha)}(x_0)
\]
\end{definition}

\begin{property}
$\dirac[x_0]^{(\alpha)}$ est une distribution d'ordre $|\alpha|$.
\end{property}

\begin{proof}
On suppose que $x_0 = 0$. Montrons que $\dirac^{(\alpha)} \in \distk[|\alpha|]$:
\[
|\dip{\dirac^{(\alpha)}, \phi}|
    = \left|\pd{^\alpha \phi}{x^\alpha}(0)\right|
    \leq \sup_x {\left|\pd{^\alpha \phi}{x^\alpha}(x)\right|}
    \leq \sum_{|\beta| \leq |\alpha|} \sup_x \left|\pd{^\beta \phi}{x^\beta}(x)\right|
\]

Donc $\dirac^{(\alpha)} \in \distk[|\alpha|]$. Il reste à voir que
$\dirac^{(\alpha)}$ n'est pas d'ordre strictement plus petit que $|\alpha|$.

Supposons que $\dirac^{(\alpha)} \in \distk[|\alpha| - 1]$. Alors $\forall K
\subset \Omega$ compact, $\exists c > 0$ et $\phi \in \mathcal{C}^\infty_0(\Omega)$ avec
$\supp{\phi} \subset K$:
\begin{eqnarray}\label{eq:dirac1}
|\dip{\dirac^{(\alpha)}, \phi}| \leq c \sum_{|\beta| \leq |\alpha| - 1}
    \sup_x \left|\pd{^\beta \phi}{x^\beta}\right|
\end{eqnarray}

Prenons $K$ avec $0 \in \mathring{K}, \theta \in \mathcal{C}^\infty_0(\mathcal{B}(0,
1))$. Alors si $\phi_\lambda = \theta(\lambda x)$ avec $\lambda > 1$. On a:
\[
\sup \phi_\lambda \subset \mathcal{B}\left(0, \frac{1}{\lambda}\right)
\]
pour $\lambda$ assez grand. On peut appliquer~\eqref{eq:dirac1} à $\phi_\lambda$.
On aura:
\[
\left|\pd{^\alpha \phi_\lambda}{x^\alpha}(0)\right|
    \leq c \sum_{|\beta| \leq |\alpha| - 1}
    \sup_x \left|\pd{^\beta \phi_\lambda}{x^\beta}(x)\right|
\]

Si on remplace $\phi_\lambda(x)$ par $\theta(\lambda x)$ on obtient:
\[
\lambda^{|\alpha|} \left|\pd{^\alpha \theta}{x^\alpha}(0)\right|
\leq \sum_{|\beta| \leq |\alpha| - 1}
    \sup_x \left|\pd{^\beta \theta}{x^\beta}(x)\right|
    \leq c' \lambda^{|\alpha| - 1}
\]
lorsque $\lambda$ est assez grand. Donc:
\[
\left|\pd{^\alpha \theta}{x^\alpha}(0)\right|
    \leq c' \lambda^{-1} \xrightarrow{\lambda \to \infty} 0
\]

Donc $\pd*{^\alpha \theta}{x}(0) = 0$. Or $\theta \in \mathcal{C}^\infty_0(\mathcal{B}(0, 1))$
est arbitraire.

On peut la choisir telle que $\pd*{^\alpha \theta}{x}(0) \neq 0$. Donc on a une
contradiction si on suppose $\dirac^{(\alpha)} \in \distk[|\alpha| - 1]$.

Donc $\dirac^{(\alpha)} \in \distk[|\alpha|] \setminus \distk[|\alpha| - 1]$.
\end{proof}

\subsection{Valeur principale de \texorpdfstring{$\frac{1}{x}$}{1/x}}

\begin{definition}
Pour toute fonction $\phi \in \mathcal{C}^\infty_0(\Omega)$ la limite :
\[
\lim_{\epsilon \to 0} \int_{|x| > \epsilon} \frac{\phi(x)}{x} \dx
\]
existe.

On définit une forme linéaire sur $\mathcal{C}^\infty_0(\Omega)$, appelée
\textbf{valeur principale} de $\frac{1}{x}$, et noté $\vp$ par:
\[
\forall \phi \in \mathcal{C}^\infty_0(\Omega),\quad \dip{\vp, \phi} =
    \lim_{\epsilon \to 0} \int_{|x| > \epsilon} \frac{\phi(x)}{x} \dx
\]

Alors $\vp$ est une distribution d'ordre $1$.
\end{definition}

\begin{proof}
Soit $M > 0$ et soit $\phi \in \mathcal{C}^\infty_0(\mathbb{R}), \supp{\phi} \subset [-M, M]$.

Par la~\Cref{thm:taylor} on a:
\[
\phi(x) = \phi(0) + \int_0^x \phi'(y) \dx[y].
\]

Par un changement de variable $y = xz$, on a:
\[
\phi(x) = \phi(0) + x \int_0^1 \phi'(xz) \dx[z] = \phi(0) + x \psi(x).
\]

Donc:
\[
\begin{aligned}
\int_{|x| < \epsilon} \frac{\phi(x)}{x} \dx & =
    \int_{\epsilon < |x| < M} \frac{\phi(0) + x \psi}{x} \dx \\
    & = \phi(0) \int_{\epsilon < |x| < M} \frac{1}{x} \dx +
        \int_{\epsilon < |x| < M} \psi(x) \dx
\end{aligned}
\]

Or $\sfrac{1}{x}$ est une fonction impaire intégrée sur un intervalle
symétrique, donc:
\[
\int_{\epsilon < |x| < M} \frac{1}{x} \dx = 0
\]

Pour la deuxième intégrale, en utilisant le~\Cref{thm:tcd} on obtient:
\[
\int_{\epsilon < |x| < M} \psi(x) \dx = \int_{|x| < M} \psi(x) \dx
\]

Donc $\vp$ existe et vaut:
\[
\vp = \int_{|x| < M} \psi(x) \dx
\]
lorsque $\supp{\phi} \subset [-M, M]$. On aura:
\[
\begin{aligned}
\left|\dip{\vp, \phi}\right| & = \left|\int_{|x| < M} \psi(x) \dx\right| \\
& \leq \sup_{[-M, M]} |\psi(x)| \int_{-M}^M \dx \\
& \leq 2M \sup_{y \in \mathbb{R}} |\phi'(y)|
\end{aligned}
\]

Donc $\vp \in \distk[1][\mathbb{R}]$.
\end{proof}

\subsection{Partie finie de
    \texorpdfstring{$x_t^\alpha$}{xta} lorsque
    \texorpdfstring{$\alpha \in [-2,-1]$}{a dans [-2, -1]}}

\begin{definition}
Soit $\alpha \in ]-2,-1[$. On définit la \textbf{partie finie} de $x^{\alpha},
\forall \phi \in \mathcal{C}^\infty_0(\mathbb{R})$, par:
\[
\dip{\pfa, \phi} = -\int_0^\infty \phi'(x) \frac{x^{\alpha + 1}}{\alpha + 1} \dx
\]

Alors $\pf$ est une distribution d'ordre $1$. Si $\phi \in \mathcal{C}^\infty_0(\mathbb{R})$
vérifie $\phi(0) = 0$, on a:
\[
\dip{\pf, \phi} = \int_0^\infty \phi'(x) x^{\alpha} \dx
\]
\end{definition}

\begin{proof}
Soit $M > 0$ et $\phi$ avec $\supp{\phi} \subset [-M, M]$.
\[
\begin{aligned}
|\dip{\pfa, \phi}| & \leq
    \int_0^M |\phi'(x)| \frac{x^{\alpha + 1}}{\alpha + 1} \dx \\
& \leq
    \sup |\phi'| \underbrace{\int_0^M \frac{x^{\alpha + 1}}{\alpha + 1} \dx}_{ = c_M < \infty \text{ car } \alpha + 1 > -1}
\end{aligned}
\]

Donc $\pfa \in \distk[1][\mathbb{R}]$.

Si $\phi(0) = 0, \phi(x) = x \psi(x)$ avec $\psi \in \mathcal{C}^\infty_0(\mathbb{R})$ donc:
\[
\int_0^\infty x^\alpha \phi(x) \dx = \int_0^\infty x^{\alpha + 1} \psi(x) \dx
    = \lim_{\epsilon \to 0_+} \underbrace{\int_\epsilon^\infty \psi(x)
    x^{\alpha + 1} \dx}_{ = I_\epsilon}
\]
qui converge car $\alpha + 1 > -1$.

Si on applique l'intégration par partie à $I_\epsilon$ on obtient:
\[
I_\epsilon =
    \underbrace{\frac{-\phi(\epsilon) \epsilon^{\alpha + 1}}{\alpha + 1}}_{\xrightarrow{\epsilon \to 0} 0} -
    \underbrace{\int_\epsilon^\infty \phi'(x) \frac{x^{\alpha + 1}}{\alpha + 1} \dx}_{\xrightarrow{\epsilon \to 0} <\pfa, \phi>}
\]
\end{proof}

\section{Support des distributions}

\begin{reminder}
Soit $f \in \mathcal{C}^0(\Omega)$. $x_0 \in \Omega, x_0$ n'est pas dans le $\supp{f}$
si et seulement si $\exists \omega$ voisinage de $x_0, \omega \subset \Omega$,
tel que $f|_{\omega} = 0$.
\end{reminder}

\begin{definition}
Soient $\omega \in \Omega$ un ouvert et $T \in \dist$. On définit
$T|_{\omega}: \mathcal{C}^\infty_0(\Omega) \to \mathbb{C}$ par:
\[
\dip{T|_{\omega}, \phi} = \dip{T, \phi}, \forall \phi \in  \mathcal{C}^\infty_0(\omega)
\]
\end{definition}

Alors $T|_{\omega} \in \mathcal{D}'(\omega)$. On dit que c'est la restriction
de $T$ à l'ouvert $\omega$.

On dit que $T \in \dist$ est nulle sur l'ouvert $\omega \in \Omega$ (c'est-à-dire
$T|_{\omega} = 0)$ si et seulement si $\dip{T, \phi} = 0, \forall \phi \in
\mathcal{C}^\infty_0(\omega)$.

\begin{lemma}
Soit $(\omega_i)_{i \in I}$ une suite d'ouverts tels que $\bigcup \omega_i =
\Omega$. Soit $T \in \dist$ telle que $T|_{\omega_{i}} = 0, \forall i \in
I$.

Alors $T = 0$.
\end{lemma}

\begin{proof}
Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$. Soit $\supp{\phi} \subset K \subset \Omega$.
$(\omega_i)$ est un recouvrement ouvert de $K$ (i.e. il existe $J \subset I$
fini tel que $K \subset \bigcup_J \omega_i$).

On sait qu'il existe $\theta_i \in \mathcal{C}^\infty_0(\omega_i)$ pour toute $i \in J$
tels que $\sum \theta_i(x) = 1$. Donc, comme $\supp{\phi} \subset K$,
$\sum \theta_i(x)\phi(x) = \phi(x),\ \forall x \in \Omega$.

Posons $\phi_i(x) = \theta_i(x)\phi(x) \in \mathcal{C}^\infty_0(\omega_i)$. Donc:
\[
\dip{T, \phi} = \sum_{i \in J} \dip{T, \phi_j} = 0,\quad \forall \phi \in \mathcal{C}^\infty_0(\Omega)
\]
donc $T = 0$.
\end{proof}

\begin{definition}
Soit $T \in \dist$. Soit $\omega$ le plus grand ouvert de $\Omega$ sur lequel
$T$ est nulle. Le support de $T$ est par définition $\supp{T} = \Omega \setminus
\omega$.
\end{definition}

\begin{remark}
~\begin{itemize}
    \item $\supp{T} = \displaystyle \Omega \bigcap (\mathbb{R}^d \setminus \omega)$, donc
    $\supp{T}$ est un fermé de $\Omega$ (car $\mathbb{R}^d \setminus \omega$ est un
    fermé de $\mathbb{R}^d$).

    \item Soit $x_0 \in \Omega$. Alors $x_0 \notin \supp{T}$ si et seulement s'il
    existe $V$ voisinage ouvert de $x_0$, $V \in \Omega$ tel que $T|_{V} = 0
    \Leftrightarrow \exists V$ voisinage ouvert de $x_0, V \in \Omega$ tel que:
    \[
    \forall \phi \in  \mathcal{C}^\infty_0(V),\quad \dip{T, \phi} = 0.
    \]

    \item Si $F$ est un fermé de $\Omega$, on a:
    \[
    \supp{T} \subset F \Leftrightarrow T|_{\Omega \setminus F} = 0.
    \]

    \item Si $f \in \mathcal{C}^\infty_0(\Omega), \supp{T_f} = \supp{f}$.
\end{itemize}
\end{remark}

\begin{example}
Soit $x_0 \in \mathbb{R}^d, \alpha \in \mathbb{N}^d$. Alors $\supp{\dirac[x_0]^{(\alpha)}} =
\{ x_0 \}$.

On doit voir que $\forall y_0 \neq x_0$, il existe $V$ voisinage ouvert de
$y_0$ avec $\dirac[x_0]^{(\alpha)}|_{V} = 0$. Si $V = \mathcal{B}(y_0,
\sfrac{r}{2})$ avec $r = |x_0 - y_0|, \forall \phi \in \mathcal{C}^\infty_0(V)$ et
$\phi^{(\alpha)}(x_0) = 0$ donc $\dip{\dirac[x_0]^{(\alpha)}, \phi} = 0$.

On doit voir que $\supp{\dirac[x_0]^{(\alpha)}} \neq 0$. Par la définition du
support: $\supp{T} = \emptyset \Leftrightarrow T \equiv 0$.

Il suffit de voir que $\forall \alpha \in \mathbb{N}^d, \dirac[x_0]^{(\alpha)} \neq 0$.

Posons $x_0 = 0$. Soit $\phi(x) = \theta(x) x^\alpha$ avec $\theta \in
\mathcal{C}^\infty_0(\mathbb{R}^d)$ et $\theta \equiv 1$ près de $0$. $\pd*{^\alpha \phi}{x} =
\theta(x) (\pd*{^\alpha x^\alpha}{x}) = \alpha! \theta(x)$ sur le voisinage sur lequel
$\theta \equiv 1$. Donc:
\[
\dip{\dirac^{|\alpha|}, \phi} = (-1)^{|\alpha|} \alpha! \neq 0.
\]
\end{example}

\begin{property}
Soit $T \in \dist, x_0 \in \Omega$. Supposons $\supp{T} \subset \{ x_0 \}$. Il
existe $k \in \mathbb{N}$ et des constantes $ a_{\alpha} \in \mathbb{C}$ pour tout $\alpha \in
\mathbb{N}^d$, tel que $|\alpha| \leq k$, tels que:
\[
T = \sum_{|\alpha| \leq k} a_{\alpha} \dirac[x_0]^{\alpha}
\]
\end{property}

\section{Distributions à support compact}

\begin{definition}
Une distribution $T \in \dist$ est à support compact si et seulement si
$\supp{T}$ est un compact inclus dans $\Omega$.
\end{definition}

\begin{notation}
On note $\distc$ l'espace de distributions à support compact.
\end{notation}

\begin{definition}
On note $(\mathcal{C}^\infty(\Omega))'$ l'espace des formes linéaire $T: \mathcal{C}^\infty(\Omega) \to \mathbb{C}$
qui sont continues lorsque $\mathcal{C}^\infty(\Omega)$ est muni de sa topologie naturelle.
\end{definition}

\begin{property}\label{propr:support}
$T \in (\mathcal{C}^\infty(\Omega))'$ si et seulement si $T$ est une forme linéaire sur
$\mathcal{C}^\infty(\Omega)$ telle que $\exists K \subset \Omega$ compact de $\mathbb{R}^d$, $\exists
k \in \mathbb{R}, \exists c > 0, \forall \phi \in \mathcal{C}^\infty(\Omega)$:
\begin{eqnarray}\label{eq:support}
|\dip{T, \phi}| \leq c \sum_{|\alpha| \leq k}
    \sup_{x \in K} \left|\pd{^\alpha \phi}{x^\alpha}(x)\right|
\end{eqnarray}
\end{property}

\begin{proof}
$\mathcal{C}^\infty(\Omega)$ est muni de la topologie associée aux semi-normes:
\[
p_n(\phi) = \sum_{|\alpha| \leq n}
    \sup_{x \in K_n} \left|\pd{^\alpha \phi}{x^\alpha}(x)\right|
\]
(où $(K_n)_n$ est une suite exhaustive de compacts définie
dans~\ref{def:suitekn}).

Alors, $V$ est une voisinage de $0$ si et seulement si $\exists r > 0, n \in
\mathbb{N}^*$ tels que $\{\phi \mid p_n(\phi) < r\} \subset V$.

Donc $T: \mathcal{C}^\infty(\Omega) \to \mathbb{C}$ est continue (car $T$ est linéaire).

\hspace{0.9cm} $\Leftrightarrow T$ est continue en $0$.

\hspace{0.9cm} $\Leftrightarrow \forall \epsilon > 0, \forall V$ voisinage de $0$ et
$\forall \phi \in V, |\dip{T, \phi}| < \epsilon$.

\hspace{0.9cm} $\Leftrightarrow \forall \epsilon > 0, \exists r > 0, n \in \mathbb{N}^*$
telle que $\forall \phi$ vérifiant $p_n(\phi) < r$, on a:
\begin{eqnarray}\label{eq:distalt}
|\dip{T, \phi}| < \epsilon
\end{eqnarray}

On montre que~\eqref{eq:dist} $\implies$~\eqref{eq:distalt}.
\begin{itemize}
    \item[$\mathbf{\Rightarrow}$]

Appliquons~\eqref{eq:distalt} avec $\epsilon = 1$: donc $\exists r, n$ telle que:
\begin{eqnarray}\label{eq:supp1}
p_n(\tilde{\phi}) < r \implies |\dip{T, \tilde{\phi}}| < 1
\end{eqnarray}

Si $\phi \in \mathcal{C}^\infty(\Omega)$, si $p_n(\phi) = 0$, d'après~\eqref{eq:distalt},
on a $|\dip{T, \phi}| < \epsilon$.

Si $p_n(\phi) \neq 0$, on pose:
\[
\tilde{\phi} = \frac{r}{2} \frac{\phi}{p_n(\phi)}
\]
et on a $p_n(\tilde{\phi}) = \sfrac{r}{2} < r$. Donc, par~\eqref{eq:supp1} on a
$|\dip{T, \tilde{\phi}}| < 1$. D'où $|\dip{T, \phi}| < \frac{2}{r} p_n(\phi)$.
Donc~\eqref{eq:dist} est vrai avec $c = \frac{2}{r}$ et $K = K_n$ car:
\[
p_n(\phi) = \sum_{|\alpha| \leq n}
    \sup_{x \in K_n} \left|\pd{^\alpha \phi}{x^\alpha}(x)\right|
\]

    \item[$\mathbf{\Leftarrow}$]

Si~\eqref{eq:dist} est vrai, $\exists K \subset \Omega$ compact,
$\exists k \in \mathbb{N}, c > 0, \forall \phi \in \mathcal{C}^\infty(\Omega)$:
\[
|\dip{T, \phi}| \leq c \sum_{|\alpha| < k}
    \sup_{x \in K} \left|\pd{^\alpha \phi}{x^\alpha}(x)\right|
\]

On peut choisir $n$ telle que $K \subset K_n$ et $n > k$ (car $K_n$ est une
suite exhaustive de compacts). On a donc $\forall \phi \in \mathcal{C}^\infty(\Omega)$:
\[
|\dip{T, \phi}| < c p_n(\phi)
\]

Soit $\epsilon > 0$. Posons $r = \frac{\epsilon}{c}$. Alors:
\[
p_n(\phi) < r \implies |\dip{T, \phi}| < \epsilon
\]
Donc~\eqref{eq:distalt} est vrai.
\end{itemize}
\end{proof}

\begin{theorem}
Il existe une application linéaire bijective $\Phi: \distc \to (\mathcal{C}^\infty(\Omega))'$.
Cela permet d'identifier $\distc$ et $(\mathcal{C}^\infty(\Omega))'$.
\end{theorem}

\begin{proof}
~\begin{itemize}[$\blacksquare$]
    \item Construction de $\Phi$: Soit $T \in \distc$. On veut définir
    $\Phi(T) \in (\mathcal{C}^\infty(\Omega))'$.

\begin{notation}
On note $K_0 = \supp{T}$ un compact inclus dans $\Omega$.
\end{notation}

On sait qu'il existe $\chi \in \mathcal{C}^\infty_0(\Omega)$ telle que $\chi \equiv 1$
sur un ouvert $V$ contenant $K_0$. Pour $\phi \in \mathcal{C}^\infty(\Omega)$, posons:
\begin{eqnarray}\label{eq:phi}
\dip{\Phi(T), \phi} \ :=\ \dip{T, \chi \phi}
\end{eqnarray}

On ne sait faire agir une distribution $T$ que sur des fonctions
$\mathcal{C}^\infty_0(\Omega)$. Or, on a bien $\chi \phi \in \mathcal{C}^\infty_0(\Omega)$ car $\supp{\chi}$
est un compact inclus dans $\Omega$, donc $\dip{T, \chi \phi}$ est bien défini.

La définition~\eqref{eq:phi} est indépendante du choix de $\chi$: si $\chi_1$
est une autre fonctions du même type ($\chi_1 \equiv 1$ sur $V_1$ contenant
$K_0$):
\[
\dip{T, \chi \phi} - \dip{T, \chi_1 \phi} = \dip{T, (\chi - \chi_1) \phi}
\]
or $\chi - \chi_1 \equiv 0$ sur $V \cap V_1$ et $(\chi - \chi_1)\phi \in
\mathcal{C}^\infty_0(\Omega)$ et sont support est inclus dans $\Omega \setminus (V \cap V_1)$ donc:
\[
\supp{(\chi - \chi_1) \phi} \cap K_0 = \emptyset
\]

Donc $T|_{\Omega \setminus K_0} = 0$ et comme $\supp{(\chi - \chi_1) \phi}
\subset \Omega \setminus (V \cap V_1) \subset \Omega \setminus K_0$, on a:
\[
\dip{T, (\chi - \chi_1) \phi} = 0
\]

On a donc:
\[
\dip{\Phi(T), \phi} = \dip{T, \chi \phi} = \dip{T, \chi_1 \phi}
\]
    \item Montrons que si $T \in \distc \implies \Phi(T) \in (\mathcal{C}^\infty(\Omega))'$ (
    i.e. on montre que $\Phi(T)$ vérifie~\eqref{eq:dist})

$T \in \dist$ vérifie~\eqref{eq:dist} par définition. Posons $K = \supp{\chi}$.

Soit $\phi \in \mathcal{C}^\infty(\Omega)$. On a $\dip{\Phi(T), \phi} = \dip{T, \chi \phi}$.

\begin{remark}[La formule de Leibniz] On a:
\[
\pd{^\alpha \chi\phi}{x^\alpha} = \sum_{\beta \leq \alpha} \binom{\alpha}{\beta}
(\partial^{\alpha - \beta}\chi)(\partial^\beta\phi)
\]
\end{remark}

On a:
\[
\sup_{x \in K}|\pd{^\alpha (\chi \phi)}{x^\alpha}| \leq
    c_\alpha \sum_{\beta \leq \alpha} \sup_{x \in K} |\partial^\beta \phi|
\]

On applique l'inégalité définie dans~\eqref{eq:dist} à $\chi \phi$:
\[
\begin{aligned}
|\dip{\Phi(T), \phi}| = |\dip{T, \chi \phi}| & \leq c \sum_{|\alpha| \leq k}
    c_\alpha \sum_{\beta \leq \alpha} \sup_{x \in K} |\partial^\beta \phi| \\
& \leq c' \sum_{\beta \leq k} \sup_{x \in K} |\partial^\beta \phi|
\end{aligned}
\]

On a obtenu que $\Phi(T)$ vérifie l'estimation~\eqref{eq:dist} de
caractérisation de $(\mathcal{C}^\infty(\Omega))'$.

    \item Montrons que $\Phi$ est injective: soit $T \in \distc$ telle que
    $\Phi(T) = 0$. On veut voir que $T = 0$ (i.e. $K_0 = \emptyset$).

Supposons que c'est faux: soit $x_0 \in K_0$. On a définit $\Phi$ par:
\[
\dip{\Phi(T), \phi} = \dip{T, \chi \phi}
\]
où $\chi \in \mathcal{C}^\infty_0(\Omega), \chi \equiv 1$ sur un ouvert $V$ avec $K_0 \subset V$.

Comme $\Phi(T) = 0$, on a $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$:
\[
0 = \dip{\Phi(T), \phi} = \dip{T, \chi \phi}
\]

Prenons $\phi \in \mathcal{C}^k_0(V)$. Alors $\chi \phi = \phi$ ($\chi
\equiv 1$ sur $V$). On a donc: $\forall \phi \in \mathcal{C}^k_0(V),
\dip{T, \phi} = 0$ . Donc $T|_{V} = 0 \implies x_0 \notin K_0$.

    \item Montrons que $\Phi$ est surjective.

Soit $S \in (\mathcal{C}^\infty(\Omega))'$. On cherche $T \in \distc$ telle que $\Phi(T) = S$.
Définissons $T: \forall \phi \in \mathcal{C}^\infty_0(\Omega)$:
\[
\dip{T, \phi} = \dip{S, \phi}
\]

Par caractérisation de $(\mathcal{C}^\infty(\Omega))'$, il existe $K \subset \Omega$ compact,
$k \in \mathbb{N}, c > 0$ et $\forall \phi \in \mathcal{C}^\infty(\Omega)$:
\[
|\dip{S, \phi}| \leq c \sum_{|\alpha| < k}
    \sup_{x \in K} \left|\pd{^\alpha \phi}{x}\right|
\]
Cela entraîne que $T \in \distk$.

Si $\supp{\phi} \supset \Omega \setminus K$, le nombre de droite de
l'inégalité précédente est nul, donc $\dip{T, \phi} = 0$, donc
$T|_{\Omega \setminus K} = 0$, donc $\supp{T} \subset K$, donc
$T$ est à support compact $\implies T \in \distc$.

Soit $\chi \in \mathcal{C}^\infty_0(\Omega), \chi \equiv 1$ sur un ouvert $V \supset K_0$. Par
définition $\dip{\Phi(T), \phi} = \dip{T, \chi \phi}, \forall \phi \in \mathcal{C}^\infty(\Omega)$

Par définition de $T$: $\dip{T, \chi \phi} = \dip{S, \chi \phi}$. Il reste à voir
que $\forall \phi \in \mathcal{C}^\infty(\Omega)$:
\[
\dip{S, \chi \phi} = \dip{S, \phi}
\]

Soit $\dip{S, (1 - \chi) \phi} = 0$. On a vu que $\forall \theta \in \mathcal{C}^\infty(\Omega)$:
\[
|\dip{S, \theta}| \leq c \sum_{|\alpha| \leq k}
    \sup_{x \in K} \left|\pd{^\alpha \theta}{x}\right|
\]

Si on prend $\theta = (1 - \chi)\phi$ que est égale à zéro sur $V$ qui
contient $K$, on a $\sup_{x \in K} |\pd*{^\alpha \theta}{x}| = 0$. Donc
$\dip{S, (1 - \chi) \phi} = 0$.
\end{itemize}
\end{proof}

\begin{remark}
~\begin{itemize}
    \item $\dist$ coïncide avec $(\mathcal{C}^\infty(\Omega))'$, donc toute distribution à support
    compact se prolonge, de manière unique, en une forme linéaire
    $T: \dist \to (\mathcal{C}^\infty(\Omega))'$ tel qu'il existe $K \subset \Omega$ compact,
    $k \in \mathbb{N}, c > 0$ et $\forall \phi \in \mathcal{C}^\infty(\Omega)$:
\[
|\dip{T, \phi}| \leq c \sum_{|\alpha| \leq k}
    \sup_{x \in K} \left|\pd{^\alpha \phi}{x^\alpha}\right|
\]
    \item Toute distribution à support compact est d'ordre fini.
    \item $T \in (\mathcal{C}^\infty(\Omega))' \approx \distc \Leftrightarrow T:\mathcal{C}^\infty(\Omega) \to \mathbb{C}$ est
    linéaire et pour toute suite $(\phi_n)$ de $\mathcal{C}^\infty(\Omega)$ convergent vers $0$,
    ainsi que toutes ses dérivées, uniformément sur les compacts:
    \[
    \dip{T, \phi_n} \xrightarrow{n \to \infty} 0
    \]
\end{itemize}
\end{remark}

% kate: default-dictionary fr_FR;
