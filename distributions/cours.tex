% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\documentclass[DIV=14,10pt,parskip=half*]{scrbook}

% {{{ packages

\usepackage{xltxtra}
\usepackage{polyglossia}
\setdefaultlanguage{french}

% math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{mathrsfs}
\usepackage{xfrac}
\usepackage{cases}

% formatting
\usepackage[shortlabels]{enumitem}
\usepackage[usenames,svgnames]{xcolor}
\usepackage{graphicx}
\usepackage{cleveref}

\usepackage{tikz}

% }}}

% {{{ commands

\NewDocumentCommand \dx { O{x} } {\,\mathrm{d} #1}
\NewDocumentCommand \ind { m } { {\ \mbox{l\hspace{-0.55em}1}}_{#1} }

\NewDocumentCommand \lploc { s O{\Omega} m }
{
\IfBooleanTF #1
    { L^{#3}_{\mathrm{loc}} }
    { L^{#3}_{\mathrm{loc}}(#2) }
}
\NewDocumentCommand \dip { sm } {\IfBooleanTF#1{\langle #2 \rangle}{\left\langle #2 \right\rangle}}

\NewDocumentCommand \distk { O{k} } {\mathcal{D}^{'(#1)}(\Omega)}
\NewDocumentCommand \dist {} {\mathcal{D}'(\Omega)}
\NewDocumentCommand \distf {} {\mathcal{D}^{'F}(\Omega)}
\NewDocumentCommand \distc {} {\mathcal{E}'(\Omega)}
\NewDocumentCommand \dirac { O{0} } {\delta_{#1}}
\NewDocumentCommand \vp {} {\mathrm{v.p.} \left(\frac{1}{x}\right)}
\NewDocumentCommand \pfa {} {\mathrm{p.f.}\, x_t^{\alpha}}
\NewDocumentCommand \pf {} { \mathrm{p.f.} \left(\frac{1}{x^2}\right)}

\NewDocumentCommand \pd { s m m } {
    \IfBooleanTF #1 {\partial_{#3} #2}{\dfrac{\partial #2}{\partial #3}}
}

\NewDocumentCommand \deffunction { m m m m m} {
\begin{aligned}
{#1} & ~\colon \!\!\!\!\!\! & {#2} & ~\longrightarrow & & {#3} \\
     &        & {#4} & ~\longmapsto  & & {#5}.
\end{aligned}
}

% operators
\DeclareMathOperator{\measure}{mesure}
\DeclareMathOperator{\supp}{Supp}

% theorems
\theoremstyle{plain}
\newtheorem{theorem}{Théorème}[chapter]
\newtheorem{lemma}{Lemme}[section]
\newtheorem{proposition}{Proposition}[section]
\newtheorem*{corollary}{Corollaire}

\theoremstyle{definition}
\newtheorem{definition}{Définition}[chapter]
\newtheorem{property}{Propriété}[chapter]

\theoremstyle{remark}
\newtheorem*{remark}{Remarque}
\newtheorem*{notation}{Notation}
\newtheorem*{example}{Exemple}
\newtheorem*{reminder}{Rappel}

% }}}

% {{{ titlepage

\NewDocumentCommand \logo {} {\fbox{$\mathcal{DES}$}}

\NewDocumentCommand \titleAT {} {
\begingroup                                 % Create the command for including the title page in the document
\newlength{\drop}                           % Command for generating a specific amount of whitespace
\drop=0.1\textheight                        % Define the command as 10% of the total text height

\rule{\textwidth}{1pt}\par                  % Thick horizontal line
\vspace{2pt}\vspace{-\baselineskip}         % Whitespace between lines
\rule{\textwidth}{0.4pt}\par                % Thin horizontal line

\vspace{\drop}                              % Whitespace between the top lines and title
\centering                                  % Center all text
\textcolor{Black}{\textbf{                  % Red font color
{\Huge Distributions}\\[0.5\baselineskip]   % Title line 1
{\Large et}\\[0.75\baselineskip]            % Title line 2
{\Huge Espaces de Sobolev}}}                % Title line 3

\vspace{0.25\drop}                          % Whitespace between the title and short horizontal line
\rule{0.3\textwidth}{0.4pt}\par             % Short horizontal line under the title
\vspace{\drop}                              % Whitespace between the thin horizontal line and the author name

{\Large \textsc{MACS2}}\par                 % Author name

\vfill                                      % Whitespace between the author name and publisher text
{\large \textcolor{Black}{\logo}}\\[0.5\baselineskip]

\vspace*{\drop}                             % Whitespace under the publisher text

\rule{\textwidth}{0.4pt}\par                % Thin horizontal line
\vspace{2pt}\vspace{-\baselineskip}         % Whitespace between lines
\rule{\textwidth}{1pt}\par                  % Thick horizontal line

\endgroup}

% }}}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       TITLE PAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\pagestyle{empty}
\titleAT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    TABLE OF CONTENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\pagestyle{plain}
\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       CONTENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\include{chapitre1}     % Introductions
\include{chapitre2}     % Distributions
\include{chapitre3}     % Opérations sur les distributions
\include{chapitre4}     % Convolution des distributions

\end{document}

% kate: default-dictionary fr_FR;
