% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Opérations sur les distributions}

\section{Produit d'une distribution et d'une fonction \texorpdfstring{$\mathcal{C}^\infty$}{Cinf}}

\begin{definition}
Soient $T \in \dist$ et $a \in \mathcal{C}^\infty(\Omega)$. On définit $aT$ par:
\[
\forall \phi \in \mathcal{C}^\infty_0(\Omega), \qquad \dip{aT, \phi} := \dip{T, a\phi}.
\]

Alors $aT \in \dist$.
\end{definition}

\begin{proof}
Si $T \in \dist \Leftrightarrow \forall K \subset \Omega$ compact, $\exists k \in
\mathbb{N}, c > 0$ et $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$ avec $\supp{\phi} \subset K$:
\[
|\dip{T, \phi}| \leq c \sum_{|\alpha| < k} \sup \left|\pd{^\alpha \phi}{x^\alpha}\right|
\]

Appliquons cela à $\phi = a \psi$:
\[
\sup \left|\pd{^\alpha \psi}{x^\alpha}\right| \leq
    \sum_{|\beta| \leq |\alpha|} \binom{\alpha}{\beta}
    \sup \left|\pd{^{\alpha - \beta} a}{x^{\alpha - \beta}}\right|
    \sup \left|\pd{^\beta \phi}{x^\alpha}\right|
\]

Il existe $c_K$ dépendant de $K$:
\[
\begin{aligned}
& \sup \left|\pd{^\alpha \psi}{x^\alpha}\right|
    \leq c \sum_{|\beta| < |\alpha|}
        \sup_{x \in K} \left|\pd{^\beta \psi}{x^\beta}\right| \\
\Leftrightarrow &
    \sup \left|\pd{^\alpha \psi}{x^\alpha}\right| \leq
    c \sum_{|\beta| < |\alpha|}
        \sup_{x \in \Omega} \left|\pd{^\beta \psi}{x^\beta}\right|
\end{aligned}
\]

Donc $\forall K$ compact, $\exists k \in \mathbb{N}, c > 0$ et $\forall \phi
\in \mathcal{C}^\infty_0(\Omega)$ avec $\supp{\phi} \subset K$:
\[
|\dip{a T, \phi}| = |\dip{T, \psi}| \leq c_K \sum_{|\beta| < k}
                    \sup_{x \in \Omega} \left|\pd{^\beta \phi}{x^\beta}\right|
\]

Donc $aT \in \dist$.
\end{proof}

\begin{property}
Soient $a, b \in \mathcal{C}^\infty(\Omega)$ et $T, S \in \dist$:
\begin{enumerate}[(i)]
    \item $(a + b)T = aT +bT$.
    \item $a(T + S) = aT +aS$.
    \item $a(bT) = (ab)T$
    \item $\supp{aT} \subset (\supp{a} \cap \supp{T})$.
\end{enumerate}
\end{property}

\begin{proof}[Démonstration de (v)]
Montrons que si $x_0 \in \supp{a} \cap \supp{T}$, alors $x_0 \notin \supp{aT}$.
\begin{itemize}
    \item $x_0 \notin \supp{a}$: il existe $\omega$ voisinage ouvert de $x_0$
    tel que $a|_{\omega} = 0$. Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$:
    \[
    \dip{a T, \phi} = \dip{T, a \phi} = 0
    \]
    Donc $x_0 \notin \supp{aT}$.

    \item $x_0 \notin \supp{T}$: il existe $\omega$ voisinage de $x_0$ avec
    $T|_{\omega} = 0$:
    \[
    \dip{T, \psi} = 0, \forall \psi \in \mathcal{C}^{\infty}_0(\omega)
    \]

    Si $\phi \in \mathcal{C}^\infty_0(\omega)$:
    \[
    \dip{aT, \phi} = \dip{T, a \phi} = 0
    \]

    Donc $x_0 \notin \supp{aT}$.
\end{itemize}
\end{proof}

\subsection{Exemples de produits}

\begin{example}
Soit $a: \mathbb{R}\to \mathbb{C}\in \mathcal{C}^\infty(\mathbb{R})$ et $x_0 \in \mathbb{R}$.
Alors $a \dirac[x_0] = a(x_0) \dirac[x_0]$.

Soit $\phi \in \mathcal{C}^\infty_0(\mathbb{R})$.
\[
\begin{aligned}
\dip{a \dirac[x_0], \phi} & = \dip{\dirac[x_0], a\phi} \\
& = (a\phi)(x_0) \\
& = a(x_0) \phi(x_0) \\
& = a(x_0) \dip{\dirac[x_0], \phi} \\
& = \dip{a(x_0)\dirac[x_0], \phi}
\end{aligned}
\]

Et ceci $\forall \phi \in \mathcal{C}^\infty_0(\mathbb{R})$ donc $a \dirac[x_0] =
a(x_0) \dirac[x_0]$.
\end{example}

\begin{example}
Autre exemple: $x~ \vp = 1$.

Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$:
\[
\begin{aligned}
\dip{x \vp, \phi} & = \dip{\vp, x \phi} \\
& = \lim_{x \to 0^+} \int_{|x| > \epsilon}{\frac{1}{x} x \phi(x) \dx} \\
& = \int_{\mathbb{R}}{\phi(x) \dx} \\
& = \dip{1, \phi}
\end{aligned}
\]

Et ceci $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$ donc $x~ \vp = 1$.
\end{example}

\begin{proposition}
Soit $T \in \dist $ avec $\Omega = \mathbb{R}$. On a équivalence entre:
\begin{enumerate}[(i)]
    \item $xT = 0$.
    \item $T = \lambda \dirac$ pour $\lambda \in \mathbb{C}$.
\end{enumerate}
\end{proposition}

\begin{proof}
~\begin{itemize}[$\blacksquare$]
    \item $(ii) \implies (i)$

Évident: $x \dirac = 0 \dirac = 0$.
    \item $(i) \implies (ii)$

Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$ avec $\Omega = \mathbb{R}\setminus \{0\}$. Alors
$\phi(x) = x \psi(x)$ avec $\psi \in \mathcal{C}^\infty_0(\Omega)$ où $\Omega = \mathbb{R}$.

Il suffit de prendre $\psi(x) = \frac{\phi(x)}{x}$ si $x \neq 0$ et
$\psi(0) = 0$. Alors:
\[
\dip{T, \phi} = \dip{T, x \psi} = \dip{x T, \psi} = 0 \text{ car } xT = 0.
\]

Donc, $T|_{K \setminus \{ 0 \}} = 0$ donc $\supp{T} \subset \{ 0 \}$. On
sait que cela implique qu'il existe $N \in \mathbb{N}, a_j \in \mathbb{C}$ tels que
\[
T = \sum_{j = 0}^N a_j \dirac^{(j)}.
\]

Alors:
\[
xT = \sum_{j = 0}^N a_j x \dirac^{(j)}
\]

donc $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$ avec $\Omega = \mathbb{R}$:
\[
\sum_{j = 0}^N a_j \dip{\dirac^{(j)}, x \phi} = 0
\]

Supposons que, pour $x$ voisin de $0, \phi(x) = x^k$. Alors:
\[
\dip{\dirac^{(j)}, x \phi} =
    (-1)^j (x \phi)^{(j)}(0) = (-1)^j (x^{k + 1})^j|_{x = 0}
\]
or:
\[
(x^{k + 1})^{(j)} = \frac{(k + 1)!}{(k - j + 2)!} x^{k + 1 - j}, \forall j \leq k + 1.
\]

Prenons:
\begin{itemize}
    \item $j = N, k = N - 1$. Alors $(x^N)^{(N)} = N!$.
    \item $j < n, k = N - 1$. $(x^N)^{(j)}|_{x = 0} = 0$ (car égal à
    $c * x^{N - j})$.
\end{itemize}

Donc:
\[
0 = \sum_{j = 0}^N a_j (-1)^j \underbrace{(x^N)^{(j)}|_{x=0}}_{ = 0} \text{ si } j < N
\]
ou encore $0 = a_N (-1)^N N!$. Donc $a_N = 0$.

On obtient de la même manière $a_{N - 1}, a_{N - 2}, \dots, a_1 = 0$. Donc
$T = a_0 \dirac$.
\end{itemize}
\end{proof}

\section{Dérivation des distributions}

\begin{theorem}
Soit $T \in \dist$. On définit pour $j = \llbracket 1, d \rrbracket,
\Omega \subset \mathbb{R}^d$ une forme linéaire $\pd*{T}{x_j}$ sur $\mathcal{C}^\infty_0(\Omega)$ par:
\[
\forall \phi \in \mathcal{C}^\infty_0(\Omega) \dip{\pd{T}{x_j}, \phi}
    = -\dip{T, \pd{\phi}{x_j}}
\]

Alors $\pd*{T}{x_j} \in \dist$.
\end{theorem}

\begin{proof}
Soit $T \in \dist$. $\forall K \subset \Omega$ compact. $\exists c > 0,
\exists k \in \mathbb{R}$ tel que $\forall \psi \in \mathcal{C}^\infty_0(\Omega)$ avec
$\supp{\psi} \subset K$:
\[
|\dip{T, \psi}| \leq c \sum_{|\alpha| \leq k}
    \sup_{x \in \Omega} \left|\pd{^\alpha \psi}{x^\alpha}\right|.
\]

Alors, en prenant $\psi = \pd*{\phi}{x_j}$ il vient:
\[
\left|\dip{\pd{T}{x_j}, \phi}\right| = \left|\dip{T, \pd{\phi}{x_j}}\right|
    \leq c \sum_{|\alpha| \leq k}
    \sup_{x \in \Omega} \left| \pd{^\alpha}{x^\alpha} \pd{\phi}{x_j} \right|
\]
et ceci $\forall \psi \in \mathcal{C}^\infty_0(\Omega)$ avec $\supp{\psi} \subset K$. Donc:
\[
\left| \dip{\pd{T}{x_j}, \phi} \right| \leq c \sum_{|\beta| \leq k}
    \sup_{x \in \Omega} \left| \pd{^\beta}{x^\beta} \pd{\phi}{x_j} \right|
\]
\end{proof}

\begin{remark}
~\begin{enumerate}
    \item La démonstration entraîne que:
\[
T \in \distk \Leftrightarrow \pd{T}{x_j} \in \distk[k + 1]
\]

    \item Justification de la définition: soit $f \in \mathcal{C}^\infty_0(\Omega)$.

Considérons:
\[
\int_{\Omega}{\pd{f}{x_j} \phi(x) \dx} =
    \int_{\mathbb{R}^d} \pd{f}{x_j} \phi(x) \dx =
    -\int_{\mathbb{R}^d} f(x) \pd{\phi}{x_j} \dx
\]
On peut réécrire ceci: $\dip{T_{\pd*{f}{x_j}}, \phi} = -\dip{T_f, \pd*{\phi}{x_j}}$.
\end{enumerate}
\end{remark}

\begin{property}
~\begin{enumerate}[(i)]
    \item Si $f \in \mathbb{C}^1(\Omega), \pd*{T_f}{x_j} = T_{\pd*{f}{x_j}}$.
    \item $\supp{\pd*{T}{x_j}} \subset \supp{T}$.
    \item Soient $a \in \mathcal{C}^\infty(\Omega), T \in \dist$:
\[
\pd{aT}{x_j} = \pd{a}{x_j} T + a \pd{T}{x_j}
\]
\end{enumerate}
\end{property}

\begin{proof}
~\begin{itemize}[$\blacksquare$]
    \item \textbf{(i)} Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$.
\[
\begin{aligned}
\dip{\pd{T_f}{x_j}, \phi} & = - \dip{T_f, \pd{\phi}{x_j}} \\
& = -\int_{\Omega} f(x) \pd{\phi}{x_j} \dx \\
& = \int_{\Omega} \pd{f}{x_j} \phi(x) \dx
\end{aligned}
\]

Or
\[
\begin{aligned}
\int_\mathbb{R}f(x) \pd{\phi}{x_j} \dx & =
    \int_{-M}^M f(x) \pd{\phi}{x_j} \dx \\
& = f(M) \underbrace{\phi(M)}_{ = 0} - f(-M)\underbrace{\phi(-M)}_{ = 0} -
    \int_{-M}^M \pd{f}{x_j} \phi(x) \dx
\end{aligned}
\]

Donc:
\[
\dip{\pd{T_f}{x_j}, \phi} = \dip{T_{\pd*{f}{x_j}}, \phi}.
\]

    \item \textbf{(ii)} Il suffit de voir que si $\Omega$ est un ouvert et
    $T|_{\Omega} = 0$, alors $\pd*{T}{x_j}|_{\Omega} = 0$ ($\Leftrightarrow
    \Omega \setminus \supp{T} \subset \Omega \setminus \supp{\pd*{T}{x_j}}$).

Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$. Alors:
\[
\dip{\pd{T}{x_j}, \phi} = -\dip{T, \pd{\phi}{x_j}} = 0
\]
car $\supp{\pd*{\phi}{x_j}} \subset \Omega$ et $T|_{\Omega} = 0$.

Donc $\pd*{T}{x_j}|_{\Omega} = 0$.

    \item \textbf{(iii)} Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$.
\[
\dip{\pd{aT}{x_j}, \phi} = -\dip{aT, \pd{\phi}{x_j}} = - \dip{T, a \pd{\phi}{x_j}}
\]
or,
\[
a\pd{\phi}{x_j} = \pd{a \phi}{x_j} - \phi \pd{a}{x_j}.
\]
Donc:
\[
\begin{aligned}
\dip{\pd{aT}{x_j}, \phi} & =
     -\dip{T, \pd{a \phi}{x_j}} + \dip{T, \phi \pd{a}{x_j}} \\
& = \dip{\pd{T}{x_j}, a\phi} + \dip{T,\phi \pd{a}{x_j}} \\
& = \dip{a\pd{T}{x_j}, \phi} + \dip{\pd{a}{x_j}T,\phi} \\
& = \dip{a\pd{T}{x_j} + \pd{a}{x_j}T, \phi}
\end{aligned}
\]
\end{itemize}
\end{proof}

\subsection{Extension}

Soit $\alpha \in \mathbb{N}^d$. On définit $\pd*{^\alpha T}{x}$ pour $T \in \dist$ par:
\[
\forall \phi \in \mathcal{C}^\infty_0(\Omega)
    \dip{\pd{^\alpha T}{x^\alpha}, \phi} =
    (-1)^{\alpha} \dip{T, \pd{^\alpha \phi}{x^\alpha}}
\]

Plus généralement, si $x \mapsto a_{\alpha}(x)$ sont des fonctions $\mathcal{C}^\infty_0(\Omega)$ et
si on note $P = \sum_{|\alpha| \leq m} a_{\alpha}(x) \pd*{^\alpha}{x}$ alors on écrit:
\[
PT = \sum_{|\alpha| \leq m} a_{\alpha}\pd{^\alpha T}{x^\alpha}
\]

Si on définit $^tP$ par:
\[
^tPT = \sum_{|\alpha| \leq m} (-1)^{\alpha} \pd{^\alpha (a_{\alpha} T)}{x^\alpha},
\]
$\forall T \in \dist, \forall \phi \in \mathcal{C}^\infty_0(\Omega)$, on a:
\[
\dip{PT, \phi} = \dip{T, P^t \phi}.
\]

\subsection{Exemples de dérivées au sens des distributions}

\begin{example}[Exemple 1]
Soit $u \in L^1_{\mathrm{loc}}(\mathbb{R})$. Posons $v(x) = \int_0^x u(y) \dx[y]$. Alors
$v$ est continue et sa dérivée au sens des distributions $v'$ est égale à $u$.

\begin{itemize}[$\blacksquare$]
    \item Continuité de $v$.

\[
\begin{aligned}
|v(x + h) - v(x)| & = \left|\int_x^{x + h} u(y)\dx[y]\right| \\
& \leq \int |u(y)| \ind{[x, x+h]} \dx[y] \xrightarrow{h \to 0} 0
\end{aligned}
\]
par le \Cref{thm:tcd}. Donc $v$ est continue en $x$.

    \item Dérivabilité de $v$.

Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$. On a:
\[
\begin{aligned}
\dip{v', \phi} & = -\dip{v, \phi'} \\
& = - \int_{\mathbb{R}} v(x) \phi'(x) \dx \\
& = - \int_{\mathbb{R}} \phi'(x) \left(\int_0^x u(y) \dx[y]\right) \dx
\end{aligned}
\]
Or:
\[
\int_0^x u(y) \dx[y] =
\left\{
    \begin{aligned}
    \int \ind{\{0 < y < x\}} u(y) \dx[y], & \quad x \geq 0 \\
    -\int \ind{\{x < y < 0\}} u(y) \dx[y], & \quad  x < 0
    \end{aligned}
\right.
\]

Donc:
\[\int_0^x u(y) \dx[y] = \int_{\mathbb{R}} \underbrace{(\ind{\{0 < y < x\}} -
                         \ind{\{x < y < 0\}})}_{g(x, y)} u(y) \dx[y]\]

Donc:
\[
\dip{v', \phi} =
    -\int_{\mathbb{R}} \phi'(x) \left(\int g(x,y) u(y)\dx[y]\right) \dx
\]

On a $|g(x,y)| \leq 1$ et $|y| \leq |x|$ si $g(x, y) \leq 0$.
\[\int \int |\phi'(x)| |g(x,y)| |u(y)| \dx \dx[y] \leq
  \int_{-M}^M \int_{-M}^M |\phi'(x)| |u(y)| \dx \dx[y]\]

Si $M$ est tel que $\supp{\phi} \subset [-M, M]$, comme
$u \in L^1_{\mathrm{loc}}(\mathbb{R})$, la dernière intégrale est finie et le théorème
de Fubini s'applique. Alors, on a:
\[
-\dip{v', \phi} = - \int_{\mathbb{R}} u(y) \left(\int g(x,y) \phi'(x) \dx\right) \dx[y]
\]

Or:
\[
\begin{aligned}
\int g(x,y) \phi'(x) \dx & =
    \int \ind{\{0 < y < x\}} \phi'(x) \dx -
    \int \ind{\{x < y < 0\}} \phi'(x) \dx \\
& = -\phi(y) \ind{\{y > 0\}} -\phi(y) \ind{\{y < 0\}} \\
& = -\phi(y)~ p.p.
\end{aligned}
\]
Donc, $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$:
\[
\dip{v', \phi} = -\int u(y) (-\phi(y)) \dx[y] = \dip{u, \phi}
\]
On a donc $v' = u$.
\end{itemize}
\end{example}

\begin{example}[Exemple 2]
Soit $H$ la fonction de Heaviside définie par $H(x) = \ind{\{x > 0\}}$. Alors
$\pd*{H}{x} = \dirac$.

Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$.
\[
\begin{aligned}
\dip{\pd{H}{x}, \phi} & = -\dip{H, \pd{\phi}{x}} \\
& = -\int H(x) \phi'(x) \dx \\
& = -\int_0^{+\infty} \phi'(x)\dx \\
& = \phi(0) \\
& = \dip{\dirac, \phi}
\end{aligned}
\]
Donc $\pd*{H}{x} = \dirac$.
\end{example}

\begin{example}[Exemple 3]
$x \mapsto \log(|x|)$ est localement intégrable sur $\mathbb{R}$
(car $\displaystyle \int_0^a |\log(|x|)| \dx < \infty$).

Montrons que:
\[\pd{\log(|x|)}{x} = \vp.\]

\begin{proof}
Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$ où $\Omega = \mathbb{R}$.
\[
\begin{aligned}
\dip{\pd{\log(|x|)}{x}, \phi} & = -\dip{\log(|x|), \phi'} \\
& = -\int_{\mathbb{R}} \log(|x|) \phi'(x) \dx \\
& = -\lim_{\epsilon \to 0^+} \int_{|x| > \epsilon}
                               \log(|x|) \phi'(x) \dx \\
& = I_{\epsilon}.
\end{aligned}
\]

On a
\[
\begin{aligned}
\int_{\epsilon}^{+\infty} \log(x) \phi'(x) \dx & =
       \left[\log(x) \phi(x)\right]_{\epsilon}^{+\infty} -
       \int_{\epsilon}^{+\infty} \frac{1}{x} \phi(x) \dx \\
& = -\log(\epsilon) \phi(\epsilon) -
     \int_{\epsilon}^{+\infty} \frac{\phi(x)}{x} \dx
\end{aligned}
\]

et
\[
\begin{aligned}
\int_{-\infty}^{-\epsilon} \log(-x) \phi'(x) \dx & =
      \left[\log(-x) \phi(x)\right]_{-\infty}^{-\epsilon} -
      \int_{-\infty}^{-\epsilon} \frac{1}{x} \phi(x) \dx \\
& = \log(\epsilon) \phi(-\epsilon) -
      \int_{-\infty}^{-\epsilon} \frac{\phi(x)}{x} \dx
\end{aligned}
\]

Donc
\[I_{\epsilon} = \log(\epsilon) (\phi(-\epsilon) - \phi(\epsilon)) -
                 \int_{|x| > \epsilon} \frac{\phi(x)}{x} \dx\]

Par un développement limité:
\[\phi(\epsilon) = \phi(0) + \epsilon g(\epsilon)\]
avec $g(\epsilon)$ bornée quand $\epsilon \to 0$.

Donc
\[\log(\epsilon) (\phi(-\epsilon) - \phi(\epsilon)) =
    -\log(\epsilon)\underbrace{(g(-\epsilon) + g(\epsilon))}_{\text{borné}})\]

Donc
\[
\dip{\pd{\log(|x|)}{x}, \phi} =
    - \lim_{\epsilon \to 0^+}
    \int_{|x| > \epsilon} \frac{\phi(x)}{x} \dx = \dip{\vp, \phi}
\]
\end{proof}
\end{example}

\begin{proposition}
Soit $I$ intervalle ouvert de $\mathbb{R}, T \in \dist$ avec $\Omega = I$.
Alors $\pd*{T}{x} = 0$ si et seulement si $T$ est une constante.
\end{proposition}

\begin{proof}
\[
\pd{T}{x} = 0 \Leftrightarrow \forall \theta \in \mathcal{C}^\infty_0(\Omega)
    \dip{\pd{T}{x}, \theta} = 0.
\]
i.e. $\dip{T, \theta'} = 0$ avec $\Omega = I$.

Soit $\rho \in \mathcal{C}^\infty_0(\Omega)$ avec $\Omega = I$ tel que $\int{\rho(x) \dx} = 1$.
Soit $\phi \in \mathcal{C}^\infty_0(\Omega)$ avec $\Omega = I$. Soit $\psi \in \mathcal{C}^\infty_0(\Omega)$ avec
$\Omega = I$ tel que:
\[\psi(x) = \phi(x) - \rho(x)\int \phi(y) \dx[y]\]

Alors,
\[\int_{\mathbb{R}} \psi(x) \dx = \int_{\mathbb{R}} \phi(x) \dx -
    \left(\int \rho(x) \dx\right) \int \phi(y) \dx[y] = 0\]

Posons $\theta(x) = \int_{-\infty}^x{\psi(y) \dx[y]} \in \mathcal{C}^\infty(\Omega)$ et on a:
\[
\theta(x) =
\left\{
    \begin{aligned}
    0 & \quad x \leq a \\
    \int_{-\infty}^x \psi(y) \dx[y] =
        \int_{-\infty}^{+\infty} \psi(y) \dx[y] = 0 & \quad x > b
    \end{aligned}
\right.
\]

Donc $\supp{\theta} \subset [a, b]$ donc $\theta \in \mathcal{C}^\infty_0(\Omega)$ avec
$\Omega = I$. Donc $\dip{T, \theta'} = 0$.

Or $\theta' = \psi$ donc $\dip{T, \psi} = 0$, i.e. $\dip{T, \phi} - \dip{T, \rho}
\int \phi(y) \dx[y] = 0$.

Posons $C = \dip{T, \phi}$. On a:
\[
\dip{T, \phi} = c \int \phi(y) \dx[y] = \dip{C, \phi}
\]
$\forall \phi \in \mathcal{C}^\infty_0(\Omega)$ avec $\Omega = I$.

Donc $T = C$.
\end{proof}

\section{Suites de distributions}

\begin{theorem}
Soit $(T_n)_n$ une suite de $\dist$ telle que $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$,
$(\dip{T_n, \phi})_n$ converge vers une limite $l_f \in \mathbb{C}$. Il existe
alors $T \in \dist$ telle que $\forall \phi \in \mathcal{C}^\infty_0(\Omega) l_f = \dip{T, \phi}$,
i.e. $T_n \to T$ dans $\dist$.
\end{theorem}

\begin{definition}
Soit $(T_n)_n$ une suite de $\distc$. On dit que $T_n$ converge vers $T \in \distc$
dans $\distc$ si et seulement si $\forall \phi \in \mathcal{C}^\infty(\Omega)$:
\[\dip{T_n, \phi} \xrightarrow{n \to \infty} \dip{T, \phi}.\]
\end{definition}

\begin{remark}
Si $(T_n)_n$ converge vers $T$ dans $\distc$, on a $\dip{T_n, \phi} \to
\dip{T, \phi}$, $\forall \phi \in \mathcal{C}^\infty(\Omega) \implies \dip{T_n, \phi} \to
\dip{T, \phi}$, $\forall \phi \in \mathcal{C}^\infty_0(\Omega)$. Donc $T_n \to T$ dans $\dist$.
\end{remark}

\begin{example}
Soit:
\[T_n = \frac{1}{2n} \ind{[-n, n]} \in \distc\]
Avec $T_n \to 0$ quand $n \to \infty$. $T_n$ ne converge pas dans $\distc$.
$\lim T_n = T = 0$ par unicité de la limite. On doit avoir $\forall \phi
\in \mathcal{C}^\infty(\Omega), \dip{T_n, \phi} \to 0$.

Prenons $\phi \equiv 1$:
\[\dip{T_n, \phi} = \frac{1}{2n} \int_\mathbb{R}\ind{[-n, n] \dx} =
                   \frac{1}{2n} \int_{-n}^n \dx = 1\]
Donc $\dip{T_n, \phi}$ ne peut pas tendre vers $0$.
\end{example}

\section{Formules de Green et Gauss}

\subsection{Ouverts de classe \texorpdfstring{$\mathcal{C}^k$}{Ck}}

\begin{definition}\label{def:ouvert}
Un ouvert $\Omega \subset \mathbb{R}^d$ est dit à bord de classe $\mathcal{C}^k,
k \in \mathbb{N}$ si et seulement si $\exists \rho: \mathbb{R}^d \to \mathbb{R}^d$
de classe $\mathcal{C}^k(\Omega)$ telle que $\Omega = \{x \,|\, \rho(x) < 0 \}$ et que
$\forall x \in \partial \Omega = \{x \,|\, \rho(x) = 0 \}$ on a
$\nabla \rho \neq \mathbf{0}$.
\end{definition}

\begin{example}
Soit $|x| = \|x\|_{L^2(\mathbb{R}^d)}$. Soit $\Omega = \mathcal{B}(0, R)$.

$\Omega$ est un ouvert à bord $\mathcal{C}^k(\Omega)$ avec $\rho = |x|^2 - R^2$ et
$\nabla \rho = 2 [x_1, \dots, x_d]^T \neq 0$.
\end{example}

\begin{example}
Soit $\psi: \mathbb{R}^{d - 1} \to \mathbb{R}$ de classe $\mathcal{C}^k(\Omega), k \geq 1$. Alors:
\[
\Omega = \{(x', x_d) \in \mathbb{R}^{d - 1} \times \mathbb{R}\,|\, x_d < \psi(x')\}
\]
où $x' = (x_1, \dots, x_{d - 1})$.

Posons $\rho(x) = x_d - \psi(x')$ et $\nabla \rho =
[- \nabla_{x'} \psi, 1]^T \neq 0$.
\end{example}

\begin{lemma}
Soit $\Omega$ ouvert à bord $\mathcal{C}^k(\Omega)$. Supposons données deux fonctions $\rho$
et $\tilde{\rho}$ de $\mathbb{R}^d$ dans $\mathbb{R}$ vérifiant les conditions de
la Définition~\ref{def:ouvert} telle que:
\[\Omega = \{x \,|\, \rho(x) < 0\} = \{x \,|\, \tilde{\rho}(x) < 0\}.\]
Alors, $\forall x \in \partial \Omega$, il existe $\lambda(x) > 0$ telle que
$\nabla \tilde{\rho} = \lambda(x) \nabla \rho$.
\end{lemma}

\begin{proof}
Fixons $x \in \partial \Omega$. Par translation, on se ramène au cas $x = 0$. On
aura $\rho(0) = \tilde{\rho}(0) = 0$ et $\nabla \rho \neq \mathbf{0}$ et
$\nabla \tilde{\rho} \neq \mathbf{0}$. Donc il existe $j$ telle que
$\pd*{\rho}{x_j} \neq 0$, par exemple $j = d$.

Il existe, par la théorème des fonctions implicite $U$ voisinage ouvert de $0$
dans $\mathbb{R}^{d - 1}$, $V$ voisinage de $0$ dans $\mathbb{R}$, $\psi \in \mathcal{C}^k(\Omega)$
($\psi: U \to V$) vérifiant $\psi(0) = 0$ telle que $(x', x_d) \in U \times V$
et $\rho(x) = 0 \Leftrightarrow x \in U \times V$ et $x_d = \psi(x')$.

Alors si $(x', x_d) \in U \times V$:
\[\rho(x', x_d) = \rho(x', \psi(x')) + \int_{\psi(x')}^{x_d} \pd{\rho}{z}(x', z) \dx[z]\]

On fait le changement de variable $z = \psi(x') + t(x_d - \psi(x'))$ avec
$t \in [0, 1]$ et on a:
\[\rho(x', x_d) = \underbrace{\rho(x', \psi(x'))}_{ = 0} +
    (x_d - \psi(x'))
    \underbrace{\int_0^1 \pd{\rho}{t}(x', \psi(x') +
                                      t(x_d - \psi(x'))) \dx[t]}_{\alpha(x)}\]

Donc $\rho(x', x_d) = (x_d - \psi(x')) \alpha(x', x_d)$ où $\alpha$ est de
classe $\mathcal{C}^{k - 1}(\Omega)$ et:
\[\alpha(0) = \int_0^1 \pd{\rho}{x_d}(0) \dx[t] = \pd{\rho}{x_d}(0) \neq 0\]

Donc si $U \times V$ est assez petit, $\alpha(x) \neq 0, \forall x \in
U \times V$. De même $\tilde{\alpha} \neq 0, \forall x \in U \times V$.
Supposons $k \geq 2$, donc $\alpha, \tilde{\alpha} \in \mathcal{C}^1(\Omega)$.
\[
\nabla \rho = (x_d - \psi(x')) \nabla \alpha + \alpha
\begin{bmatrix}
-\nabla_x \psi(x') \\
1
\end{bmatrix}
\]

Si $x \in U \times V$, on sait que $x_d = \psi(x')$. Donc:
\[
\nabla \rho = \alpha
\begin{bmatrix}
-\nabla_x \psi(x') \\
1
\end{bmatrix}
\]

De même:
\[
\nabla \tilde{\rho} = \tilde{\alpha}
\begin{bmatrix}
-\nabla_x \psi(x') \\
1
\end{bmatrix}
\]
Donc:
\[
\nabla \tilde{\rho} = \underbrace{\frac{\tilde{\alpha}(x)}{\alpha(x)}}_{\lambda(x)}
  \nabla \rho
\]

Montrons $\lambda(0) > 0$. Prenons $x \in U \times V \cap \Omega$. On a:
\[
\begin{aligned}
\rho(x) & = (x_d - \psi(x')) \alpha(x) < 0 \\
\tilde{\rho}(x) & = (x_d - \psi(x')) \tilde{\alpha(x)} < 0 \\
\frac{\rho(x)}{\tilde{\rho}(x)} & = \frac{\alpha(x)}{\tilde{\alpha(x)}} > 0
\end{aligned}
\]
si $x \to 0 \in \partial \Omega$:
\[\frac{\alpha(0)}{\tilde{\alpha}(0)} > 0 \implies \lambda(0) > 0.\]
\end{proof}

\begin{definition}
Soit $\Omega$ un ouvert à bord de classe $\mathcal{C}^k(\Omega)$. On appelle \textbf{normale
extérieure} à $\Omega$ en $x \in \partial \Omega$ tout vecteurs $\nabla \rho(x)$
où $\rho$ est une fonction comme dans la définition~\ref{def:ouvert}.

On appelle normale unitaire extérieure en $x \in \partial \Omega$:
\[\eta(x) = \frac{\nabla \rho(x)}{|\nabla \rho(x)|}\]
\end{definition}

\begin{notation}
On note:
\[\frac{\partial}{\partial \eta}\]
l'expression:
\[\sum_1^d \eta_j(x) \frac{\partial}{\partial x_j}\]
où $\eta(x) = (\eta_1(x), \dots, \eta_d(x))^T$ et:
\[\eta_j(x) = \left(\frac{1}{|\nabla \rho(x)|} \pd{\rho}{x_j}\right)\]
\end{notation}

\subsection{Théorème de Gauss}

\begin{theorem}[Théorème de Gauss]\label{thm:gauss}
Soit $\Omega$ un ouvert à bord $\mathcal{C}^k(\Omega), k \geq 1$. Il existe $\dx[\sigma]$
mesure positive sur $\partial \Omega$ telle que pour toute fonctions $F$ de
classe $\mathcal{C}^1(\Omega)$, $\phi \in \mathcal{C}^1_0(\Omega)$, on a:
\[
\int_\Omega \nabla \cdot F(x) \phi(x) \dx = - \int_\Omega F(x) \cdot \nabla \phi(x)
+ \int_{\partial \Omega}\phi(x) (F(x) \cdot \eta(x)) \dx[\sigma]
\]
\end{theorem}

\begin{proof}
~\begin{itemize}[$\blacksquare$]
    \item 1ère étape. Soit:
    \[
    T = - \sum \pd{\phi}{x_j} \pd{\ind{\Omega}}{x_j}.
    \]
    Montrons que $T$ est à support compact dans $\partial \Omega$.

    $\supp{\ind{\Omega}} = \overline{\Omega}$ d'où $\supp{\partial_{x_j}
    \ind{\Omega}} \subset \overline{\Omega}$ donc $\supp{T} \subset
    \overline{\Omega}$. Il reste à voir que $T|_{\Omega} = 0$.

    Montrons que $\dip{T, \phi} = 0, \forall \phi \in \mathcal{C}^\infty_0(\Omega)$.
\[
\begin{aligned}
\dip{T, \phi} & = -\sum \dip{\pd{\phi}{x_j} \pd{\ind{\Omega}}{x_j}, \phi} \\
& = \sum \dip{\ind{\Omega}, \pd{}{x_j}\left(\pd{\phi}{x_j} \phi \right)} \\
& = \sum \int_\Omega \pd{}{x_j}\left(\pd{\phi}{x_j} \phi \right) \dx \\
& = \sum \int_{\mathbb{R}} \underbrace{\pd{}{x_j}\left(\pd{\phi}{x_j}
                          \phi \right)}_{\in \mathcal{C}^\infty_0(\Omega)} \dx = 0\\
\end{aligned}
\]

    Donc $\supp{T} \subset \partial \Omega$.

    \item 2ème étape. Si $T \in \dist$ positive ou nulle alors $T$ est une
    mesure positive.

    Soit $\theta \in \mathcal{C}^\infty(\mathbb{R}), \theta(x) \in [0, 1]$ une
    fonction décroissante définit par:
\[
\theta(x) =
\left\{
\begin{aligned}
1 & \quad x leq -1 \\
0 & \quad x > 0 \\
decroissante & \quad x \in [-1, 0[
\end{aligned}
\right.
\]

Posons $\theta_k = \theta(k \phi(x))$. Donc:
\[T_k = - \sum \pd{\phi}{x_j} \pd{\theta_k}{x_j}\]

Montrons que $T_k \to T$ quand $k \to \infty$. Il suffit de voir que
$\theta_k \to \ind{\Omega}$:
\begin{itemize}
    \item si $x \in \Omega, \theta_k \to 1$.
    \item si $x \in \mathbb{R}^d \setminus \Omega, \theta_k \to 0$.
\end{itemize}

$\partial \Omega$ est de mesure nulle, donc:
\[
\begin{aligned}
\int \theta_k \phi & \longrightarrow \int \ind{\Omega} \phi \\
\theta_k \phi & \longrightarrow \ind{\Omega} \phi~ p.p
\end{aligned}
\]
Donc $T_k \to T$.

On vaut montrer que $T \geq 0$, i.e $\forall \phi \in
\mathcal{C}^\infty_0(\mathbb{R}^d), \phi \geq 0 \implies \dip{T, \phi} \geq 0$. Il
suffit de montrer que $T_k \geq 0$ en tout point.
\[
\begin{aligned}
T_k(x) & = - \sum_{j = 1}^d \pd{\phi}{x_j} \pd{\theta_k}{x_j} \\
& = - \sum \underbrace{\left(\pd{\phi}{x_j}\right)^2}_{ > 0}
\underbrace{k \theta'(k \phi)}_{ \leq 0 } \geq 0
\end{aligned}
\]

Donc $T$ est une distribution d'ordre 1 positive ou nulle.

    \item 3ème étape. Soit:
\[
\dx[\sigma] = \frac{T}{|\nabla \phi(x)|} = -\sum^d \eta_j(x)
 \pd{\ind{\Omega}}{x_j} = - \pd{\ind{\Omega}}{\eta}.
\]

Montrons que $\eta_j(x) \dx[\sigma] = \partial_{x_j} \ind{\Omega}$.
\[
\begin{aligned}
\frac{\eta_j}{|\nabla \phi|} T & = \lim_{k \to \infty} \frac{\eta_j}{|\nabla \phi|} T_k \\
& = \lim_{k \to \infty} -\eta_j |\nabla \phi| k \theta'(k \phi(x)) \\
& = - \lim_{k \to \infty} \pd{\phi}{x_j} k \theta'(k \phi) \\
& = - \lim_{k \to \infty} \pd{}{x_j}(\theta(k\phi)) \\
& = -\pd{}{x_j} \ind{\Omega} \text{ dans } \dist
\end{aligned}
\]
    \item 4ème étape.
\[
\begin{aligned}
\dip{\ind{\Omega}, \sum \pd{}{x_j}(F_j \phi)} & = \sum
    <\ind{\Omega}, \pd{F_j}{x_j} \phi + F_j \pd{\phi}{x_j}> \\
& = <\ind{\Omega}, \nabla \cdot F \phi + F \cdot \nabla \phi> \\
& = \int_\Omega \nabla \cdot F \phi + \int_\Omega F \cdot \nabla \phi
\end{aligned}
\]

On écrit le membre de gauche comme:
\[
\begin{aligned}
\sum \dip{\pd{\ind{\Omega}}{x_j}, F_j \phi} & =
    \sum \dip{\eta_j \dx[\sigma], F_j \phi} \\
& = \dip{\dx[\sigma], \left(\sum \eta_j F_j\right)\phi} \\
& = \dip{\dx[\sigma], (F\cdot \eta) \phi}
\end{aligned}
\]

On a obtenu:
\[
\int_{\partial \Omega} (F \cdot \eta) \phi \dx[\sigma] =
\int_\Omega \nabla \cdot F \phi \dx + \int_\Omega F \cdot \nabla \phi \dx
\]
\end{itemize}
\end{proof}

\subsection{Example de calcul de \texorpdfstring{$\dx[\sigma]$}{ds}}

Soit:
\[\Omega = \{ (x', x_d) \in \mathbb{R}^d \,|\, x_d > \psi(x')\}\]
Soit $\dx[\sigma]$ la mesure sur $\partial \Omega$. On a aussi:
\[
\begin{aligned}
\rho(x) & = & \psi(x') - x_d \\
\nabla \phi & = & (\nabla \psi(x'), -1)^T \\
|\nabla \phi|^2 & = & |\nabla \psi(x')|^2 + 1 = D(x')^2
\end{aligned}
\]

\begin{lemma}
Soit $\phi \in \mathcal{C}^0_0(\mathbb{R}^d)$ telle que:
\[\int_{\partial \Omega} \phi \dx[\sigma] = \int_{\mathbb{R}^{d - 1}}
\phi(x', \psi(x')) \sqrt{1 + |\nabla \psi(x')|^2} \dx{x'}\]
\end{lemma}

Donc:
\[
\begin{aligned}
\dx[\sigma] & = -\pd{}{\eta} \ind{\Omega} \\
& = -\sum^d \pd{\phi}{x_j} \frac{1}{|\nabla \phi|} \pd{\ind{\Omega}}{x_j} \\
& = -\sum^{d - 1} \pd{\psi}{x_j} \frac{\partial_{x_j} \ind{\Omega}}{D(x')} +
\frac{\partial_{x_d} \ind{\Omega}}{D(x')}
\end{aligned}
\]

Donc $\forall \phi \in \mathcal{C}^\infty_0(\mathbb{R}^d)$, on a:
\[
\dip{\dx[\sigma], \phi} = -\sum \dip{
    \ind{\Omega}, \pd{}{x_j} \left(\frac{\partial_{x_j} \psi}{D(x')} \phi\right)}
    - \dip{\ind{\Omega}, \pd{}{x_j}\left(\frac{\phi}{D(x')}\right)}
\]

Posons $g_j = \frac{1}{D(x')} (\partial_{x_j}\psi) \phi$ et
$g_d = \frac{1}{D(x')} \phi$:
\[
\dip{\ind{\Omega}, \partial_{x_j} g_j} =
    \int_{\partial \Omega} \partial_{x_j} g_j \dx
\]

Pour $j = 1$, on a:
\[
\begin{aligned}
\dip{\ind{\Omega}, \partial_{x_1} g_1} & =
    \int_{x_d > \psi(x')} \pd{g_1}{x_1} \\
& = \int_{z > 0} \pd{g_1}{x_1}(x_1, \dots, z + \psi(x')) \dx{z} \\
& = \int_{z > 0} \pd{g_1}{x_1} - \pd{g_1}{x_1} \pd{\psi}{x_1} \dx \\
& = \int \left(\int_{z > 0} -\pd{g_1}{x_1} \dx{z}\right) \pd{\psi}{x_1} \dx{x'} \\
& = \int g_1(x_1, \dots, \psi(x')) \partial_{x_1} \psi \dx{x'}
\end{aligned}
\]

Donc:
\[
\begin{aligned}
\dip{\ind{\Omega}, \partial_{x_1} g_1} & =
    \int g_1(x', \psi(x')) \partial_{x_1} \psi \dx{x'} \\
& = \int \frac{1}{D(x')} \left(\pd{\psi}{x_1}\right) \phi(x', \psi(x')) \dx
\end{aligned}
\]

Pour $j \in \llbracket 2, d - 1 \rrbracket$, on a:
\[
\dip{\ind{\Omega}, \partial_{x_j} g_j} =
    \int \frac{1}{D(x')}
    \left(\pd{\psi}{x_j}\right) \phi(x', \psi(x')) \dx\]

Pour $j = d$, on a:
\[
\dip{\ind{\Omega}, \partial_{x_d} g_d} = - \int \frac{\phi}{D(x')} \dx
\]

Donc:
\[
\begin{aligned}
\dip{\dx[\sigma], \phi} & = \int \frac{1}{D(x')}
    \left[\sum^{d - 1} |\partial_{x_j} \psi|^2 + 1\right] \phi \dx{x'} \\
& = \int D(x') \phi(x', \psi(x')) \dx{x'}
\end{aligned}
\]

\subsection{Formule de Green pour le laplacien}
\begin{theorem}[Formule de Green pour le laplacien]\label{thm:green}
Soient $u \in \mathcal{C}^2(\mathbb{R}^d), v \in \mathcal{C}^2_0(\mathbb{R}^d)$. Soit $\Omega$
un ouvert à bord de classe $\mathcal{C}^1$. On a:
\[
\int_\Omega (\Delta u) v \dx = -\int_\Omega u (\Delta v) \dx +
    \int_{\partial \Omega} \left(\pd{u}{\eta} v - \pd{v}{\eta} u \dx[\sigma]\right)
\]
\end{theorem}

\begin{proof}
à faire...
\end{proof}

% kate: default-dictionary fr_FR;
