% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Introduction et rappels}

\section{Rappels sur les espaces \texorpdfstring{$L^p$}{Lp}}

Soit $\Omega$ un ouvert de $\mathbb{R}^d$. On peut définir les espaces suivantes:
\begin{itemize}
    \item si $p = 1$:
\[
\begin{aligned}
L^1(\Omega) & = \left\{
    f: \Omega \to \mathbb{C} \mid f \text{ mesurable},
    \int_\Omega |f(x)| \dx < +\infty
\right\} \\
L^1(\Omega) & = \{\text{ l'espace des fonctions intégrables }\}.
\end{aligned}
\]
    \item si $p \in [1, \infty [$:
\[
\begin{aligned}
L^p(\Omega)     & = \left\{
    f: \Omega \to \mathbb{C} \mid f \text{ mesurable}, |f(x)|^p \text{ intégrable}
    \right\} \\
\|f\|_{L^p}  & = \left(\int_\Omega |f(x)|^p \dx[x] \right)^\frac{1}{p}
\end{aligned}
\]
    \item si $p = \infty$:
\[
\begin{aligned}
L^\infty(\Omega)    & = \left\{
    f: \Omega \to \mathbb{C} \mid
    f \text{ mesurable}, \exists
    \lambda \in \mathbb{R} \text{ telle que }
    \measure(\{ x \mid f(x) > \lambda\}) = 0
    \right\} \\
\|f\|_{L^\infty} & = \inf \{\lambda \mid \measure\{x \mid f(x) > \lambda\} = 0\}
\end{aligned}
\]
\end{itemize}

\begin{notation}
Si $A \subset \Omega$ est mesurable, on note $\ind{A}$ la fonction
caractéristique de $A$.
\end{notation}

\begin{notation}
On note $\lploc{p}$ comme l'espace de fonctions mesurable
$f: \Omega \to \mathbb{C}$ tel que $\forall K \subset \Omega$ compact, $\ind{K}
f \in L^p(\Omega)$.
\end{notation}

\begin{theorem}[Inégalité de Hölder]\label{thm:holder}
Soient $p, q, r \in [1, \infty]$ telle que $\frac{1}{p} + \frac{1}{q} =
\frac{1}{r}$. Si $f \in L^p, g \in L^q$ et $fg \in L^r$, on a:
\[
\|fg\|_{L^r} \leq \|f\|_{L^p} \|g\|_{L^q}
\]
\end{theorem}

\begin{example}
Soit $\Omega = ]0, 1[$. Soit $f(x) = \frac{1}{x}$. Alors $f \notin L^1(]0, 1[)$,
mais $f(x) \in \lploc[\text{]0, 1[}]{1}$.

% https://docs.google.com/drawings/d/1qdMKUeezcRYA_ugxANftqqR1S-l1X1gPPty3IrnM-wI/edit?usp=sharing
\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.3\linewidth]{img/fct1x}
\end{figure}
\end{example}

\section{Théorème de convergence dominée}

\begin{theorem}[Théorème de convergence dominée]\label{thm:tcd}
Soient $\Omega$ un ouvert de $\mathbb{R}^d$ et $f_n: \Omega \to \mathbb{C}$ une
suite de fonctions. On suppose que $\forall n, f_n$ est mesurable et:
\begin{enumerate}[(i)]
    \item il existe $\Gamma \subset \Omega$ de mesure nulle telle que $\forall
    x \in \Omega \setminus \Gamma, \lim f_n(x) = f(x)$ existe.
    \item il existe $g: \Omega \to \mathbb{R}_+$ intégrable et indépendante
    de $n$ telle que $\forall n \in \mathbb{N}, \forall x \in \Omega
    \setminus \Gamma$:
    \[
    |f_n(x)| \leq g(x)
    \]
\end{enumerate}
Alors:
\[
\lim_{n \to \infty} \int_\Omega f_n(x) \dx =
    \int_\Omega \lim_{n \to \infty} f_n(x) \dx =
    \int_\Omega f(x) \dx.
\]
\end{theorem}

\begin{remark}
Souvent, on peut prendre $\Gamma = \emptyset$.
\end{remark}

\begin{theorem}[Continuité sous l'intégrale]
Soient $\Omega$ un ouvert de $\mathbb{R}^d, Y$ un espace métrique et
$f: \Omega \times Y \to \mathbb{C}$. On suppose:
\begin{enumerate}[(i)]
    \item pour tout $y$ fixé: $x \mapsto f(x, y)$ est intégrable.
    \item soit $y_0 \in Y$. Il existe $\Gamma \subset \Omega$ de mesure nulle,
    telle que $\forall x \in \Omega \setminus \Gamma$, $y \mapsto f(x, y)$ est
    continue en $y = y_0$.
    \item $\exists g: \Omega \to \mathbb{R}_+$ intégrable, indépendante de $y$,
    telle que $\forall x \in \Omega \setminus \Gamma, \forall y \in Y$:
    \[
    |f(x, y)| \leq g(x).
    \]
\end{enumerate}
Alors $F(y) = \displaystyle \int_\Omega f(x, y) \dx$ est continue en $y_0$.
\end{theorem}

\begin{remark}
Si on suppose:
\begin{enumerate}
    \item[(ii)'] $\exists \Gamma \subset \Omega$ de mesure nulle telle que
    $\forall x \in \Omega \setminus \Gamma$, $y \mapsto f(x, y)$ soit
    continue en $y$.
\end{enumerate}
Alors, $y \mapsto F(y)$ est continue en $y$.
\end{remark}

\begin{theorem}[Dérivabilité sous l'intégrale]
Soient $\Omega$ un ouvert de $\mathbb{R}^d$, $Y$ un ouvert de $\mathbb{R}^d$
et $f: \Omega \times Y \to \mathbb{R}$. Supposons:
\begin{enumerate}[(i)]
    \item $\forall y \in Y$ fixé, $x \mapsto f(x, y)$ est intégrable.
    \item il existe $\Gamma \subset \Omega$ de mesure nulle, telle que
    $\forall x \in \Omega \setminus \Gamma$, $y \mapsto f(x, y)$ est de
    classe $\mathcal{C}^1$ (les dérivées partielles $\pd*{f}{y_j}$ existent et
    sont continues).
    \item il existe $g: \Omega \to \mathbb{R}_+$ intégrable et indépendante
    de $y$, telle que $\forall j \in \mathbb{N}, \forall y \in Y, \forall x \in
    \Omega \setminus \Gamma$:
    \[
    \left| \pd{f}{y_j} \right| \leq g(x).
    \]
\end{enumerate}

Alors $F(y) = \displaystyle \int_\Omega f(x, y) \dx$ est $\mathcal{C}^1$
et:
\[
\pd{F}{y_j} = \int_\Omega \pd{f}{y_j} \dx
\]
\end{theorem}

\begin{remark}
La condition de domination porte sur les dérivées $\pd*{f}{y_j}$.
\end{remark}

\begin{notation}
Soit $h: Y \to \mathbb{C}$ de classe $\mathcal{C}$, $\mathbf{\alpha} =
(\alpha_1, \dots, \alpha_d) \in \mathbb{R}^d$ un multi-indice. Si la longueur
de $\alpha$, $|\alpha| = \sum \alpha_i \leq k$, on pose:
\[
\pd{^\alpha h}{^\alpha y} =
    \pd{^\alpha h}{y^\alpha} =
    h^{(\alpha)}(y) =
    \frac{\partial^{|\alpha|} h}{\partial^{\alpha_1} y_1 \dots \partial^{\alpha_d} y_d}
\]
\end{notation}

\begin{theorem}[Dérivabilité multiple sous l'intégrale]
Soient $k \in \mathbb{N}^*$, $f: \Omega \times Y \to \mathbb{C}$. On suppose:
\begin{enumerate}[(i)]
    \item $\forall y \in Y$ fixé, $x \mapsto f(x, y)$ est intégrable.
    \item il existe $\Gamma \subset \Omega$ de mesure nulle, telle que
    $\forall x \in \Omega \setminus \Gamma$, $y \mapsto f(x, y)$ est de
    classe $\mathcal{C}$.
    \item il existe $g: \Omega \to \mathbb{R}_+$ intégrable et indépendante
    de $y$, telle que $\forall \alpha \in \mathbb{N}^d, |\alpha| \leq k, \forall y
    \in Y, \forall x \in \Omega \setminus \Gamma$:
    \[
    \left|\partial^\alpha_y f(x, y)\right| \leq g(x)
    \]
\end{enumerate}
Alors, $y \mapsto F(y)$ est de classe $\mathcal{C}^k$ et $\forall \alpha \in
\mathbb{N}^d$:
\[
\partial^\alpha_y F(y) = \int_\Omega \partial^\alpha_y f(x, y) \dx.
\]
\end{theorem}

\section{Dualité dans les espaces \texorpdfstring{$L^p$}{Lp}}

Soit $p \in [1, \infty]$. On définit $q \in [1, \infty]$ telle que $\frac{1}{p} +
\frac{1}{q} = 1$ ($q$ est l'exposant conjugué de $p$). Soit $f \in L^p(\Omega)$
et soit:
\[
\deffunction{l_f}{L^q(\Omega)}{\mathbb{C}}{g}{l_f(g) = \int_\Omega f(x)g(x) \dx}
\]
intégrable. On a aussi:
\[
|l_f(g)| \leq \|f\|_{L^p} \|g\|_{L^q} \leq c(f) \|g\|_{L^q}
\]

Donc, à $f$ fixé, $g \mapsto l_f(g)$ est une application linéaire continue.

\begin{notation}
On note $(L^q(\Omega))'$ le dual topologique de $L^q(\Omega)$, i.e. l'ensemble
des applications linéaires $l:L^q(\Omega) \to \mathbb{C}$, telle que:
\[
\exists c > 0, \forall g \in L^q(\Omega), |l(g)| \leq c \|g\|_{L^q}
\]
\end{notation}

On veut vérifier que si $f \in L^p(\Omega), l_f \in (L^q(\Omega))'$. On a
donc défini:
\[
\deffunction{\Phi}{L^p(\Omega)}{(L^q(\Omega))'}{f}{\Phi(f) = l_f}
\]

\begin{theorem}
Supposons $1 \leq q < \infty$. Alors, si $p$ vérifie $\frac{1}{p} +
\frac{1}{q} = 1$, l'application $\Phi$ est bijective et $\forall f \in
L^p(\Omega)$:
\[
\|f\|_{L^p} = \|l_f\|_{(L^q(\Omega))'} =
    \sup_{\|g\|_{L^q} \leq 1} |l_f(g)|
\]
\end{theorem}

\begin{example}
~\begin{itemize}
    \item $p = q = 2$: $L^2(\Omega)$ s'identifie à $(L^2(\Omega))'$.
    \item $p = \infty, q = 1$: $L^\infty(\Omega)$ s'identifie à
    $(L^1(\Omega))'$.
\end{itemize}
\end{example}

On peut donc considère un élément de $L^\infty(\Omega)$ de deux points de vue
distinct:
\begin{itemize}
    \item comme une fonction $f: \Omega \to \mathbb{C}$.
    \item comme un élément de $(L^1(\Omega))'$, donc comme une forme
    linéaire continue.
\end{itemize}

On va définir les distributions en généralisant le second point de vue.

\section{Espaces topologiques}

\begin{notation}
$\mathcal{P}(E)$ est l'ensemble des parties de $E$.
\end{notation}

\begin{definition}
Un espace topologique est un couple $(E, \mathcal{O})$ où $\mathcal{O}
\in \mathcal{P}(E)$, vérifiant:
\begin{enumerate}[(i)]
    \item $\emptyset$ et $E$ sont dans $\mathcal{O}$.
    \item si $(U_i)_{i \in I}$ est une famille d'éléments de $\mathcal{O}$,
    alors $\bigcup U_i \subset \mathcal{O}$.
    \item si $U$ et $V$ sont dans $\mathcal{O}$, alors $U \cap V \in
    \mathcal{O}$.
\end{enumerate}
\end{definition}

On dit que les éléments de $\mathcal{O}$ sont les ouvert de la topologie.

\begin{definition}
On dit que $F \subset E$ est fermé si et seulement si $E \setminus F$ est
ouvert.
\end{definition}

\begin{property}
Propriétés des fermés:
\begin{enumerate}[(i)]
    \item $\emptyset$ et $E$ sont fermés.
    \item $(F_i)_{i \in I}$ est une famille de fermés, $\bigcap F_i$ est
    fermé.
    \item $F$ est $G$ sont deux fermés, $F \cap G$ est fermé.
\end{enumerate}
\end{property}

\begin{definition}
$\mathcal{O}$ est ouvert si et seulement si $\forall a \in \mathcal{O},
\exists r > 0$ avec $\mathcal{B}(a, r) \subset \mathcal{O}$.
\end{definition}

\begin{example}
Si $(E, \lambda)$ est un espace métrique, on dit que $U \subset E$ est ouvert
si et seulement si $\forall a \in U, \exists r > 0$ avec $\mathcal{B}(a, r)
\subset U$.
\end{example}

\begin{definition}
Soit $(E, \mathcal{O})$ une espace topologique. On dit que $V$ est
voisinage de $a \in E$ si et seulement si il existe $U$ ouvert avec
$a \in U \subset V$.
\end{definition}

\begin{property}
Propriétés des voisinages:
\begin{enumerate}[(i)]
    \item si $V_1$ est $V_2$ sont voisinages de $a$, $V_1 \cap V_2$ est
    voisinage de $a$.
    \item si $V \subset W$ est voisinage de $a$, $W$ est voisinage de $a$.
    \item $U$ est ouvert si et seulement si $U$ est voisinage de chaque
    de ses points.
\end{enumerate}
\end{property}

\subsection{Topologie associée à la donnée d'une famille de semi-normes}

Soit $E$ un espace vectoriel.

\begin{definition}
Une semi-norme sur $E$ est une application $p: E \to \mathbb{R}_+$ vérifiant:
\begin{enumerate}[(i)]
    \item $\forall x \in E, \forall \lambda \in \mathbb{C}, p(\lambda x) =
    |\lambda| p(x)$.
    \item $\forall x, y \in E, p(x + y) \leq p(x) + p(y)$
\end{enumerate}
(on ne suppose pas que $p(x) = 0 \implies x = 0$).
\end{definition}

Si $p$ est une semi-norme sur $E$, $a \in E$ et $r > 0$, la pseudo-boule
de centre $a$ et de rayon $r$ pour la semi-norme $p$ est:
\[
\mathcal{B}_p(a, r) = \{x \in E \,|\, p(x - a) < r\}
\]

\begin{definition}
Soient $E$ une espace vectoriel et $(p_i)_{i \in I}$ une famille de semi-normes
sur $E$. On dira que $U \in E$ est ouvert si et seulement si $\forall a \in U$,
il existe $J \subset I$, sous-ensemble fini, et $r > 0$ telle que:
\[\displaystyle \bigcap_{i \in J}\mathcal{B}_{p_i}(a, r) \subset U.\]
On obtient ainsi une topologie sur $E$.
\end{definition}

\begin{remark}
$V \subset E$ est voisinage de $a$ si et seulement si $\exists J \subset I$
fini et $r > 0$ telle que:
\[
\bigcap_{i \in J}\mathcal{B}_{p_i}(a, r) \subset U.
\]
\end{remark}

\subsection{Convergence des suites}
\begin{definition}
Soit $(E, \mathcal{O})$ un espace topologique. Une famille $(x_n)$ converge
vers $a$ si et seulement si $\forall V$ voisinage de $a$, $\exists n_0,
\forall n \geq n_0, x_n \in V$.
\end{definition}

\begin{proposition}
Considérons $E$ un espace vectoriel, muni de la topologie associée à une
famille de semi-normes $(p_i)$.

Une suite $(x_n)$ converge vers $a$ si et seulement si $\forall i \in I$:
\[
p_i(x_n - a) \xrightarrow{n \to \infty} 0
\]
\end{proposition}

\section{Espaces de fonctions différentiables}

\subsection{Les espaces \texorpdfstring{$\mathcal{C}^*$}{Ck}}

Soit $\Omega$ un ouvert de $\mathbb{R}^d$:
\[
\begin{aligned}
\mathcal{C}^0 & = \{u: \Omega \to \mathbb{C} \mid u \text{ continue}\} \\
\mathcal{C}^1 & = \left\{
    u: \Omega \to \mathbb{C} \mid
    \pd{u}{x_j} \text{ existe et est dans } \mathcal{C}^0
    \right\} \\
\vdots        & \\
\mathcal{C}^k & = \left\{
    u: \Omega \to \mathbb{C} \mid
    \pd{u}{x_j} \text{ existe et est dans } \mathcal{C}^{k - 1}
    \right\} \\
\mathcal{C}^\infty & = \bigcap_{k \in \mathbb{N}} \mathcal{C}^k
\end{aligned}
\]

Si $\alpha = (\alpha_1, \dots, \alpha_d) \in \mathbb{N}^d$, $|\alpha| =
\sum \alpha_i \in \mathbb{N}$ et $\alpha ! = (\alpha_1 !)\cdots(\alpha_d !)$.

Si $\alpha, \beta \in \mathbb{N}^d$, on écrit $\beta \leq \alpha$ si et
seulement si $\beta_1 \leq \alpha_1, \dots, \beta_d \leq \alpha_d$.

On pose:
\[
\binom{\alpha}{\beta} = \prod_{j = 1}^d \binom{\alpha_j}{\beta_j} =
                        \frac{\alpha !}{(\alpha - \beta)! \beta !}
\]

\begin{remark}
$\mathcal{C}$ est un espace vectoriel et une algèbre ($u, v \in \mathcal{C}^k
\implies uv \in \mathcal{C}$) et, si $|\alpha| \leq k$, on a la formule de
Leibniz:
\[
\pd{^\alpha}{x^\alpha} (uv) = \sum_{\beta \leq \alpha}
    \binom{\alpha}{\beta} \pd{^{\alpha - \beta} u}{x^{\alpha - \beta}}
    \pd{^\beta v}{x^\beta}.
\]
\end{remark}

\begin{notation}
Soient $x = (x_1, \dots, x_d) \in \mathbb{R}^d$, on pose $|x| = \sqrt{x_1^2 + \dots
x_d^2}$ et $F \subset \mathbb{R}^d$. On définit la distance entre $x$ et l'ensemble $F$
par $d(x, F) = \inf_{y \in F} |x - y|$.

\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[scale=0.8]
        \draw [thick, domain=-30:120] plot ({2 * cos(\x)}, {2 * sin(\x)});
        \draw[fill] (-0.5, 1) circle [radius=0.055];
        \draw[fill] (3, 2) circle [radius=0.055];
        \draw [dashed, thick] (-0.5, 1) -- (3, 2);
        \node at (-0.5, 0.7) {$y$};
        \node at (3.2, 2) {$x$};
        \node at (0, 0) {{\Large $F$}};
    \end{tikzpicture}
    \caption{Distance entre $x$ et $y \in F$.}
\end{figure}
\end{notation}

\begin{definition}\label{def:suitekn}
Soit $\Omega$ un ouvert de $\mathbb{R}^d$. Pour tout $n \in \mathbb{N}^*$, posons:
\[
K_n = \left\{x \in \Omega \,\middle|\,
    |x| \leq n \text{ et }
    d(x, \mathbb{R}^d \setminus \Omega) \geq \frac{1}{n}
    \right\}
\]

\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[scale=0.6]
        \draw [thick, domain=-30:120] plot ({4 * cos(\x)}, {4 * sin(\x)});
        \draw [thick] plot [smooth] coordinates {(0, 5) (-2, -2) (5, 0)};
        \draw [thick] plot [smooth] coordinates {(-0.6, 5) (-2.6, -2.6) (5, -0.6)};
        \draw [<->] [thick] (-2.6, -2.6) -- (-2, -2);
        \draw [->] [thick, dashed] (-5.6, -2) -- (2, 3.4);
        \draw (-5.7, -2.1) circle [radius=0.2];
        \node at (-1.9, -2.4) {$\frac{1}{n}$};
        \node at (1, 1) {$K_n$};
        \node at (-3.6, -0.2) {$n$};
    \end{tikzpicture}
    \caption{Suite de $K_n$.}
\end{figure}

Alors $(K_n)$ est une suite de compacts de $\mathbb{R}^d$, inclus dans $\Omega$
et:
\begin{enumerate}[(i)]
    \item $K_n \subset \mathring{K}_{n + 1}, \forall n \in \mathbb{N}^+$
    \item $\Omega= \bigcup K_n = \bigcup \mathring{K}_{n + 1}$
    \item pour tout compact $K$ de $\mathbb{R}^d$ inclus dans $\Omega$, il
    existe $n_o \in \mathbb{N}^*$ telle que $\forall n \geq n_0, K \subset K_n$.
\end{enumerate}
\end{definition}

On dit qu'une suite $K_n$ vérifiant $(i), (ii), (iii)$ est une suite
exhaustive de compacts sur $\Omega$.

\subsection{Topologie sur \texorpdfstring{$\mathcal{C}^k, \mathcal{C}^\infty$}{Ck, Cinf}}

Si $k \in \mathbb{N}$, on pose, pour $n \in \mathbb{N}^*, u \in \mathcal{C}$:
\[
p_n(u) = \sum_{|\alpha| \leq k} \sup_{x \in K_n} \left|\pd{^\alpha u}{x^\alpha}\right|
\]

Si $k = \infty$, on pose:
\[
p_n(u) = \sum_{|\alpha| \leq n} \sup \left|\pd{^\alpha u}{x^\alpha}\right|
\]

On obtient des formules de semi-normes sur $\mathcal{C}^k$ et $\mathcal{C}^\infty$.
On muni $\mathcal{C}^k$ et $\mathcal{C}^\infty$ de la topologie associée à
cette famille de semi-normes.

\begin{remark}
On a $\forall u \in \mathcal{C}^k, p_n(u) \leq p_{n + 1}(u)$. Alors, $V$ est voisinage
de $0$ dans $\mathcal{C}^k$ si et seulement si $\exists r > 0, n \in \mathbb{N}^*$ telle que:
\[
\{u \mid p_n(u) < r\} \subset V
\]
\end{remark}

La définition générale dit que $V$ est voisinage de $0$ si et seulement
si $\exists J = \{n_1, \dots, n_l\}, r > 0$ telle que:
\[
\bigcup_{j = 1}^l \mathcal{B}_{p_{n_j}}(0, r) \subset V
\]

Si on suppose $n_1 \leq \dots \leq n_l$, on a $p_{n_1} \leq \dots p_{n_l}$
donc $p_{n_l}(u) < r $, donc:
\[
\bigcap \mathcal{B}_{p_{n_j}}(0, r) = \mathcal{B}_{p_{n_l}}(0, r)
\]

\begin{proposition}
Soit $k \in \mathbb{N}$ ou $k = \infty$. Soit $\{u_j\}$ une suite de $\mathcal{C}^k$.
Soit $u \in \mathcal{C}^k$. Il y a équivalence entre:
\begin{enumerate}[(i)]
    \item $(u_j)$ converge vers $u$ pour la topologie de $\mathcal{C}^k$.
    \item pour tout compact $K$ de $\mathbb{R}^d$ inclus dans $\Omega$ et
    tout $\alpha \in \mathbb{N}^d, |\alpha| \leq k$, $\pd*{^\alpha u_j}{x}$
    converge vers $\pd*{^\alpha u}{x}$ uniformément sur $K$, i.e.
    \[
    \sup_{x \in K} \left|\pd{^\alpha u_j}{x^\alpha} - \pd{^\alpha u}{x^\alpha}\right| \xrightarrow{j \to \infty} 0
    \]
\end{enumerate}
\end{proposition}

\begin{remark}
On peut construire une distance sur $\mathcal{C}^k$. On a donc une notion de
suite de Cauchy. On peut aussi les définir directement à l'aide des
semi-normes.
\end{remark}

\begin{definition}
Soit $(u_j)$ une suite de $\mathcal{C}^k$. On dit que $(u_j)$ est une suite de
Cauchy si et seulement si $\forall \epsilon > 0, \forall n \in \mathbb{N}$,
$\exists j_0$ et $\forall j, j' \geq j_0$:
\[
p_n(u_j - u_{j'}) < \epsilon
\]
\end{definition}

\begin{theorem}
~\begin{itemize}
    \item $\mathcal{C}^k$ est complet (i.e toute suite de Cauchy converge).
    \item $\mathcal{C}^0(K_n)$ est complet.
\end{itemize}
\end{theorem}

\begin{proof}
Soit $(u_j)$ de Cauchy (cas $k \in \mathbb{N}$). Soit $\epsilon > 0, n \in \mathbb{N}$. Il
existe $j_0$ et $\forall j, j' < j_0$ on a:
\[
p_n(u_j - u_{j'}) = \sum_{|\alpha| \leq k}
    \sup_{K_n} \left|\pd{^\alpha u_j}{x^\alpha} - \pd{^\alpha u_{j'}}{x^\alpha}\right| \leq \epsilon
\]

Soit $\mathcal{C}^0(K_n)$ l'espace des fonctions $f: K_n \to \mathbb{C}$ continues, muni de
la norme $\|f\|_n = \sup |f(x)|$. On sait que $\mathcal{C}^0(K_n)$ est complet. On
a $\|\pd*{^\alpha u_j}{x} - \pd*{^\alpha u_{j'}}{x}\|_n \leq p_n(u_j - u_{j'})$. Donc
$(\pd*{^\alpha u_j}{x})$ est de Cauchy dans $\mathcal{C}^0(K_n)$ donc convergente vers
une limite $v^{n, (\alpha)} \in \mathcal{C}^0(K_n)$.

En particulier, $(u_j)$ converge uniformément sur $\mathring{K}_n$ ainsi que
ses dérivées d'ordre inférieur à $k$. La limite $v^{n, (0)}$ de $(u_j)$ est
donc de classe $\mathcal{C}^*$ sur $\mathring{K}_n$.

Donc $u_j|_{\mathring{K}_n}$ converge uniformément ainsi que ses dérivées
d'ordre inférieur à $k$ sur $\mathring{K}_n$ vers une limite $v^{n, (0)}$ qui
à priori dépend de $n$. On a $u_j|_{\mathring{K}_n} =
(u_j|_{\mathring{K}_{n + 1}})|_{\mathring{K}_n}$. Mais
$u_j|_{\mathring{K}_n} \to v^{n, (0)}$ et $u_j|_{\mathring{K}_{n + 1}}
\to v^{n + 1, (0)}$. Donc $v^{n, (0)} = v^{n + 1, (0)}|_{\mathring{K}_n}$.

On définit $v: \Omega \to \mathbb{C}$ en posant $v|_{\mathring{K}_n} = v^{n, (0)}$.
On a montré que $\pd*{^\alpha u_j}{x} \to \pd*{^\alpha v}{x}$ uniformément
sur $\mathring{K}_n$ pour tout $n$.

Comme $K_{n - 1} \subset \mathring{K}_n$ on a $\pd*{^\alpha u_j}{x} \to
\pd*{^\alpha v}{x}$ uniformément sur $K_{n - 1}$ pour tout $n$ et tout $\alpha$,
avec $|\alpha| \leq k$. Donc:
\[
u_j \to v \text{ dans } \mathcal{C}^k.
\]
\end{proof}

\section{Espace des fonctions \texorpdfstring{$\mathcal{C}^k$}{Ck} à support compact}

\begin{definition}
Soit $u \in \mathcal{C}^0$. On appelle support de $u$ l'ensemble noté:
\[
\supp{u} = \Omega \cap F, F = \overline{\{x \in \Omega \mid u(x) \neq 0\}}
\]
\end{definition}

Soit $x \in \Omega \setminus \supp{u}$ dans la figure~\ref{fig:supp}. On
dit que $x \notin F$ si et seulement si $\exists V$ voisinage ouvert
de $x$ telle que $V \cap \supp{u} = \emptyset \Leftrightarrow \exists V$
voisinage ouvert de $x$ tel que $u|_{V} = 0$.

\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[scale=0.35]
        \draw (0, 0) circle [radius=5];
        \draw (3, 2) circle [radius=1];
        \draw[fill] (3, 2) circle [radius=0.06];
        \draw [thick] plot [smooth] coordinates {(0, 5) (0, 0) (4, -3)};
        \node at (-2, -1) {\large $\Omega \cap F$};
        \node at (3, -0.5) {\large $\mathcal{O}$};
        \node at (3, 1.5) {$x$};
        \node at (1.6, 2) {$V$};
    \end{tikzpicture}
    \caption{Support de $u$.}
    \label{fig:supp}
\end{figure}

\begin{definition}
Soit $k \in \mathbb{N}$ ou $k = \infty$. On note $\mathcal{C}^k_0$ l'espace des
fonctions $u: \Omega \to \mathbb{C}$, de classe $\mathcal{C}$ telle que $\supp{u}$
soit un compact de $\mathbb{R}^d$ inclus dans $\Omega$.
\end{definition}

\begin{example}
Soit $\Omega = ]-1, 1[$ et $u(x) = 1 - |x|$:

% TODO: bad image
\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.4\linewidth]{./img/fct1absx.jpg}
    \caption{Fonction à support compact.}
\end{figure}

Donc $F = [-1, 1]$ et $\supp{u} = ]-1, 1[ \implies u \notin \mathcal{C}^0_0$.

Si on prend $\Omega = ]-2, 2[$ et:
\[
u(x) = \left\{
\begin{array}{ll}
1 - |x| & \quad |x| \leq 1 \\
0 & \quad sinon
\end{array}
\right.
\]

On obtient $F = [-1, 1]$ et $\supp{u} = [-1, 1]$ compact inclus dans $\Omega$,
donc $u \in \mathcal{C}^0_0$.
\end{example}

\begin{remark}
Soit $u \in \mathcal{C}^k_0(\mathbb{R}^d)$. Définissons $\tilde{u}: \mathbb{R}^d \to \mathbb{C}$ en
posant:
\[
\tilde{u}(x) = \left\{
\begin{array}{ll}
u(x) & \quad x \in \Omega \\
0 & \quad sinon
\end{array}
\right.
\]

Alors, la fonction $\tilde{u} \in \mathcal{C}^k_0(\mathbb{R}^d)$.
\end{remark}

\begin{theorem}
Soient $\Omega$ un ouvert de $\mathbb{R}^d$ et $K \subset \Omega$ un compact.
Soit $\mathcal{O}$ un ouvert avec $K \subset \mathcal{O} \subset
\overline{\mathcal{O}} \subset \Omega$. Il existe $\phi \in \mathcal{C}^k_0$ avec
$\supp{\phi} \subset \mathcal{O}$.
\end{theorem}

\begin{example}
Soit $\phi \equiv 1$ sur $K$ et $0 \leq \phi \leq 1$.

% TODO: add image.
\end{example}

\section{Partitions de l'unité}

\begin{theorem}
Soit $(\omega_j)$ une famille d'ouverts de $\mathbb{R}^d$ vérifiant:
\begin{enumerate}[(i)]
    \item $\forall j, \omega_j$ est un compact.
    \item $\mathbb{R}^d = \bigcup^\infty \omega_j$.
    \item $\forall x_0 \in \mathbb{R}^d, \exists V$ voisinage ouvert de $x_0$ et
    $N \in \mathbb{N}$ tel que $\forall j, \omega_j \cap V = \emptyset$.
\end{enumerate}

Il existe alors une famille $(\phi_j)$ de fonctions vérifiant $\forall j$:
$0 \leq \phi_j \leq 1$, $\phi_j \in \mathcal{C}^\infty_0(\omega_j)$
et $\sum \phi_j(x) = 1, \forall x \in \mathbb{R}^d$.
\end{theorem}

\begin{remark}
Pour tout $x_0$ fixé, il existe $N = N(x_0)$ tel que $\phi_j(x_0) = 0$ si
$j = N$ (puisque si (iii) est vérifiée et $j > N, \supp{\phi_j} \cap V =
\emptyset$ donc $\phi_j = 0$ si $j > N$). Donc $\sum \phi_j(x_0)$ est une
somme finie:
\[
\sum_0^N \phi_j(x_0).
\]
\end{remark}

\begin{corollary}
Soient $K$ un compact de $\mathbb{R}^d$ et $(\omega_j)$ une famille d'ouverts. Alors
$K \subset \bigcup \omega_j$. Il existe $\phi_j \in \mathcal{C}^\infty_0(\omega_j)$
telle que:
\[
\sum^N \phi_j(x) \leq 1 \text{ pour tout } x \in \mathbb{R}^d
\]
et $\forall x \in K$:
\[
\sum^N \phi_j(x) = 1.
\]
\end{corollary}

\section{Approximation par troncature et par convolution}

\begin{notation}
Soit $L^p_0(\Omega)$ l'espace $L^p$ à support compact, i.e. les fonctions
de $L^p$ telles qu'il existe $K$ compact de $\mathbb{R}^d, K \subset \Omega$ tel
que $u = 0$ p.p. sur $\Omega \setminus K$.
\end{notation}

En particulier $u = u \ind{K}$.

\begin{proposition}
~\begin{enumerate}[(i)]
     \item soit $p \in [1, \infty[$. $L^p_0(\Omega)$ est dense dans $L^p(\Omega)$.
     \item soit $k \in \mathbb{N}$. Alors $\mathcal{C}^k_0$ est dense dans $\mathcal{C}$ (pour la
     topologie introduite sur $\mathcal{C}$).
 \end{enumerate}
\end{proposition}

\begin{proof}
~\begin{itemize}[$\blacksquare$]
    \item Preuve de $(i)$.

Soit $u \in L^p(\Omega)$. Soit $u_n \in L^p(\Omega)$. Montrer que:
\[
\|u_n - u\|_{L^p} \xrightarrow{n \to \infty} 0
\]

Il existe $(K_n)$ une suite de compacts inclus dans $\Omega$ avec
$K_n \subset \mathring{K}_{n + 1}, \bigcup K_n = \Omega$ et telles que
$\forall K \subset \Omega$ compact, $\exists n_0$ et $\forall n \geq n_0,
K \subset K_n$.

Posons $u = \ind{K_n}u \in L^p_0(\Omega)$. Alors:
\[
\|u_n - u\|_{L^p}^p = \int_\Omega |\underbrace{(\ind{K_n} - 1)}_{|\ind{K_n} - 1|
    \xrightarrow{n \to \infty} 0} u(x)|^p \dx \to 0.
\]

On a donc:
\begin{itemize}
    \item $\forall x$ fixé $f_n(x) \to 0$.
    \item $|f_n(x)| \leq |u(x)|^p$ (intégrable sur $\Omega$ et
    indépendante de $n$).
    \item \ref{thm:tcd} $\implies \displaystyle \int f_n(x) \to 0$.
\end{itemize}

    \item Preuve de $(ii)$.

Soit $u \in \mathcal{C}$. Soit $u_n \in \mathcal{C}^k_0$. Donc $\forall K \subset \Omega$
compact de $\mathbb{R}^d, \forall \alpha \in \mathbb{N}^d, |\alpha| \leq k$ on a
$\pd*{^\alpha u_n}{x} \to \pd*{^\alpha u}{x}$ uniformément sur $K$.

On considère la suite $(K_n)$. Il existe $\phi_n \in \mathcal{C}^k_0$ avec
$\phi_n \equiv 1$ sur $K_n$.

On définit $u_n = (u \phi_n)(x) \in \mathcal{C}^k_0$. Si $K$ est un compact de
$\Omega, \exists n_0, \forall n > n_0$ avec $K \subset K_{n - 1} \subset
\mathring{K}_n \subset K_n$.

On a: $u_n(x) = u(x), \forall x \in K_n$ et $u_n(x) = u(x), \forall x \in
\mathring{K}_n$. Donc, $\forall \alpha, |\alpha| \leq k$:
\[
\pd{^\alpha u_n}{x^\alpha} = \pd{^\alpha u}{x^\alpha}, \forall x \in \mathring{K}_n
\]

Comme $K \subset \mathring{K}_n$:
\[
\pd{^\alpha}{x^\alpha} (u_n - u) = 0 \text{ sur } K \text{ si } n \geq n_0
\]
\end{itemize}
\end{proof}

\subsection{Régulation et convolution}
\begin{definition}
Soit $f \in L^1(\mathbb{R}^d), \phi \in \mathcal{C}^\infty(\mathbb{R}^d)$. On appelle convolution de
$f$ et $\phi$ la fonction $f * \phi$ définie par:
\[
(f*\phi)(x) = \int_{\mathbb{R}^d} \phi(x - y) f(y) \dx[y]
\]
\end{definition}

On définit ainsi une fonction bornée de $x$ vérifiant $\|\phi * f\|_{L^\infty}
\leq \|\phi\|_{L^\infty} \|f\|_{L^1}$. De plus, $x \mapsto (\phi * f)(x)$
est $\mathcal{C}^\infty$ et pour $\alpha \in \mathbb{N}^d$:
\[
\pd{^\alpha}{x^\alpha} (\phi * f) = \left(\pd{^\alpha \phi}{x^\alpha} * f \right).
\]

\begin{proof}
On a:
\[
|\phi(x - y) f(y)| \leq \sup_z |\phi(z)| |f(y)|.
\]
donc:
\[
\begin{aligned}
|(\phi * f)(x)| & =     \left|\int \phi(x - y)f(y) \dx[y]\right| \\
                & \leq  \int |\phi(x - y)| |f(y)| \dx[y] \\
                & \leq  \sup_z |\phi(z)| \int |f(y)| \dx[y] \\
                & =     \|\phi\|_{L^\infty} \|f\|_{L^1}
\end{aligned}
\]

Pour voir que $\phi * f$ est $\mathcal{C}^\infty$ on applique le théorème de
dérivabilité sous l'intégrale:
\begin{itemize}
    \item $\forall x$ fixé: $y \mapsto \phi(x - y)f(y)$ est intégrable.
    \item $\forall y$ fixé: $x \mapsto \phi(x - y)f(y)$ admet des dérivées
    partielles d'ordre quelconque continue (car
    $\phi \in \mathcal{C}^\infty(\mathbb{R}^d)$).
    \item $\forall \alpha, \exists h_\alpha(y)$ intégrable telle que:
    \[
    \left|\pd{}{x^\alpha} (\phi(x - y)f(y))\right|
        \leq h_\alpha (y) =\sup_z |\phi(z)| |f(y)|.
    \]
    \item Par le théorème de dérivabilité sous l'intégrale on a:
    \[
    \pd{^\alpha}{x^\alpha} (\phi * f) = \left(\pd{^\alpha \phi}{x^\alpha} * f \right).
    \]
    et $x \mapsto \int \phi(x - y)f(y) \dx[y]$ est $\mathcal{C}^\infty$.
\end{itemize}
\end{proof}

\begin{corollary}
Soient $p \in [1, \infty], f \in L^p_0(\mathbb{R}^d)$ et
$\phi \in \mathcal{C}^\infty_0(\mathbb{R}^d)$.
Alors $\phi * f \in \mathcal{C}^\infty_0(\mathbb{R}^d)$.
\end{corollary}

\begin{proof}
On a: $L^p_0(\mathbb{R}^d) \subset L^1(\mathbb{R}^d)$.

Comme $\supp{\phi}$ est compact, $\exists A > 0$ avec $\supp{\phi} \subset
\mathcal{B}(0, A)$. De même, $\exists B > 0$ telle que $K \subset
\overline{\mathcal{B}(0, B)}$. Alors si $|x| > A + B$ et $y \in K$:
\[
|x - y| \geq ||x| - |y|| \geq |A + B - B| = A
\]
Donc $\phi(x - y) = 0$ et $(\phi * f)(x) = 0$ si $|x| > A + B$.
\end{proof}

\begin{remark}
On a $\phi * f \in L^p$ et $\|\phi * f\|_{L^p} \leq \|\phi\|_{L^1}
\|f\|_{L^p}$.
\end{remark}

\begin{proof}
On a:
\[
\begin{aligned}
|\phi * f| & \leq \int |\phi(x - y)| |f(y)| \dx[y] \\
& =     \int \underbrace{|\phi(x - y)|^{\frac{1}{q}}}_{f_1(y)}
               \underbrace{|\phi(x - y)|^{\frac{1}{p}} |f(y)|}_{f_2(y)} \dx[y] \\
& \leq  \|f_1\|_{L^q} \|f_2\|_{L^p} \\
& \leq  \left( \int |\phi(x - y)| \right)^{\frac{1}{q}}
          \left( \int |\phi(x - y)||f(y)|^p \right)^{\frac{1}{p}} \\
& =     \|\phi\|_{L^1}^{\frac{1}{q}}
          \left(\int |\phi(x - y)||f(y)|^p \right)^{\frac{1}{p}}
\end{aligned}
\]

et:
\[
\begin{aligned}
\|\phi * f\|_{L^p} & \leq \|\phi\|_{L^1}^{\frac{1}{q}}
                \left( \int \int |\phi(x - y)||f(y)|^p \right)^{\frac{1}{p}}
                \dx \dx[y] \\
& \leq \|\phi\|_{L^1}^{\frac{1}{q}}
         \left( \int |\phi(z)| \dx[z] \int |f(y)|^p \dx[y] \right)^{\frac{1}{p}} \\
& = \|\phi\|_{L^1}^{\frac{1}{q}}
      \|\phi\|_{L^1}^{\frac{1}{p}}
      \|f\|_{L^p} \\
& = \|\phi\|_{L^1} \|f\|_{L^p}
\end{aligned}
\]
\end{proof}

\begin{proposition}
Soit $\phi \in \mathcal{C}^\infty_0(\mathbb{R}^d),
f \in \mathcal{C}^k_0(\mathbb{R}^d)$. Alors $\phi * f \in
\mathcal{C}^k_0(\mathbb{R}^d)$ et si $|\alpha| \leq k$:
\[
\pd{^\alpha}{x^\alpha}(\phi * f) = \phi * \pd{^\alpha f}{x^\alpha}.
\]
\end{proposition}

\begin{notation}
Soit $\phi \in \mathcal{C}^k_0(\mathbb{R}^d)$ avec
$\phi \geq 0, \supp{\phi} \subset \mathcal{B}(0, 1)$ et:
\[
\int \phi(x) \dx = 1.
\]

On définit:
\[
\phi_\epsilon = \frac{1}{\epsilon^d} \phi\left(\frac{x}{\epsilon}\right)
    \text{ avec } \int \phi_\epsilon(x) \dx = 1
\]
\end{notation}

\begin{proposition}
~\begin{enumerate}[(i)]
     \item Soient $k \in \mathbb{N}$ et $f \in \mathcal{C}^{k}_0(\mathbb{R}^d)$. Alors$\phi_\epsilon * f
     \in \mathcal{C}^{\infty}(\mathbb{R}^d)$ et $\forall \alpha, |\alpha| \leq k$:
     \[
     \pd{^\alpha}{x^\alpha} (\phi_\epsilon * f) \xrightarrow{\epsilon \to 0}
     \pd{^\alpha f}{x^\alpha}.
     \]
     \item Soit $f \in L^1$. Alors:
     \[
     \|\phi_\epsilon * f - f\|_{L^1} \xrightarrow{\epsilon \to 0} 0.
     \]
     \item Soient $p \in [1, \infty[$ et $f \in L^p_0(\mathbb{R}^d)$. Alors:
     \[
     \|\phi_\epsilon * f - f\|_{L^p} \xrightarrow{\epsilon \to 0} 0.
     \]
 \end{enumerate}
\end{proposition}

\begin{theorem}[Théorème de Heine] Toute fonction continue à support compact est
uniformément continue.
\end{theorem}

\begin{corollary}
Soit $\Omega$ un ouvert de $\mathbb{R}^d$. Alors $\mathcal{C}^\infty_0$ est dense dans
$\mathcal{C}^\infty$.
\end{corollary}

\begin{proof}
Soit $u \in \mathcal{C}^\infty$. Soit $u_n \in \mathcal{C}^\infty_0$. $\forall \alpha \in \mathbb{N}^d$
avec $|\alpha| \leq k$, on a $\pd*{^\alpha u_n}{x} \to \pd*{^\alpha u}{x}$
uniformément sur le compact $K \subset \Omega$.

On a vu que $\mathcal{C}^\infty_0$ est dense dans $\mathcal{C}^\infty$. Il suffit de voir
que si $u \in \mathcal{C}^\infty_0$, il existe $(u_n)$ comme ci-dessous. On a vu que si
$u \in \mathcal{C}^\infty_0$ et si on pose $\tilde{u}(x) = u(x)$ pour $x \in \Omega$
et $0$ pour si $x \in \mathbb{R}^d \setminus K$, alors $\tilde{u} \in \mathcal{C}^{\infty}_0(\mathbb{R}^d)$.

On vient de voir qu'alors $\tilde{u} * \phi_\epsilon \to \tilde{u}$
et $\tilde{u} * \phi_\epsilon \in \mathcal{C}^\infty_0$.

Pour $F$ fermé de $\mathbb{R}^d, \forall x \in \mathbb{R}^d$:
\[
d(x, F) = \inf_{y \in F} |x - y|.
\]

Posons $K_\epsilon = \{x \in \mathbb{R}^d \mid d(x, K) < \epsilon\}$. Donc
$\supp{\phi_\epsilon * \tilde{u}} \subset K_\epsilon$.

On pose $\delta = \inf d(x, F)$ où $x \in K$ et $F = \mathbb{R}^d \setminus \Omega$. Si
$\epsilon < \frac{\delta}{2}$, on aura $K_\epsilon \subset \Omega$. Pour de tels
$\epsilon$ on a que $\phi_\epsilon * \tilde{u} \in \mathcal{C}^\infty_0$ car $K_\epsilon$
est un compact inclus dans $\Omega$.

Donc $\tilde{u} * \phi_\epsilon \to \tilde{u}$ uniformément sur $\mathbb{R}^d$,
donc aussi uniformément vers $u$ sur $\Omega$.
\end{proof}

\begin{definition}
Soient $\Omega_1, \Omega_2$ deux ouverts de $\mathbb{R}^d[d_1], \mathbb{R}^d[d_2]$.
On note $\mathcal{C}^\infty_0(\Omega_1) \otimes \mathcal{C}^\infty_0(\Omega_2)$ l'espace des
fonctions:
\[
\deffunction{u}{\Omega_1 \times \Omega_2}{\mathbb{C}}{(x, y)}{u(x, y)}
\]
tels qu'il existe $N \in \mathbb{N}, v_i \in \mathcal{C}^{\infty}_0(\Omega)$ et $w_j \in
\mathcal{C}^{\infty}_0(\Omega_2)$ avec:
\[
u(x, y) = \sum v_i(x) w_j(y).
\]
\end{definition}

\begin{proposition}
$\mathcal{C}^\infty_0(\Omega_1) \otimes \mathcal{C}^\infty_0(\Omega_2)$ est dense dans
$\mathcal{C}^{\infty}_0(\Omega_1 \times \Omega_2)$ au sens suivant: si
$u \in \mathcal{C}^{\infty}_0(\Omega_1 \times \Omega_2)$, il existe une suite
de fonctions $\mathcal{C}^\infty_0(\Omega_1) \otimes \mathcal{C}^\infty_0(\Omega_2)$
et $K$ compact de $\Omega_1 \times \Omega_2$ tel que $\forall n,
\supp{u_n} \subset K$ et $\forall \alpha \in \mathbb{N}^d.
\pd*{^\alpha u_n}{x} \to \pd*{^\alpha u}{x}$ uniformément sur $\Omega_1 \times
\Omega_2$.
\end{proposition}

\begin{theorem}[Formule de Taylor avec reste intégrale]\label{thm:taylor}
Soient $\Omega$ ouvert de $\mathbb{R}^d, N \in \mathbb{N}^*$ et $u \in \mathcal{C}^N$. Soit
$x \in \Omega, h \in \mathbb{R}^d$ telle que $[x, x + h] \subset \Omega$. Alors:
\[
u(x + h) =
    \sum_{|\alpha \leq N|} \frac{h^\alpha}{\alpha!} \pd{^\alpha u}{x^\alpha} +
    N \sum_{|\alpha| = N} h^\alpha \int_0^1 \frac{(1 - t)^{N - 1}}{\alpha!} \pd{^\alpha u}{x^\alpha}(x + th) \dx[t]
\]
où $h^\alpha = (h^{\alpha_1}_1, \dots, h^{\alpha_N}_N)$.
\end{theorem}

% kate: default-dictionary fr_FR;
