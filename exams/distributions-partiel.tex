% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\documentclass[DIV=14,10pt,parskip=half*]{scrartcl}

% {{{ packages

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{mathrsfs}
\usepackage{cases}

% }}}

% {{{

\NewDocumentCommand \vp {} {\mathrm{v.p.}\left(\frac{1}{x}\right)}
\NewDocumentCommand \ind { m } {{\ \mbox{l\hspace{-0.55em}1}}_{#1}}

\NewDocumentCommand \pd { m m } {\dfrac{\partial #1}{\partial #2}}
\NewDocumentCommand \dx { O{x} } {\,\mathrm{d} #1}
\NewDocumentCommand \avg { sm } {
    \IfBooleanTF#1{\langle #2 \rangle}{\left\langle #2 \right\rangle}
}

\DeclareMathOperator{\supp}{Supp}

% }}}

\begin{document}
\begin{minipage}{.45\linewidth}
    Université Paris-Nord - Institut Galilée \hfill
\end{minipage}
\hspace{10pt}
\begin{minipage}{.45\linewidth}
    \hfill 2013
\end{minipage}

\hbox{}

\begin{center}

{\Large \textbf{MACS 2ème année}}

Épreuve de \emph{Distributions} d'avril 2013
\end{center}

\section*{Exercice 1}

Si $(u_n)$ tend vers $0$ dans $\mathcal{C}^0_0$ et $K$ est le compact de $(i)$:
\[
p_f(u_n) \leq \left(\sup_{K} \frac{1}{f}\right) \sup |u_n| \to 0.
\]

Réciproquement, il faut montrer que si $p_f(u_n) \to 0$ pour tout $f \in
\mathcal{F}$, alors $(i)$ et $(ii)$ sont satisfaites, ou encore que si $(i)$ ou
$(ii)$ est fausse, il existe $f \in \mathcal{F}$ avec $p_f(u_n) \nrightarrow 0$.

Si $(ii)$ est fausse, il suffit de prendre $f = 1$.

Si $(i)$ est fausse, pour tout compact $K$, il existe $n \in \mathbb{N}^*$ et
$x \in \mathbb{R} \setminus K$ avec $u_n(x) \neq 0$. Si on prend $K = [-k, k]$ avec
$k \in \mathbb{N}$ on trouve $n_k \in \mathbb{N}^*$ et $x_k$ vérifiant $|x_k| > k$
tel que $u_{n_k}(x_k) \neq 0$. On suppose $n_k$ strictement croissante, tendant vers
$+\infty$ et $(x_k)$ strictement croissante tendant vers $+\infty$ ou
$-\infty$. Plaçons-nous dans le premier cas ($x_k \to \infty$). On pose
$\lambda_k = \min_{j \leq k} u_{n_j}(x_j)$ et on choisit $\theta_k$ continue
vérifiant $\theta_k = 1$ sur $]-\infty, x_k]$, $\theta_k = 0$ sur $[x_{k + 1},
\infty[$ et $\theta_k \in [0, 1]$ sur $[x_k, x_{k + 1}]$. On pose:
\[
f(x) = \sum_{k = 1}^\infty 2^{-k} \theta_k(x) \lambda_k.
\]

Comme $\lambda_k \leq \lambda_1$, la série converge normalement, donc $f$
est continue. Elle est strictement positive par construction. De plus:
\[
f(x_k) = \sum_{k' = k}^\infty 2^{-k'} \lambda_{k'} \leq
         \lambda_k \sum_{k' = k}^\infty 2^{-k'} \leq \lambda_k \leq |u_{n_k}(x_k)|,
\]
d'où:
\[
p_f(u_{n_k}) \geq \frac{|u_{n_k}(x_k)|}{f(x_k)} \geq 1.
\]

Donc $p_f(u_{n_k}) \nrightarrow 0$, donc $p_f(u_n) \nrightarrow 0$.

\section*{Exercice 2}
Soit $\phi \in \mathcal{C}^\infty_0(\mathbb{R})$. On a:
\[
\avg{T_n^\beta, \phi}
= \int_0^\infty \frac{n^\beta}{x^\alpha} e^{\imath nx}\phi(x) \dx[x]
= n^{\alpha + \beta - 1} \int_0^\infty
    \frac{e^{\imath x}}{x^\alpha} \phi\left(\frac{x}{n}\right) \dx[x]
\]

Supposons $\beta = 1 - \alpha$ et montrons qu'alors l'intégrale converge vers
$\phi(0)I$ où:
\[
I = \int_0^\infty \frac{e^{\imath x}}{x^\alpha} \dx[x]
\]

On écrit:
\[
\begin{aligned}
\int_0^\infty \frac{e^{\imath x}}{x^\alpha} \phi\left(\frac{x}{n}\right) \dx[x]
& =
    \int_0^1 \frac{e^{\imath x}}{x^\alpha} \phi\left(\frac{x}{n}\right) \dx[x] +
    \int_1^\infty \frac{e^{\imath x}}{x^\alpha} \phi\left(\frac{x}{n}\right) \dx[x] \\
& = I_1 + I_2
\end{aligned}
\]
et
\[
\begin{aligned}
I_2
& = \left[
    \frac{1}{\imath} \frac{e^{\imath x}}{x^\alpha}
    \phi\left(\frac{x}{n}\right)\right]_1^\infty
+ \frac{\alpha}{\imath} \int_1^\infty
    \frac{e^{\imath x}}{x^{\alpha + 1}} \phi\left(\frac{x}{n}\right) \dx[x]
- \frac{1}{\imath n} \int_1^\infty
    \frac{e^{\imath x}}{x^\alpha} \phi'\left(\frac{x}{n}\right) \dx[x] \\
& = J_1 + J_2 + J_3.
\end{aligned}
\]

On a:
\[
\begin{aligned}
J_1 & = -\frac{1}{\imath} \phi\left(\frac{1}{n}\right) e^\imath
    \to -\frac{1}{\imath} \phi(0) e^\imath, \\
J_2 & \to \phi(0) \frac{\alpha}{\imath}
    \int_1^\infty \frac{e^{\imath x}}{x^{\alpha + 1}} \dx[x], \\
|J_3| & \leq \frac{1}{n} \int_1^\infty
    \left|\phi'\left(\frac{x}{n}\right)\right| \frac{\dx[x]}{x^\alpha} \to 0.
\end{aligned}
\]

Donc:
\[
\avg{T_n^{1 - \alpha}, \phi} \to
    -\frac{1}{\imath} \phi(0) e^\imath + \frac{\alpha}{\imath}
    \left(\int_1^\infty \frac{e^{\imath x}}{x^{\alpha + 1}} \dx[x]\right) \phi(0).
\]

En refaisant des intégrations par partie, on constate que cette dernière
expression vaut $\phi(0)I$ donc $T_n^{1 - \alpha} \to \delta_0 I$.

Si $\beta < 1 - \alpha, T_n^\beta = n^{\beta - (1 - \alpha)} T_n^{1 - \alpha} \to 0$
puisque $\beta - (1 - \alpha) < 0$.

Si $\beta > 1 - \alpha, T_n^\beta$ ne peut converger: si elle convergeait vers
$S$, alors $T_n^{1 - \alpha} = n^{(1 - \alpha) - \beta}T_n^\beta$ devrait
converger vers $0$. Mais on a vu que $T_n^{1 - \alpha} \to \delta_0 I$
avec $I \neq 0$.

\section*{Exercice 3}

Si la série converge $\sum \lambda_n \phi(x_n)$ converge pour tout $\phi$
dans $\mathcal{C}^\infty_0(\mathbb{R})$. Si on prend $\phi \equiv 1$ près de $0$, on en
déduit que $\sum \lambda_n$ converge.

Réciproquement, supposons que $\sum \lambda_n$ converge. Posons $S_0 = 0,
S_n = \sum^n \lambda_k$. Soient $p \leq q$. Écrivons, comme $\lambda_n = S_
n - S_{n - 1}$,:
\[
\begin{aligned}
\sum_{n = p}^q \lambda_n \phi(x_n)
& = \sum_{n = p}^q (S_n - S_{n - 1}) \phi(x_n) \\
& = \sum_{n = p}^q S_n \phi(x_n) - \sum_{n = p}^q S_{n - 1} \phi(x_n) \\
& = S_q \phi(x_q) + \sum_{n = p}^{q - 1} S_n(\phi(x_n) - \phi(x_{n + 1}))
    - S_{p - 1} \phi(x_p)
\end{aligned}
\]

Or par hypothèse, $S_q$ tend vers $S = \sum^\infty \lambda_n$ si $q \to \infty$
et $\phi(x_q) \to \phi(0)$. Pour tout $\epsilon > 0$, il existe donc $n_0$
tel que $p, q > n_0 \implies |S_q\phi(x_q) - S_p\phi(x_p)| < \frac{\epsilon}{2}$.
D'autre part:
\[
\left|
    \sum_p^{q - 1} S_n(\phi(x_n) - \phi(x_{n + 1}))
\right| \leq (\sup |S_n|) \sup |\phi'| \sum_p^{q - 1} (x_n - x_{n + 1})
\]
puisque $(x_n)$ est décroissante. On obtient donc une majoration par
$c(x_{q - 1} - x_{p + 1}) < \frac{\epsilon}{2}$ si $p, q > n_0$ assez grand.

Par conséquence, $\forall \epsilon > 0, \exists N$ et $\forall p, q > N$:
\[
\left|\sum_{n = p}^q \lambda_n \phi(x_n)\right| < \epsilon
\]

Par le critère de Cauchy, $\sum \lambda_n \phi(x_n)$ converge, i.e.
$\sum \lambda_n \delta_{x_n}$ converge dans $\mathcal{D}'$.
\end{document}

% kate: default-dictionary fr_FR;
