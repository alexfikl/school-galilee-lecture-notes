% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\documentclass[DIV=14,10pt,parskip=half*]{scrartcl}

% {{{ packages

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{xfrac}
\usepackage{stmaryrd}
\usepackage{mathrsfs}
\usepackage{cases}

\usepackage{enumitem}

% }}}

% {{{


\NewDocumentCommand \Hz {} {H^1_0(\Omega)}
\NewDocumentCommand \ip {} {{i + \sfrac{1}{2}}}
\NewDocumentCommand \im {} {{i - \sfrac{1}{2}}}

\NewDocumentCommand \abs { m } {\left|#1\right|}
\NewDocumentCommand \pd { m m } {\dfrac{\partial #1}{\partial #2}}
\NewDocumentCommand \dx { O{x} } {\,\mathrm{d} #1}
\NewDocumentCommand \avg { sm } {
    \IfBooleanTF#1{\langle #2 \rangle}{\left\langle #2 \right\rangle}
}

\DeclareMathOperator{\supp}{Supp}

% }}}

\begin{document}

\begin{center}

{\large \textbf{Examen du cours de volumes finis pour les équations \\ elliptiques}}

Institut Galilée, option MACS, deuxième année

Le 25 Juin 2013
\end{center}

\vspace{1cm}

\begin{enumerate}[label=\textsc{Question} \arabic*.]
    \item On a $a(u, u) = \int k(u'(x))^2 \dx[x]$ donc:
\[
k_* \int_\Omega u'(x)^2 \dx[x] \leq a(u, u) \leq k^* \int_\Omega u'(x)^2 \dx[x]
\]
et donc $\sqrt{k_*}\abs{u}_{H^1_0} \leq |||u||| \leq \sqrt{k^*} \abs{u}_{H^1_0}$.
On sait que $\abs{\cdot}_{H^1_0}$ est la norme équivalente de $H^1$ sur $H^1_0$.
Donc $|||\cdot|||$ l'est aussi.

    \item La formulation variationnelle de $(1)-(2)$ dans $\Hz$ est
de trouver $\hat{u} \in \Hz$ telle que pour tout $v \in \Hz, a(\hat{u}, v) =
\int fv \dx[x]$.

    \item Si $\hat{u}$ est discontinue en un point $x$ quelconque sa dérivée au
sens de distribution présentera un Dirac en $x$; même chose pour $k\hat{u}'$.
Or l'égalité $(1)$ est écrite dans $L^2$ et les Dirac ne sont pas dans $L^2$.
Donc nécessairement $\hat{u}$ et $k\hat{u}'$ sont continues aux points
$x_{\ip}$.

    \item $F_{\ip}$ est une approximation de $-k\hat{u}'$ en
$x_{\ip}$ du côté de $T_i$; sur $T_i$ $k$ vaut $k_i$ et
on peut y approcher $\hat{u}'$ par $\displaystyle \frac{u_{i +
\sfrac{i}{2}} - u_i}{\sfrac{|T_i|}{2}}$. Donc:
\[
F_{i, \ip} = -2k_i\frac{u_{\ip} - u_i}{|T_i|}
\]
De même:
\[
F_{i, \im} = 2k_i\frac{u_i - u_{\im}}{|T_i|}
\]

    \item Le principe de conservativité dit que:
\[
F_{i, \ip} + F_{i + 1, \ip} = 0
\]
Soit:
\[
-2k_i\frac{u_{\ip} - u_i}{|T_i|} + 2k_i\frac{u_{i + 1} - u_{\ip}}{|T_{i + 1}|} = 0
\]
et:
\[
\left(\frac{k_i}{|T_i|} + \frac{k_{i + 1}}{|T_{i + 1}|}\right)u_\ip =
\frac{k_i u_i}{|T_i|} + \frac{k_{i + 1}u_{i + 1}}{|T_{i  + 1}|}
\]

Donc:
\[
u_\ip = \frac{k_i h_{i + 1} u_i + k_{i + 1} h_i u_{i + 1}}{k_i h_{i + 1} + k_{i + 1} h_i}
\]
d'où
\[
F_{i, \ip} = -\frac{2k_{i + 1} k_i}{k_i h_{i + 1} + k_{i + 1} h_i} (u_{i + 1} - u_i)
\]

De même:
\[
F_{i, \im} = \frac{2k_i k_{i - 1}}{k_i h_{i - 1} + k_{i - 1} h_i} (u_i - u_{i - 1})
\]

Sur $T_i$ on a donc:
\[
-\frac{2k_{i + 1} k_i}{k_i h_{i + 1} + k_{i + 1} h_i} (u_{i + 1} - u_i) +
\frac{2k_i k_{i - 1}}{k_i h_{i - 1} + k_{i - 1} h_i} (u_i - u_{i - 1}) =
h_i f_i, \quad \forall i \in [1, N]
\]
à condition de poser $k_0 = k_1$ et $h_0 = 0$ et $k_{N + 1} = k_N$ et
$h_{N + 1} = 0$ pour retrouver les flux
\[
2k_1\frac{u_1 - u_0}{h_1}
\]
pour $i = 1$ et pour $i = N$:
\[
-2k_N\frac{u_{N + 1} - u_N}{h_N}
\]

    \item $u_h(0) = u_h(1) = 0$ et $u_h$ est une fonction $\mathcal{C}^0(\Omega)$
    et $\mathcal{C}^1$ par morceaux.

    \item Pour toute norme $n$, tout vecteur $v$ et tout réel $\lambda$, on a
$n(\lambda v) = |\lambda|n(v)$.

Ici, on a:
\[
|||\varphi||| = \left|\left|\left|\frac{\hat{u} - u_h}{|||\hat{u} - u_h|||}
\right|\right|\right| = \frac{1}{|||\hat{u} - u_h|||}|||\hat{u} - u_h||| = 1
\]

Par définition $|||\hat{u} - u_h||| = a(\hat{u} - u_h, \hat{u} - u_h)$. Donc:
\[
|||\hat{u} - u_h||| = a\left(\hat{u} - u_h, \frac{\hat{u} - u_h}{|||\hat{u} - u_h|||}\right) = a(\hat{u} - u_h, \varphi)
\]

Par linéarité $a(\hat{u} - u_h, \varphi) = a(\hat{u}, \varphi) - a(u_h, \varphi)$. $\varphi$
étant dans $\Hz$ la \textsc{Question} 2 (formulation variationnelle) dit que
$a(\hat{u}, \varphi) = \int f\varphi$. Donc:
\[
\begin{aligned}
|||\hat{u} - u_h||| & = \int_\Omega f\varphi \dx[x] - a(u_h, \varphi) \\
& = (f, \varphi)_{L^2} - \int_\Omega k u_h' \varphi \dx[x]
\end{aligned}
\]

Le résultat est obtenu en décomposant l'intégrale sur $\Omega$ en somme
d'intégrales sur les $T_i$, $k$ étant constant et volant $k_i$ sur chaque $T_i$.

    \item Dans l'égalité de la \textsc{Question} 7 on ajoute et on retranche
    $(t', \varphi)_{L^2}$ (qui a bien un sens puisque $t \in \Hz$).

    Puisque $t \in H^1(\Omega)$ et $\varphi \in \Hz$, la formule de Green donne:
\[
(t', \varphi)_{L^2} = -(t, \varphi')_{L^2}
\]

On a donc bien:
\[
|||\hat{u} - u_h||| = (f - t', \varphi)_{L^2} - \sum_{i = 1}^N \int_{T_i}
    (k_i u_h' + t) \varphi' \dx[x]
\]

Cette égalité est vraie pur certain $\varphi$ qui est dans $\Hz$ et de norme égale
à $1$. Le membre de droite est (par définition du $\sup$) inférieur ou égal
au:
\[
|||\hat{u} - u_h||| \leq \sup_{\stackrel{\varphi \in \Hz}{|||\varphi||| = 1}}
    (f - t', \varphi)_{L^2} - \sum_{i = 1}^N \int_{T_i} (k_i u_h' + t)\varphi' \dx[x]
\]
et ceci pour tout $t \in H^1(\Omega)$. Par définition de l'inf, on a le
résultat demandé.

    \item Par définition de l'inf, on a:
\[
\inf_{t \in H^1} \sup_{\stackrel{\varphi \in \Hz}{|||\varphi||| = 1}}
\left[(f - t', \varphi)_{L^2} - \sum_{i = 1}^n \int_{T_i} (k_i u_h' + t)\varphi' \dx[x]\right]
\leq \sup_{\stackrel{\varphi \in \Hz}{|||\varphi||| = 1}}
\left[(f - (k\hat{u}')', \varphi)_{L^2} - \sum_{i = 1}^n \int_{T_i} (k_i u_h' + k_i \hat{u}')\varphi' \dx[x]\right]
\]
or $(k\hat{u}')' = -f$, donc le premier terme ci-dessus est nul. On a donc:
\[
a(\hat{u} - u_h, \varphi) = -\int_\Omega k(\hat{u} - u_h)\varphi'\dx[x]
\]

Par l'inégalité de Cauchy-Schwarz appliquée à $a$ on a finalement,
en tenant compte du fait que $|||\varphi||| = 1$:
\[
\inf_{t \in H^1} \sup_{\stackrel{\varphi \in \Hz}{|||\varphi||| = 1}}
\left[(f - t', \varphi)_{L^2} - \sum_{i = 1}^n \int_{T_i} (k_i u_h' + t)\varphi' \dx[x]\right]
\leq |||\hat{u} - u_h|||
\]

Ayant les deux inégalités, on a donc l'égalité.

    \item Sur $T_i, t(x_\ip) = F_{i, \ip}$ par définition. Sur $T_{i + 1},
t_{x_\ip} = -F_{i + 1, \ip}$ par définition. Or par conservativité du schéma
$F_{i + 1, \ip} = -F_{i, \ip}$, donc $t$ est bien continue en $x_\ip$, et
donc dans $H^1(\Omega)$ car elle est $\mathcal{C}^1$ par morceaux et
$\mathcal{C}^0(\overline{\Omega})$.

    \item
\[
\begin{aligned}
\int_{T_i} t'(x) \dx[x] & = t(x_\ip) - t(x_\im) \\
& = F_{i, \ip} + F_{i, \im} \\
& = h_i f_i \\
& = \int_{T_i} f(x) \dx[x]
\end{aligned}
\]
par définition du schéma numérique.

    \item Puisque $\int (f - t')(x) \dx[x] = 0$, on a $\int(f - t')\varphi_i\dx[x] = 0$,
car $\varphi_i$ est constant sur $T_i$. D'où:
\[
\begin{aligned}
(f - t', \varphi)_{L^2} & =
    \sum_{i = 1}^N \int_{T_i} (f - t') \varphi(x)\dx[x] \\
& =
    \sum_{i = 1}^n \int_{T_i} (f - t')(\varphi - \varphi_i)(x) \dx[x]
\end{aligned}
\]

    \item Par Cauchy-Schwarz on a:
\[
\begin{aligned}
(f - t', \varphi - \varphi')_{L^2(T_i)} & \leq
    \|f - t'\|_{L^2(T_i)} \|\varphi - \varphi_i\|_{L^2(T_i)} \\
& \leq
    \|f - t'\|_{L^2(T_i)} C_1 h_i \|\varphi'\|_{L^2(T_i)}.
\end{aligned}
\]
Mais:
\[
\|\varphi'\|_{L^2(T_i)}
= k_i^{-\sfrac{1}{2}} \|k_i^{\sfrac{1}{2}} \varphi'\|_{L^2(T_i)}
\]
$k$ étant constant sur $T_i$.

Donc:
\[
|(f - t', \varphi)_{L^2(T_i)}| \leq
C_1 \sum_{i = 1}^N \frac{h_i}{k_i^{\sfrac{1}{2}}}
    \|f - t'\|_{L^2(T_i)} \|k_i^{\sfrac{1}{2}} \varphi'\|_{L^2(T_i)}.
\]

Par Cauchy-Schwarz discret, on a:
\[
|(f - t', \varphi)_{L^2(T_i)}| \leq C_1
    \left[\sum_{i = 1}^n
        \frac{h_i^2}{k_i^{\sfrac{1}{2}}} \|f - t'\|_{L^2(T_i)}^2
    \right]^{\sfrac{1}{2}}
    \left[\sum_{i = 1}^N
        \|k_i^{\sfrac{1}{2}} \varphi'\|_{L^2(T_i)}^2
    \right]^{\sfrac{1}{2}}
\]
or
\[
\begin{aligned}
\sum_{i = 1}^N \|k_i^{\sfrac{1}{2}} \varphi'\|_{L^2(T_i)} & =
    \sum_{i = 1}^N \int_{T_i} k_i \varphi'(x)^2 \dx[x] \\
& = \int_\Omega k\varphi'(x)^2 \dx[x] \\
& = |||\varphi|||^2 \\
& = 1
\end{aligned}
\]

D'où le résultat.

    \item
\[
\sum_{i = 1}^N \int_{T_i}(k_i u_h' + t)\varphi'(x) \dx[x] =
\sum_{i = 1}^N \int_{T_i}(k_i^{\sfrac{1}{2}} u_h' + k_i^{-\sfrac{1}{2}}t)
    k_i^{\sfrac{1}{2}} \varphi' \dx[x]
\]

Par Cauchy-Schwarz sur l'intégrale et après par Cauchy-Schwarz discret et en
reprenant l'argument de la \textsc{Question} 13 sur $\varphi$, on a:
\[
\left|\sum_{i = 1}^N \int_{T_i} (k_iu_h' + t) \varphi'\dx[x]\right| \leq \left(
    \sum_{i = 1}^N \|k_i^{\sfrac{1}{2}} u_h' + k_i^{-\sfrac{1}{2}}t\|_{L^2(T_i)}^2
    \right)^{\sfrac{1}{2}}
\]

    \item En reprenant les questions \emph{8, 13} et \emph{14}, on obtient le
résultat demandé.
\end{enumerate}

\textbf{Remarque}: $t$ étant $\mathbb{P}^1$ sur chaque $T_i, t'$ y est constant
et vaut:
\[
\frac{1}{h_i}[t(x_\ip) - t(x_i)] = f_i
\]
par définition de $t$. Le terme $\|f - t'\|$ vaut donc $\|f - f_i\|$.
Le terme:
\[
\left[\sum_{i = 1}^N \frac{h_i^2}{k_i} \|f - t'\|_{L^2(T_i)}^2\right]^{\sfrac{1}{2}}
\]
est d'ordre 2 (et dont asymptotiquement négligeable) dès que $f$ est $H^1$
sur chaque $T_i$ (car si $f \in H^1(T_i)$, on a $\|f - f_i\| \leq C_1 h_i
\|f'\|_{L^2(T_i)}$).

\end{document}

% kate: default-dictionary fr_FR;
