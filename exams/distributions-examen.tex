% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\documentclass[DIV=14,10pt,parskip=half*]{scrartcl}

% {{{ packages

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{mathrsfs}
\usepackage{cases}

% }}}

% {{{

\NewDocumentCommand \vp {} {\mathrm{v.p.}\left(\frac{1}{x}\right)}
\NewDocumentCommand \ind { m } {{\ \mbox{l\hspace{-0.55em}1}}_{#1}}

\NewDocumentCommand \pd { m m } {\dfrac{\partial #1}{\partial #2}}
\NewDocumentCommand \dx { O{x} } {\,\mathrm{d} #1}
\NewDocumentCommand \avg { sm } {
    \IfBooleanTF#1{\langle #2 \rangle}{\left\langle #2 \right\rangle}
}

\DeclareMathOperator{\supp}{Supp}

% }}}

\begin{document}

\begin{minipage}{.45\linewidth}
    Université Paris-Nord - Institut Galilée \hfill
\end{minipage}
\hspace{10pt}
\begin{minipage}{.45\linewidth}
    \hfill 2013
\end{minipage}

\hbox{}

\begin{center}

{\Large \textbf{MACS 2ème année}}

Épreuve de \emph{Distributions} de juin 2013
\end{center}

\section*{Exercice 0 (Questions de cours)}

\begin{enumerate}
    \item Soit la suite de fonctions $u_n(x) = \frac{1}{n}\ind{[-n, n]}$. Cette
    suite converge-t-elle dans $\mathcal{D}'(\mathbb{R})$ (respectivement dans
    $\mathcal{E}'(\mathbb{R})$)? On justifiera avec soin.
    \item \begin{enumerate}
        \item Donner la définition des distributions tempérées.
        \item Montrer que la fonction $x \to e^x e^{ix}$ est une distribution
        tempérée.
    \end{enumerate}
    \item \begin{enumerate}
        \item Donner la définition de l'espace $H^s(\mathbb{R}^d)$ pour tout réel $s$.
        \item Montrer que si $s > d / 2$, et si $u$ est dans
        $H^s(\mathbb{R}^d)$, alors sa transformée de Fourier $\hat{u}$ est intégrable.
        \item Montrer que, sous les hypothèses de la question précédente,
        toute fonction $u$ de $H^s(\mathbb{R}^d)$ est continue.
    \end{enumerate}
\end{enumerate}

\section*{Exercice 1}
\begin{enumerate}
    \item Si $x = (x_1, x_2) \neq (0, 0)$, on a:
\[
\begin{aligned}
\nabla \cdot \left(\frac{x}{|x|^2}\right)
& = \pd{}{x_1}\left(\frac{x_1}{x_1^2 + x_2^2}\right)
    + \pd{}{x_2}\left(\frac{x_2}{x_1^2 + x_2^2}\right) \\
& = \frac{2}{x_1^2 + x_2^2} - \frac{2x_1^2}{(x_1^2 + x_2^2)^2} -
    \frac{2x_2^2}{(x_1^2 + x_2^2)^2} \\
& = 0.
\end{aligned}
\]

    \item L'intégrale s'écrit:
\[
\int_0^\infty \pd{}{r}\left[\phi(r\cos\theta, r\sin\theta)\right] \dx[r] = -\phi(0).
\]
    \item Soit $\phi \in \mathcal{C}^\infty_0(\mathbb{R}^2)$. Par définition:
\[
\begin{aligned}
\avg{\nabla \cdot \frac{x}{|x|^2}, \phi}
& =
- \avg{\frac{x_1}{|x|^2}, \pd{\phi}{x_1}}
- \avg{\frac{x_2}{|x|^2}, \pd{\phi}{x_2}} \\
& =
-\int_{\mathbb{R}^2} \left[\frac{x_1}{|x|^2} \pd{\phi}{x_1} + \frac{x_2}{|x|^2}
    \pd{\phi}{x_2}\right] \dx[x_1] \dx[x_2].
\end{aligned}
\]

On fait dans l'intégrale le changement de variable donné par les coordonnées
polaires $x_1 = r\cos\theta, x_2 = r\sin\theta, r > 0, \theta \in [0, 2\pi[$.
On obtient comme $\dx[x_1] \dx[x_2] = r\dx[r] \dx[\theta]$.

L'intégrale en $r$ est celle calculée à la question précédente. On trouve donc:
\[
\nabla \cdot \frac{x}{|x|^2} = 2\pi\delta_0.
\]
\end{enumerate}

\section*{Exercice 2}

\begin{enumerate}
    \item Par définition:
\[
    \avg{\vp, \phi} =
    \lim_{\epsilon \to 0+} \int_{|x| > \epsilon} \frac{\phi(x)}{x} \dx[x]
\]
En faisant le changement de variable $x = -x'$, on a:
\[
    \int_{|x| > \epsilon} \frac{\phi(x)}{x} \dx[x]
    = -\int_{|x| > \epsilon} \frac{\phi(-x)}{x} \dx[x]
\]

En faisant la demi-somme avec la définition, on obtient la conclusion.

    \item Écrivons
\[
\avg{\frac{1}{x + \imath \epsilon}, \phi}
= \int \frac{\phi(x)}{x + \imath \epsilon} \dx[x]
= \underbrace{\int \frac{\phi(x) - \phi(-x)}{2(x + \imath \epsilon)} \dx[x]}_{I_1} +
    \underbrace{\int \frac{\phi(x) + \phi(-x)}{2(x + \imath \epsilon)} \dx[x]}_{I_2}.
\]

Si $M$ est telle que $\supp{\phi} \subset [-M, M]$, on majore le module de
l'intégrant de $I_1$ par:
\[
\ind{[-M, M]} \sup{|\phi'|} \frac{|x|}{|x + \imath \epsilon|} \leq
\ind{[-M, M]} \sup{|\phi'|},
\]
donc par une une fonction intégrable indépendante de $\epsilon$. Par le
théorème de convergence dominée, $I_1 \to \avg{\vp, \phi}$ d'après 1.

On peut écrire:
\[
\begin{aligned}
I_2 & =
    \frac{1}{2} \int \phi(x)\left[
    \frac{1}{x + \imath \epsilon} + \frac{1}{-x + \imath \epsilon}
    \right] \dx[x] \\
& = \int \phi(x) \frac{\imath \epsilon}{-x^2 - \epsilon^2} \dx[x] \\
& = \int \phi(\epsilon z) \frac{-\imath}{1 + z^2} \dx[z].
\end{aligned}
\]

Par convergence dominée, $I_2 \to \avg{-\imath \pi \delta_0, \phi}$. Donc
\[
\frac{1}{x + \imath \epsilon} \to \vp - \imath \pi \delta_0.
\]
\end{enumerate}

\section*{Exercice 3}

\begin{enumerate}
    \item
\begin{enumerate}
    \item Par l'inégalité des accroissements finis, pour tous réels
    $z, z', |f(z) - f(z')| \leq M|z - z'|$. Si on prend $z = u(x)$
    et $z' = v(x)$ on trouve l'inégalité.
    \item Par la question précédente $\|f(u_n) - f(u)\|_{L^2}
    \leq M \|u_n - u\| \to 0$.
\end{enumerate}

    \item
\begin{enumerate}
    \item Si on applique $1 (b)$ à $f'$, on obtient:
\[
\|f'(u_n) - f'(u)\|_{L^2} \to 0.
\]

D'autre part, toujours d'après $1 (b)$ appliquée à $f$, $f(u_n) - f(u) \to 0$
dans $L^2$, donc $f(u_n) - f(u)$ tend vers 0 dans $\mathcal{D}'$. On sait que
cela implique que $\nabla f(u_n) - \nabla f(u)$ tend vers $0$ dans $\mathcal{D}'$.
    \item Si $\phi \in \mathcal{C}^\infty_0(\mathbb{R})$, on a:
\[
\avg{f'(u_n) \nabla u_n - f'(u) \nabla u, \phi} =
    \underbrace{\avg{f'(u_n)(\nabla u_n - \nabla u), \phi}}_{I_1} +
    \underbrace{\avg{(f'(u_n) - f'(u)) \nabla u, \phi}}_{I_2}.
\]

On a:
\[
\begin{aligned}
|I_1| & \leq
\int |f'(u_n)||\nabla u_n - \nabla u| |\phi(x)| \dx[x] \\
& \leq \sup|f'| \|u_n - u\|_2 \|\phi\|_2. & \text{(par Cauchy-Schwarz).}
\end{aligned}
\]

Or par hypothèse $\|u_n - u\|_2$ tend vers $0$ (définition de la norme $H^1$).
Donc $I_1 \to 0$. D'autre part, en appliquant $1 (b)$ à $f'$:
\[
\begin{aligned}
|I_2| & \leq
    \int |f'(u_n(x)) - f'(u(x))| |\nabla u| |\phi(x)| \dx[x] \\
& \leq
    \sup |f''| \sup |\phi(x)| \int |u_n(x) - u(x)| |\nabla u| \dx[x] \\
& \leq
    C \|u_n - u\|_2 \|\nabla u\|_2 \to 0.
\end{aligned}
\]
    \item Soit $(u_n)$ une suite de $\mathcal{C}^\infty_0(\mathbb{B}^2), u_n \to u$
    dans $H^1$.
    On a vu en $(a)$ que $\nabla f(u_n) \to \nabla f(u)$. Mais $\nabla f(u_n) =
    f'(u_n) \nabla u_n$ qui d'après $(b)$ converge dans $\mathcal{D}'(\mathbb{R}^2)$
    vers $f'(u)\nabla u$. Par unicité de la limite $\nabla f(u) = f'(u) \nabla u$.
    Comme $f'(u) \in L^\infty$ et $\nabla u \in L^2$, on a $\nabla f(u) \in L^2$.
\end{enumerate}

    \item On applique $(2)$ à la fonction $f(t) = \sqrt{t^2 + \epsilon^2}$ qui
    vérifie les hypothèses précédentes à $\epsilon$ fixé.

    \item D'après la question précédente:
\[
\nabla u_\epsilon =
    \frac{u \nabla u}{\sqrt{u^2 + \epsilon^2}} =
    \ind{u \neq 0} \frac{u \nabla u}{\sqrt{u^2 + \epsilon^2}}.
\]

Alors:
\[
\begin{aligned}
\left|\nabla u_\epsilon - \ind{u \neq 0} \frac{u \nabla u}{|u|}\right|
& =
    \ind{u \neq 0} |\nabla u|
    \left|\frac{u}{\sqrt{u^2 + \epsilon^2}} - \frac{u}{|u|}\right| \\
& =
    \ind{u \neq 0} |\nabla u|
    \left|\frac{|u| - \sqrt{u^2 + \epsilon^2}}{\sqrt{u^2 + \epsilon^2}}\right|.
\end{aligned}
\]

On remarque que:
\[
\ind{u \neq 0} \left|
\frac{|u| - \sqrt{u^2 + \epsilon^2}}{\sqrt{u^2 + \epsilon^2}}
\right|,
\]
est majoré par $2$, et tend vers $0$ presque partout lorsque $\epsilon \to 0$.
Comme $|\nabla u|^2 \in L^1$ et est indépendant de $\epsilon$, le théorème de
convergence dominée implique que:
\[
\left\|\nabla u_\epsilon - \ind{u \neq 0} \frac{u}{|u|} \nabla u\right\|_{L^2} \to 0.
\]

    \item On a:
\[
|u_\epsilon - |u|| = |\sqrt{u^2 + \epsilon^2} - |u|| \leq
    \frac{\epsilon^2}{\sqrt{u^2 + \epsilon^2} + |u|} \leq \epsilon,
\]
donc $u_\epsilon$ tend vers $|u|$ uniformément, donc dans $\mathcal{D}'$. Alors
$\nabla u_\epsilon \to \nabla |u|$. Mais on vient de voir que:
\[
\nabla u_\epsilon = \ind{u \neq 0} \frac{u}{|u|} \nabla u.
\]
Donc:
\[
\nabla |u| = \ind{u \neq 0} \frac{u}{|u|} \nabla u
\]

Comme cette dernière fonction est $L^2$, on a $\nabla |u| \in L^2$. Comme
par hypothèse $|u| \in L^2$, on obtient que $|u| \in H^1$. L'inégalité:
\[
\|\nabla |u|\|_{L^2} \leq \|\nabla u\|_{L^2}
\]
résulte de l'expression ci-dessus de $\nabla |u|$.
\end{enumerate}
\end{document}

% kate: default-dictionary fr_FR;
