% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Tribus, mesures, fonctions}

Notation:
\begin{itemize}
    \item $\mathbb{N} = $ ensemble des entiers positifs $\{0, 1, 2, \dots \}$.
    \item $\mathbb{N}^* = \mathbb{N} \setminus \{0\}$.
    \item $\mathbb{N} = $ ensemble des entiers relatifs $\{ \dots, -2, -1, 0, 1,
    2, \dots \}$.
    \item $\mathbb{Q} = $ ensemble des rationnels.
    \item $\mathbb{Q}^c = $ ensemble des irrationnels.
    \item $\mathbb{R} = $ ensemble des réels $]-\infty, \infty[$.
    \item $\mathbb{R}^d = $ espace réel de dimension $d$.
\end{itemize}

\begin{definition}
On dit qu'un ensemble $E$ est dénombrable s'il existe une bijection entre $E$ et
$\mathbb{N}$.
\end{definition}

Rappel sur l'intégrale de Riemann:

Soit $f : [a, b] \rightarrow \mathbb{R}$
\[
    \int_a^b f(x)\dx[x].
\]

Soit $\tau = \{a = t_0 < t_1 < \dots < t_n = b\}$ une subdivision de $[a, b]$.

On appellera: $|\tau| =\displaystyle \max_{1 \leq i \leq n}(t_i - t_{i+1})$
le pas de la subdivision.
\[
    \overbrace{\sum_{i = 1}^n (t_i - t_{i-1}) \inf_{t_{i - 1} \leq x \leq t_i}
    f(x)}^{I_{-}(f, \tau)} \leq \int_a^b f(x) \dx[x] \leq \overbrace{\sum_{i = 1}^n
    (t_i - t_{i-1}) \sup_{t_{i - 1} \leq x \leq t_i} f(x)}^{I_{+}(f, \tau)}
\]

\begin{definition}
    On dit que f est Riemann int\'egrable si, pour tout suite $\tau_n$ de subdivisions
dont le pas $|\tau|{} \to 0$, la différence
\[
I_{+}(f, t_n) - I_{-}(f, t_n) \xrightarrow[]{n \to 0} 0.
\]
\end{definition}

Inconvénients:
\begin{itemize}
    \item c'est compliqué de décrire les fonctions Riemann intégrables.
    \item cette classe de fonctions Riemann intégrables est plutôt petite.
\end{itemize}

\begin{example}
La fonction:
\[
f(x) = \chi_{\mathbb{Q}}(x) =
\begin{cases}
1 & \quad x \in \mathbb{Q}\\
0 & \quad x \in \mathbb{Q}^c
\end{cases}
\]
n'est pas Riemann intégrable.
\end{example}

La théorie de l'intégration au sens de Lebesgue a pour but de pallier ces inconvénients.

\emph{Idée}: On fait une subdivision sur les valeurs de $f$:

Soit $-k = z_0 < z_1 < \dots < z_n = k $
\[
\begin{aligned}
\int_a^b f(x) \dx[x] & \approx \sum_{i = 1}^n z_i \cdot \text{ longueur} \\
\underbrace{\text{longueur}}_{\text{mesure}} & = \{
    \underbrace{x : z_i \leq f(x) < z_{i + 1}
    }_{\text{ensembles mesurables } \in \text{ tribu}}
\}
\end{aligned}
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                ENSEMBLES MESURABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Ensembles mesurables}

\begin{definition}[Tribu]\label{def:tribu}
    Soit $E \neq \phi$ un ensemble quelconque.

    Une tribu (ou $\sigma$ - algèbre) sur $E$ est une famille $\mathcal{A}$
des parties de $E$ tel que:
\begin{enumerate}[(i)]
    \item $E \in \mathcal{A}$,
    \item $A \in \mathcal{A} \Rightarrow A^C \in \mathcal{A}$,
    \item si $A_n \in \mathcal{A}$, $\forall n \geq 1$, alors $\displaystyle
    \bigcup_{n =1}^{\infty} A_n \in \mathcal{A}$.
\end{enumerate}

\end{definition}

\begin{example}
Exemples de tribus:
\begin{enumerate}[(a)]
    \item $\mathcal{A} = \{\phi, E\}$ - est une tribu, appelée la tribu triviale sur
$E$ (la plus petite tribu sur $E$).
    \item $\mathcal{A} = \mathcal{P}(E) =$ l'ensemble de tout les parties de $E$,
plus $\phi$ et $E$ (la plus grande tribu sur $E$).
\end{enumerate}
\end{example}

\begin{remark}
Propriété des tribus:
\begin{enumerate}
    \item $\phi \in \mathcal{A} \text{, car } \phi = E^C$.
    \item On peut remplacer, dans Définition~\ref{def:tribu}, (iii) par:
\[
    (iii)' si ~A_n \in \mathcal{A}, \forall n \geq 1, \text{ alors }\bigcap_{n =1}^{\infty}
    A_n \in \mathcal{A}.
\]
Nous avons l’équivalence:
\[
    (i) + (ii) + (iii) \Leftrightarrow (i) + (ii) + (iii)'.
\]
    \item Si $\bigcup_{i =1}^n A_i \in \mathcal{A}$ on peut toujours prendre
    $A_{n + 1} = \emptyset, A_{n + 2} = \emptyset, \dots$ telle que si $A_1 \in
    \mathcal{A}, A_n \in \mathcal{A}$ alors $\bigcup_{i =1}^{\infty} A_i \in
    \mathcal{A}$. De la même façon on peut prendre $A_{n + 1} = E, A_{n + 2} = E,
    \dots$ telle que $\bigcap_{i =1}^{\infty} A_i \in \mathcal{A}$.

    \item Si $(\mathcal{A}_i)_{i \in I}$ est une famille des tribu sur $E$, alors
$\bigcap_{i \in I} \mathcal{A}_i$ est encore une tribu sur $E$.
\end{enumerate}
\end{remark}

\begin{definition}
    Soit $\mathcal{C} \subset \mathcal{P}(E)$. On note par $\sigma(\mathcal{C})$ la
tribu engendr\'ee par $\mathcal{C}$:
\[
    \sigma(\mathcal{C}) = \bigcap_{\substack{
    \mathcal{A}\text{ tribu} \\
    \mathcal{C} \subset \mathcal{A}}} \mathcal{A}
\]
\end{definition}

$\sigma(\mathcal{C})$ est une tribu (d'après Remarque $4$) et aussi elle est la
plus petite tribu qui contient $\mathcal{C}$.

\begin{example}{\ \\}
\vspace{-15pt}
\begin{enumerate}
    \item Soient $E \neq \emptyset, A \subsetneq E$, telle que $A \neq \emptyset$,
    et $\mathcal{C} = \{A\}$. Alors:

    $\sigma(\mathcal{C}) = \sigma(\{A\}) \underset{notation}{\equiv} \sigma(A)$

    $\sigma(\mathcal{C}) = \{\emptyset, E, A, A^c\}$

    \item \textbf{Tribu borélienne}: Soit $E = \mathbb{R}$ ou $\mathbb{R}^d$. Soit
    $\mathcal{O}$ la classe des ouverts de $E$. On appelle $\sigma(\mathcal{O})$ la
    tribu bor\'elienne sur $E$, not\'ee par $\mathcal{B}(E)$.

    Dans la suite, sauf indication contraire, on consid\'erera toujours la tribu
    bor\'elienne $\mathcal{B}(\mathbb{R})$ ou $\mathcal{B}(\mathbb{R}^d)$.

    \item \textbf{Tribu produit}: Soient $(E_1, \mathcal{A}_1)$ et $(E_2, \mathcal{A}_2)$
    deux espaces mesurables la tribu produit sur $E_1 \times E_2$ est la tribu $\mathcal{A}_1
    \otimes \mathcal{A}_2$ défini par $\mathcal{A}_1 \otimes \mathcal{A}_2 = \sigma
    \{A_1 \times A_2 : A_1 \in \mathcal{A}_1, A_2 \in \mathcal{A}_2 \}$.
\end{enumerate}

\end{example}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                    MESURES POSITIVES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mesures positives}

Soit $(E, \mathcal{A})$ un espace mesurable.

\begin{definition}\label{def:mes}
    Une mesure positive sur $(E, \mathcal{A})$ est une application $\mu : \mathcal{A}
\to [0, \infty]$, telle que:
\begin{enumerate}
    \item $\mu(\emptyset) = 0$,

    \item Si $(A_n)_{n \geq 1}$ est une suite des éléments de $\mathcal{A}$
    \emph{disjoints}, alors $\mu \left(\displaystyle \bigcup_{n = 1}^{\infty} A_n
    \right) = \displaystyle \sum_{n = 1}^{\infty} \mu (A_n) \in [0, \infty]$.
    ($\sigma$-additivit\'e)
\end{enumerate}
\end{definition}

\begin{remark}
    Si $(A_n)_{n \geq 1}$ sont disjoints, alors
\[
\mu \left(\bigcup_{i = 1}^n A_i \right) = \sum_{i = 1}^n \mu(A_i).
\]
Elle est une conséquence de Définition~\ref{def:mes} en prenant $A_{n + 1} = \emptyset$,
$A_{n + 2} = \emptyset, \dots$.
\end{remark}

\begin{proposition}{\ \\}\label{prop:mes}
\vspace*{-10pt}
\begin{enumerate}
    \item Si $A \subset B$, alors $\mu(A) \leq \mu(B)$, et si de plus $\mu(A) < \infty$,
    alors $\mu(B \setminus A) = \mu(B) - \mu(A)$.

    \item Si $A, B \in \mathcal{A}$, $\mu(A \cup B) + \mu(A \cap B) = \mu(A) + \mu(B)$.

    \item Si $A_n \in \mathcal{A}$, $A_n \subset A_{n + 1}$, $\forall n \geq 0$, alors
\[
    \mu \left(\bigcup_{n = 0}^{\infty} A_n \right) = \lim_{n \rightarrow \infty}
    \uparrow \mu(A_n).
\]

    \item Si $B_n \in \mathcal{A}$ et $B_{n + 1} \subset B_n$ et si $\mu(B_0) <
    \infty$, alors
\[
    \mu \left(\bigcap_{n = 0}^{\infty} B_n \right) = \lim_{n \rightarrow \infty}
    \downarrow \mu(B_n).
\]

    \item Si $A_n \in \mathcal{A}$, $\forall n$, alors
\[
    \mu \left(\bigcup_{n = 1}^{\infty} A_n\right) \leq \sum_{n = 1}^{\infty} \mu(A_n).
\]
\end{enumerate}
\end{proposition}

\begin{proof}{\ \\}
\vspace{-10pt}
\begin{enumerate}
    \item $B = A \cup (B \setminus A)$ ou $A$ et $(B \setminus A)$ sont disjoints.
    Alors $\mu(B) = \mu(A) + \mu(B \setminus A) \geq \mu(A)$, si de plus $\mu(A) <
    \infty$, alors $\mu(B \setminus A) = \mu(B) - \mu(A)$.

    \item
\begin{align}
    \mu(A) & = \mu(A \setminus B) + \mu(A \cap B) \label{rmq:mes1} \\
    \mu(B) & = \mu(B \setminus A) + \mu(A \cap B) \label{rmq:mes2} \\
    \mu(A \cup B) & = \mu(A \setminus B) + \mu(A \cap B) + \mu(B \setminus A) \label{rmq:mes3}
\end{align}

    D'après \eqref{rmq:mes3} - \eqref{rmq:mes1} - \eqref{rmq:mes2} nous avons
    $\mu(A) + \mu(B) = \mu(A \cup B) + \mu(A \cap B)$.

    \item Soit $C_n = A_n \setminus A_{n - 1}, n \geq 1$, alors $(C_n)$ sont disjoints.
\[
    \bigcup_{n = 0}^{\infty} A_n = \bigcup_{n = 0}^{\infty} C_n
\]
    D’après la $\sigma$-additivité:
\[
    \mu \left(\bigcup_{n = 0}^{\infty} C_n\right) = \sum_{n = 0}^{\infty} \mu(C_n).
\]
    D'autre part, $A_n = C_0 \cup C_1 \cup \dots \cup C_n$.
\[
    \mu(A_n) = \sum_{i = 0}^n \mu(C_i) \xrightarrow[n \to \infty]{}
    \sum_{i = 0}^{\infty} \mu(C_i)
\]
    Autrement dit:
\[
    \mu \left(\bigcup_{n = 0}^{\infty} A_n \right) = \lim_{n \to \infty}
    \sum_{i = 0}^{\infty} \mu(C_i) = \lim_{n \to \infty} \mu(A_n)
\]

    \item Soit $A_n = B_0 \setminus B_n, n \geq 0 \text{ et } A_n \subset A_{n + 1},
    \forall n \geq 0$.

    D’après Proposition~\eqref{prop:mes}-3:
\[
    \mu \left(\bigcup_{n = 0}^{\infty} A_n \right) = \lim_{n \rightarrow \infty}
    \uparrow \mu(A_n)
\]
or
\[
    \bigcup_{n = 0}^{\infty} A_n = \bigcup_{n = 0}^{\infty}
    (B_0 \cap B_n^C) = B_0 \cap \bigcup_{n = 0}^{\infty} B_n^C = B_0 \cap
    \left(\bigcup_{n = 0}^{\infty} B_n \right)^C.
\]

    Soit $\omega \in \bigcup_{n = 0}^{\infty} A_n, \exists n \text{telle que} \omega
    \in A_n = B_o \cap B_n^C \implies \omega \in B_0$ et $\omega \in B_n^C
    \Leftrightarrow \omega \in B_0 \cap \bigcup_{n = 0}^{\infty} B_n^C$.

    Comme $\mu(B_0) < \infty$:
\begin{align*}
    \mu \left(\bigcup_{n = 0}^{\infty} A_n \right) & = \mu(B_0 \setminus \cap
    \bigcup_{n = 0}^{\infty} B_n) \\
    & = \mu(B_0) - \mu( \bigcup_{n = 0}^{\infty} B_n) \\
    & \text{ et } \\
    \lim_{n \to \infty} \mu(A_n) & = \lim_{n \to \infty} (\mu(B_0) - \mu(B_n)) \\
    & = \mu(B_0) - \lim_{n \to \infty} \mu(B_n).
\end{align*}

    \item Comme dans Proposition~\eqref{prop:mes}-3, on pose que: $C_n = A_n
    \setminus \bigcup_{k = 0}^{\infty} A_k$, donc $\bigcup_{n = 0}^{\infty} A_n =
    \bigcup_{n = 0}^{\infty} C_n$, $(C_n)_{n \geq 0}$ disjoints.

    D’après la $\sigma$-additivité:
\[
    \mu \left(\bigcup_{n = 0}^{\infty} C_n \right) = \sum_{n = 0}^{\infty} \mu(C_n)
    \leq \sum_{n = 0}^{\infty} \mu(A_n).
\]
\end{enumerate}
\end{proof}

\begin{example}{\ \\}
\vspace{-10pt}
\begin{enumerate}
    \item La \textbf{mesure de comptage} est la mesure définie par:
\[
    \mu(A) = \card(A) = \sum_{x \in A} 1 \equiv \sum_{x \in E} 1_A (x).
\]
    On revient à la Propriété 4 dans la Proposition~\ref{def:mes} avec $\mu$ mesure
    de comptage sur $(\mathbb{N}, \mathcal{P}(\mathbb{N}))$.

    Soit $B_n = \{n, n + 1, n + 2, \dots\}, B_{n + 1} \subset B_, \forall n
    \text{ et } \bigcap_{n = 0}^{\infty} B_n = \emptyset$. Alors:
\[
    \mu \left(\bigcap_{n = 1}^{\infty} B_n \right) = 0 \neq \lim_{n \rightarrow \infty}
    \mu(B_n) = \infty \text{ or } \mu(B_n) = \infty, \forall n \geq 0.
\]

    \item Soit $x \in E$. La \textbf{mesure de Dirac} $\delta_x$ est définie par:
\[
\delta_x (\mathbb{A}) =
    \begin{cases}
        1 & \quad x \in \mathbb{A}\\
        0 & \quad \text{sinon}
    \end{cases}
    = 1_{\mathbb{A}} (x)
\]
    Plus généralement, soit $x_n \in E, \alpha_n \geq 0, \forall n, \text{ alors }
    \sum_{n = 0}^{\infty} \alpha_n \delta_{x_n}$ est encore une mesure positive sur
    $E$.

    \item \textbf{Mesure de Lebesgue}
    \begin{theorem}
        Il existe une unique mesure positive sur $(\mathbb{R}, \mathcal{B}(\mathbb{R}))$,
        notée
        par $\lambda$, telle que $\forall a < b$:
    \[
        \lambda (]a, b[) = b - a.
    \]
    \end{theorem}

    \begin{proof}{\ \\}
    \vspace{-10pt}
    \begin{description}
        \item[Existence] admise.
        \item[Unicité] conséquence du Lemme de classe monotone.
    \end{description}
    \end{proof}
\end{enumerate}
\end{example}

\begin{definition}{\ \\}
\vspace{-10pt}
\begin{enumerate}
    \item $\mu$ est dite finie si $\mu(E) < \infty\; (\mu(E)$ est appelée la masse
    totale de $\mu$).

        \item $\mu$ est dite une \textbf{mesure de probabilité} (ou bien probabilité)
    si $\mu(E) = 1$.

    \item $\mu$ est dite $\sigma-finie$, s'il existe une suite $(E_n)_{n \geq 1}
    \in \mathcal{A}$ croissante tel que:
\[
\begin{cases}
\displaystyle
E = \bigcup_{n = 1}^{\infty} E_n, \\
\mu(E_n) < \infty
\end{cases}
\]
    Par exemple: la mesure de Lebesgue est $\sigma-finie \;(E =\mathbb{R}, E_n = ]-n, n[)$.

    \item $x \in E$ est dite un atome de $\mu$ si $\{x\} \in \mathcal{A}$ et $\mu(\{x\}) > 0$.

    \item $\mu$ est dite diffuse si elle n'a pas d'atomes.

        Par exemple: la mesure de Lebesgue est diffuse.
\end{enumerate}
\end{definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 FONCTIONS MESURABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fonctions mesurables}

\begin{definition}
    Soient $(E, \mathcal{A}) \text{ et } (F, \mathcal{B})$ deux espaces mesurables. Une
    application $f:E \rightarrow F$ est dite mesurable si $\forall B \in \mathcal{B},
    f^{-1}(B) \in \mathcal{A}$ ou $f^{-1}(B) = \{x \in E: f(x) \in B\} \equiv \{f \in B\}$.
\end{definition}

\begin{example}{\ \\}
\vspace{-10pt}
\begin{itemize}
    \item Soit $A \in \mathcal{A}, f = 1_A: E \to \mathbb{R}$ est mesurable.

        \item Plus généralement: Soit $A_i$ disjoints, $A_i \in \mathcal{A}$, $\forall
    i \in I$ fini, $\lambda_i \in \mathbb{R}$,\\ alors
\[
    f = \displaystyle \sum_{i \in I} \lambda_i 1_{A_i}.
\]
Une telle fonction $f$ est appelée fonction étagée.
\end{itemize}
\end{example}

\begin{proposition}
    La composition de deux fonctions mesurables est encore mesurable.
\end{proposition}

\begin{proof}
    Soit: $f:(E, \mathcal{A}) \to (F, \mathcal{B}) \text{ et } g:(F, \mathcal{B}) \to
    (G, \mathcal{G})$ mesurables.

        Montre que: $g \circ f: (E, \mathcal{A}) \rightarrow (G, \mathcal{G})$ est
        mesurable.

        En fait
\[
    \forall G \in \mathcal{G}, (g \circ f)^{-1} (G) = f^{-1}(\underbrace{g^{-1}(G)
    }_{\in \mathcal{B}}) \in \mathcal{A}.
\]
\end{proof}

% TODO: a little too many letters here. does this make sense?
\begin{proposition}\label{prop:mescomp}
    Pour que $f$ soit mesurable, il suffit qu'il existe une classe $\mathcal{C}
    \subset \mathcal{B}$ tel que $\sigma(\mathcal{C}) = \mathcal{B}$ et tel que
    $\forall B \in \mathcal{C}, f^{-1}(B) \in \mathcal{A}$.
\end{proposition}

\begin{proof}
On veut démontrer que $\forall B \in \mathcal{B}, f^{-1}(B) \in \mathcal{A}$.

Pour cela, on considère
\[
    \gamma = \{ B \in \mathcal{B}  \mid f^{-1}(B) \in \mathcal{A}\} \subset \mathcal{B}.
\]
Il s'agit de montrer que $\gamma = \mathcal{B}$.
\begin{itemize}
    \item Par l'hypothèse $\mathcal{C} \in \gamma$.
        \item Montrer que $\gamma$ est une tribu (alors $\gamma \supset \sigma(\mathcal{C})$
    donc $\gamma = \mathcal{B}$):
    \begin{enumerate}[(i)]
        \item $F \in \gamma$ car $f^{-1}(F) = E \in \mathcal{A}$.

        \item Si $B \in \gamma$, $B^C \in \gamma$, $f^{-1}(B^C) = (f^{-1}(B))^C
        \in \mathcal{A}$.

            \item Si $B_n \in \gamma, \forall n$:
    \[
        f^{-1} \left(\bigcup_{n \geq 0} B_n \right) = \bigcup_{n = 0}^{\infty}
        f^{-1}(B_n) \in \mathcal{A}.
    \]
    \end{enumerate}
    $\implies \gamma$ est une tribu.
\end{itemize}
\end{proof}

\begin{example}
Par exemple quand $\mathcal{B =\mathcal{B}(\mathbb{R})}, F = \mathbb{R}$. On peut prendre:
\[
\begin{aligned}
    \mathcal{C} & = \{]-\infty, a[ \mid a \in \mathcal{A}\} \\
    & \text{ ou } \\
    \mathcal{C} & = \{]a, b] \mid \forall a < b\}.
\end{aligned}
\]
Pour vérifier que $f: (E, \mathcal{A}) \to (\mathbb{R}, \mathcal{B(\mathbb{R})})$
est mesurable il suffit de vérifier que $\forall a \in \mathbb{R}, f^{-1}(]-\infty, a[)
\in \mathcal{A}$ ou bien $\forall a < b, f^{-1}(]a, b]) \in \mathcal{A}$.
\end{example}

\begin{corollary}
Toute application continue est mesurable (par rapport aux tribu boréliennes).
\end{corollary}

\begin{proof}
    Si $f$ est continue, alors pour tout ouvert $\mathcal{O}, f^{-1}(\mathcal{O})$
    est encore un ouvert donc mesurable par rapport à la tribu boréliennes.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           OPERATIONS SUR LES FONC MESURABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Opérations sur les fonctions mesurables}

\begin{lemma}\label{lem:mes}
    Soit $f_1: (E, \mathcal{A}) \to (F_1, \mathcal{B}_1) \text{ et } f_2:
    (E, \mathcal{A}) \to (F_2, \mathcal{B}_2)$ mesurables. Alors
    l'application produit $f:(E, \mathcal{A}) \to (F_1 \times F_2, \mathcal{B}_1
    \otimes \mathcal{B}_2)$, définie par $f(x) = (f_1(x), f_2(x)), x \in E$, est
    mesurable.
\end{lemma}

\begin{proof}
    Soit
\[
\begin{cases}
B_1 \in \mathcal{B}_1, B_2 \in \mathcal{B}_2 \\
f^{-1}(B_1 \times B_2) = f_1^{-1}(B_1) \cap f_2^{-1}(B_2) \in \mathcal{A},
\end{cases}
\]
    autrement dit, si on prend
\[
\begin{aligned}
    \mathcal{C} & = \{B_1 \times B_2: B_1 \in \mathcal{B}_1, B_2 \in \mathcal{B}_2 \}\\
    f^{-1}(\mathcal{C}) & \in \mathcal{A}, \\
    \sigma(\mathcal{C}) & = \mathcal{B}_1 \otimes \mathcal{B}_2,
\end{aligned}
\]
% TODO: what proposition?
d'après la Proposition~\eqref{prop:mescomp} $f$ est mesurable.
\end{proof}

\begin{lemma}
    Si $f,g:(E, \mathcal{A}) \rightarrow (\mathbb{R}, \mathcal{B}(\mathbb{R}))$
    mesurables, alors les fonctions $f+g$, $f*g$, $\max(f, g)$, $\min(f, g)$,
    $f_- = \max(f, 0)$, $f_+ = \max(-f, 0)$ sont toutes mesurables.
\end{lemma}

\emph{Rappelles}: Soit $a_n \in \mathbb{R}$, $\forall n \geq 1$, on définit:
\begin{align*}
    \lim_{n \rightarrow \infty} \sup a_n &= \lim_{n \rightarrow \infty}
    \downarrow \sup_{k \geq n} a_k \in \mathbb{R}
    \\
        \lim_{n \rightarrow \infty} \inf a_n &= \lim_{n \rightarrow \infty}
    \uparrow \inf_{k \geq n} a_k \in \mathbb{R}
\end{align*}

De plus, $\lim_{n \rightarrow \infty} a_n$ existe si et seulement
si $\lim \sup a_n = \lim \inf a_n$ et dans ce cas: $\lim \sup a_n = \lim a_n =
\lim \inf a_n$.

\begin{remark}
On a toujours $\lim \inf a_n \leq \lim \sup a_n$. Pour une suite des
fonctions $(f_n)_{n \geq 1}$ boréliennes réelles. On définit,
$\forall x \in E$:
\[
\begin{cases}
\displaystyle
\lim \sup f_n(x) = \lim \sup_{n} f_n(x) \\
\displaystyle
\lim \inf f_n(x) = \lim \inf_{n} f_n(x)
\end{cases}
\]
\end{remark}

\begin{proposition}{\ \\}
\vspace{-10pt}
\begin{itemize}
    \item Si $f_i$ est mesurable $\forall n$, alors $\sup_{n} f_n, \inf_{n} f_n,
    \lim \sup_{n} f_n, \lim \inf_{n} f_n$ sont toutes mesurables.

        \item Si de plus, $f_n(x) \xrightarrow[n \rightarrow \infty]{} f(x),
    \forall x \in E$, alors la fonction $\lim f$ est aussi mesurable.

        \item En général, $\{x \in E: \displaystyle \lim_{n \rightarrow \infty}
    f_n(x) \text{ existe}\}$ est  mesurable.
\end{itemize}
\end{proposition}

\begin{proof}
Par exemple on montre $\sup_{n} f_n$ est mesurable. Il suffit
de vérifier que $\forall a \in \mathbb{R}$:
\[
\begin{aligned}
F^{-1}(]a, \infty) & \in \mathcal{A} \\
& \text{ or} \\
F^{-1}(]a, \infty) & = \{x: \sup_{n \geq 0} f_n(x) > a\}
= \bigcup_{n \geq 0} \{x: f_n(x) > a\} \in \mathcal{A}.
\end{aligned}
\]
\end{proof}
\begin{definition}[Mesure image]
Soit $f:(E, \mathcal{A}) \to (F, \mathcal{B})$ une fonction mesurable
soit $\mu$ une mesure sur $(E, \mathcal{A})$. Alors l'image de $\mu$ par $f$,
not\'ee par $f(\mu)$ est la mesure positive sur $(F, \mathcal{B})$ définie par:
\[
    f(\mu)(B) = \mu(f^{-1}(B)),\;\forall B \in \mathcal{B}.
\]
\end{definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       CLASSE MONOTONE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Classe monotone}
\begin{definition}
    Un sous-ensemble $\mathcal{M}$ de $\mathcal{P}(E)$ est appelé classe monotone si
\begin{enumerate}[(i)]
    \item $E \in \mathcal{M}$,
    \item Si $A, B \in \mathcal{M} \text{ et } A \subset B$, alors $B \setminus A
    \in \mathcal{M}$,
    \item Si $A_n \in \mathcal{M} \text{ et } A_n \subset A_{n + 1}$, alors
    $\bigcup_n A_N \in \mathcal{M}$
\end{enumerate}
\end{definition}

Toute tribu est aussi une classe monotone. Comme dans le cas des tribus, on voit
immédiatement que toute intersection de classes monotone est encore une classe
monotone. Si $\mathcal{C}$ est une partie quelconque de $\mathcal{P}(E)$, on peut
donc définir la classe monotone engendrée par $\mathcal{C}$, notée $\mathcal{M(C)}$,
en posant:
\[
    \mathcal{M(C)} = \bigcap_{\substack{
    \mathcal{M}\text{ classe monotone} \\
    \mathcal{C} \subset \mathcal{M}}} \mathcal{M}.
\]

\begin{theorem}[Théorème de classe monotone]
Si $\mathcal{C} \subset \mathcal{P}(E)$ est stable par intersection finies, alors
$\mathcal{M(C)} = \sigma(\mathcal{C})$.
\end{theorem}

\begin{proof}
Puisque toute tribu est une classe monotone, il est clair qu'on a $\mathcal{M(C)}
\subset \sigma(\mathcal{C})$. Pour établir l'inclusion inverse, il suffit de montrer
que $\mathcal{M(C)}$ est une tribu. Or une classe monotone est une tribu si et
seulement si elle est stable par intersection finies (en effet, par passage au
complémentaire, elle sera alors stable par réunion finies, puis par passage à la
limite croissant par réunion dénombrable). Montrons donc que $\mathcal{M(C)}$ est
stable par intersection finies.

Soit $A \in \mathcal{C}$ fixé. Posons:
\[
    \mathcal{M}_1 = \{B \in \mathcal{M(C)}  \mid A \cap B \in \mathcal{M(C)}\}.
\]

Puisque $\mathcal{C}$ est stable par intersection finies, il est clair que
$\mathcal{C} \subset \mathcal{M}$. Vérifions ensuite que $\mathcal{M}_1$ est une
classe monotone:
\begin{itemize}
    \item $E \in \mathcal{M}_1$ est immédiat.
    \item Si $B, B' \in \mathcal{M}_1 \text{ et } B \subset B'$, on a $A \cap
    (B' \setminus B) = \cup (A \cap B_n) \in \mathcal{M(C)}$ et donc $B'\setminus
    B \in \mathcal{M}_1$.
    \item Si $B_n \in \mathcal{M}$ pour tout $n$ et la suite $B_n$ croît, on a
    $A \cap (B' \setminus B) = \cup (A \cap B_n) \in \mathcal{M}_1$ et $\cup B_n \in
    \mathcal{M}_1$.
\end{itemize}

Puisque $\mathcal{M}_1$ est une classe monotone que contient $\mathcal{C},
\mathcal{M}_1$ contient aussi $\mathcal{M(C)}$. On a donc montré:
\[
    \forall A \in \mathcal{C}, \forall B \in \mathcal{M(C)}, A \cap B \in \mathcal{M(C)}.
\]

Ce n'est pas encore le résultat recherché, mais on peut appliquer appliquer la même
idée une seconde fois. Précisément, on fixe maintenant $B \in \mathcal{M(C)}$, et on
pose:
\[
    \mathcal{M}_2 = \{A \in \mathcal{M(C)}  \mid A \cap B \in \mathcal{M(C)}\}.
\]

D'après la première étape de la preuve $\mathcal{C} \subset \mathcal{M}_2$. En
reprenant exactement les mêmes arguments que dans la première étape, on montre que
$\mathcal{M}_2$ est une classe monotone. Il en découle que $\mathcal{M(C)} \subset
\mathcal{M}_2$, ce qui montre bien que $\mathcal{M(C)}$ est stable par intersections
finies et termine la preuve.
\end{proof}

\begin{corollary}
Soyer $\mu$ et $\nu$ deux mesures sur $(E, \mathcal{A})$. Supposant qu'il existe une
classe $\mathcal{C} \subset \mathcal{A}$ stable par intersections finies, telle que
$\sigma(\mathcal{C}) = \mathcal{A}$ et $\mu(A) = \nu(A)$ pour tout $A \in \mathcal{C}$:
\begin{enumerate}[(i)]
    \item Si $\mu(E) = \nu(E) < \infty$, on a $\mu = \nu$.
    \item S'il existe une suite croissant de parties $E_n \in \mathcal{C}$ tell que
    $E = \cup E_n$ et $\mu(E_n) = \nu(E_n) < \infty$, on a $\mu = \nu$.
\end{enumerate}
\end{corollary}

\begin{proof}{\ \\}
\vspace{-10pt}
\begin{enumerate}[(i)]
    \item Soit $\mathcal{G} = \{A \in \mathcal{A}  \mid \mu(A) = \nu(A)\}$. Par
    hypothèse, $\mathcal{C} \subset \mathcal{G}$. Par ailleurs, on vérifie aisément
    que $\mathcal{G}$ est une classe monotone: par exemple, si $A, B \in \mathcal{G}$
    et $A \subset B$, on a $\mu(B \setminus A) = \mu(B) - \mu(A) = \nu(B) - \nu(A)
    = \nu(B \setminus A)$, et donc $B \setminus A \in E$ (noter qu'on utilise ici le
    fait que $\mu$ et $\nu$ sont finies).

    On conclut que $\mathcal{G}$ contient $\mathcal{M(C)} = \sigma(\mathcal{C}) =
    \mathcal{A}$ (la première égalité d'après le théorème de classe monotone, la
    seconde par hypothèse). Donc $\mathcal{G} = \mathcal{A}$, c'est-à-dire $\mu = \nu$.

    \item Notons, pour tout $n$, $\mu_n$ la restriction à $\mu$ à $E_n$ et $\nu_n$
    la restriction de $\nu$ à $E_n$:
\[
    \forall A \in \mathcal{A}, \mu_n(A) = \mu(A \cap E_n), \nu_n(A) = \nu(A \cap E_n).
\]
    On peut appliquer la partie $(i)$ à $\mu_n \text{ et } \nu_n$, et on trouve
    $\mu_n = \nu_n$. Finalement, en utilisant les propriétés de limite croissante
    des mesures, pour tout $A \in \mathcal{A}$,
\[
    \mu_(A) = \lim \uparrow \mu(A \cap E_n) = \lim \uparrow \nu(A \cap E_n) = \nu(A).
\]
\end{enumerate}
\end{proof}

\begin{corollary}[Unicité de la mesure de Lebesque]
Il existe au plus une mesure $\lambda$ sur $\mathbb{R}, \mathcal{B}(\mathbb{R})$ telle
que pour tout intervalle ouvert non vide $]a, b[$, on ait $\lambda(]a, b[) = b - a$.
En effet, si $\lambda'$ est une seconde mesure ayant la même propriété, on peut
appliquer à $\lambda$ et $\lambda'$ la partie $(ii)$ du corollaire précédent, en
prenant $\mathcal{C}$ la classe des intervalles ouverts (dont on sait qu'elle
engendre la tribu borélienne) et $E_n = ]-n, n[$ pour tout n.

De la même façon, on déduit du corollaire précédent qu'une mesure finie $\mu$ sur
$\mathbb{R}$ est caractérisée par les valeurs de $\mu(]-\infty, a])$ pour tout
$a \in \mathbb{R}$.
\end{corollary}

% kate: default-dictionary fr_FR;
