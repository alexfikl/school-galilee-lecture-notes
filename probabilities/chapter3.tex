% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Introduction aux probabilités}

\[(\Omega, \mathcal{F}) \text{ espace mesurable}\]

D'où:
\begin{enumerate}
    \item $\Omega$ ensemble de toutes expériences possibles.
    \item $\mathcal{F}$ tribu sur $\Omega$.
\end{enumerate}

$\mathbb{P}$ une mesure de probabilité sur $(\Omega, \mathcal{F}): \mathbb{P}$
est une mesure de masse totale 1.

Le triplet $(\Omega, \mathcal{F}, \mathbb{P})$ s'appelle un espace de probabilité.
$\omega \in \Omega$ élément générique de $\Omega$.

\begin{example}
On jette 2 fois un d\'e de six faces, équilibré.
\[
\Omega = \{(i,j): 1\leq i,j \leq 6\}
\]
$\mathcal{F} = $ l'ensemble des tous les sous-ensembles dans $\Omega$, y
compris $\emptyset \text{ et } \Omega$. $\forall A \in \mathcal{F}$ nous avons:
\[
\mathcal{P} = \frac{\card(A)}{36}
\]
Où $\card(A)$ est uniforme sur toutes expériences.
\end{example}

\begin{definition}[Variable aléatoire]
Soit $(E, \mathcal{E})$ un espace mesurable. Une application mesurable $x$.
$x: \Omega \rightarrow E$ est appelée une variable aléatoire à valeur dans $E$.
\[
x: (\Omega, \mathcal{F}) \rightarrow (E, \mathcal{E})
\]
\end{definition}

\begin{definition}[Loi d'une variable aléatoire]
La loi de la variable aléatoire $x$ est la mesure-image $\mathbb{P}$ par $x$,
notée $P_x: \forall B \in \mathcal{E}$.
\[
P_x(B) = \mathbb{P}(x^{-1}(B)) \equiv \mathbb{P}(x \in B)
\]
Où $x^{-1} \equiv \{x \in B\} = \{\omega \in \Omega: x(\omega) \in B\}$.
\end{definition}

On a déjà vu (dans la partie intégration) que $P_x$ est une mesure sur
$(E, \mathcal{E})$ et de plus sa masse totale $P_x(E) =
\mathbb{P}(x \in E) = \mathbb{P}(\Omega) = 1$.

Autrement dit, $P_x$ est une probabilité sur $(E, \mathcal{E})$.

Nous avons 2 cas particuliers:
\begin{enumerate}
    \item Variable aléatoire discrète:

	C'est le cas où $E$ est discret (soit fini, soit dénombrable).
	
	$\mathcal{E}$ est la tribu grossière sur $E$.
	
	i.e. l'ensemble de tous les sous-ensembles de $E$.
	
	$\delta_x (B):$ désigne la mesure de Dirac en $x$.
	\[ \delta_x (B) =
	\left\{\begin{array}{l l}
		1 & \quad \text{si } x \in B\\
		0 & \quad \text{sinon}
	\end{array}\right.\]

	Pour une variable aléatoire $x$ à valeurs dans $E$, soit
	$P_x := \mathbb{P}(X = x) = \mathbb{P}(\{\omega: X(\omega) = x\})$ avec
	$x \in E$.

	Alors $P_x = \sum_{x \in E} p_x \delta_x$.
	
	Vérification, $\forall B \in \mathcal{E}$
	\[
	\begin{aligned}
		P_x(B) & = \mathbb{P}(\{\omega: X(\omega) \in B\}) \\
		& = \mathbb{P}(\bigcup_{x \in B} \{X = x\})
		\text{ où $\bigcup_{x \in B}$ est une union des ensembles disjoints} \\
		& = \sum_{x \in B} \mathbb{P} (X = x) = \sum_{x \in B} p_x =
		\sum_{x \in E} p_x \delta_x (B)
	\end{aligned}
	\]
	
	\begin{remark}
	Pour une variable aléatoire $x$ discrète, déterminer la loi de $x$
	revient à calculer: $\mathbb{P} (X = x), \forall x \in E$.
	\end{remark}
	
	\begin{example}
		On lance un dé équilibre jusqu'à l'apparition de la face "6". Soit
		$x$ le nombre de fois qu'on a jeté ce d\'e pour avoir "6".
		
		$x$ prend des valeurs dans $\{1,2,\dots\} \cup \{\infty\}$. La loi de $x$?
		
		\[
		\begin{array}{l}
			\forall n \geq 1 \text{ on a }\\
			\mathbb{P} (X = n) = \mathbb{P} (1^{er}\text{fois ne donne pas 6}, \dots\\
			(n - 1)^{iem} \text{ fois ne donne pas 6}, \dots\\
			(n)^{iem} \text{ fois ne donne  6})\\
			= \frac{5}{6} \times \frac{5}{6} \times \dots \times \frac{5}{6} \times \frac{1}{6} = \left(\frac{5}{6} \right)^{n - 1} \times \frac{1}{6}\\
			\mathbb{P} (x = \infty) = 0
		\end{array}\]
	\end{example}
	
	\item Variable aléatoire à densité:
	
	Ce sont des variables aléatoires $x$ à valeurs dans $(\mathbb{R}^d, \mathcal{B}(\mathbb{R}^d))$ dont la loi $P_x$ est absolument continue par rapport à la \emph{mesure de Lebesgue}.
	
	D'après le \emph{théorème de Radon-Nikodym}, il existe une fonction borélienne $p: \mathbb{R}^d \rightarrow \mathbb{R}_+$ telle que $B \in \mathcal{B}(\mathbb{R}^d), P_x(B) = \int_B p(x) \dx{x} \Leftrightarrow \frac{\dx{P_x}}{\dx{x}} = p(x)$.
	
	$p(x)$ s'appelle la densité de (la loi de) $X$ en $x$.
	
	En dimension 1:
	\[\forall a < b, \mathbb{P}(a \leq x \leq b) = P_x([a,b]) = \int_a^b p(x) \dx{x}\]
	En particulier, $P_x$ n'a pas d'atome.
\end{enumerate}

\begin{definition}[Espérance] 
	Soit $x$ une variable aléatoire réelle (i.e. à valeur dans $\mathbb{R}$). On notera $\mathbb{E}(x)$, l'espérance de $x$.
	\[\mathbb{E} = \int_\Omega x(\omega) \dx[\mathbb{P}(\omega)]\]
	Qui est bien définie dans les 2 cas suivantes:
	\begin{enumerate}
		\item[a)] Soit $x(\omega) \geq 0, \forall \omega \in \Omega$.
		\item[b)] Soit $x(\omega)$ est intégrable:
		\[\int_\Omega |x(\omega)| \dx[\mathbb{P}(x)] < \infty\]
	\end{enumerate}
	Si $X$ est à valeur dans $\mathbb{R}^d, X = (x_1, \dots , x_d)$ alors on définit:
	\[\mathbb{E}X = (\mathbb{E} x_1, \dots, \mathbb{E} x_d) \text{ à condition que }\mathbb{E} |x_i| < \infty, \forall 1 \leq i \leq d\]
	Si $X = 1_A$ alors $\mathbb{E} 1_A = \mathbb{P}(A)$.
\end{definition}

\begin{proposition}
	Soit $X$ variable aléatoire à valeur dans $(E, \mathcal{E})$. Pour toute fonction mesurable $f: E \rightarrow [0, \infty]$ on a $\mathbb{E} f(x) = \int_E f(x) \dx{\mathbb{P}_x (x)}$.
\end{proposition}

\begin{proof}
	On montre d'abord le cas où $f$ est une fonction étagée.
	\[\begin{array}{l}
		f = \sum_{i = 1}^n \alpha_i 1_{A_i} \text{ où } \alpha_i \geq 0, A_i \in \mathcal{E}\\
		f(x) = \sum_{i = 1}^n \alpha_i 1_{\{x \in A_i\}}\\
		\mathbb{E}f(x) =^{linéaire} \sum_{i = 1}^n \alpha_i \mathbb{E} 1_{\{x \in A_i\}}\\
		\mathbb{E} 1_{x \in A_i} = \mathbb{P}(x \in A_i) \Rightarrow \mathbb{E}f(x) = \sum_{i = 1}^n \alpha_i P_x(A_i) = \int f \dx{P_x}
	\end{array}\]
	Pour le cas général, toute fonction mesurable positive est la limite d'une suite croissante de fonctions étagées positives; on applique le \emph{théorème de convergence monotone}, et on obtient \emph{Proposition}.
\end{proof}

\begin{remark}
	Pour une fonction mesurable $f: E \rightarrow \mathbb{R}$, si $\mathbb{E} |f(x)|$ alors on a:
	\[(*) \mathbb{E} f(x) = \int_E f(x) \dx{P_x(x)}\]
\end{remark}

\begin{proof}
	On décompose $f = f^+ - f^-$ avec $f^+ = \max(f^+,\delta), f^- = \max(f^-,\delta)$.
\end{proof}

\begin{remark}[sur $(*)$]
	Si on sait calculer pour des fonctions "suffisamment générales": $\mathbb{E} f(x) = \int f \dx{\mu}$ avec une certaine mesure $\mu$.
	
	Alors $\mu$ est exactement la loi de $x$.
\end{remark}

\begin{example}
	Soit $x$ une variable aléatoire à valeur dans $\mathbb{R}^d$, avec densité $p(x)$. $x = (x_1, \dots,x_d)$.

	QUESTION - $\forall 1 \leq i \leq d$, quelle est la loi de variable aléatoire de $X_i$?

	Soit $f: \mathbb{R} \rightarrow \mathbb{R}_+$ borélienne. On cherche à calculer $\mathbb{E} f(x_i)$.

	Si on pose $\pi_i (x_1, \dots, x_d) \rightarrow x_i$ la projection sur $i^{eme}$ coordonnée. Alors $f(X_i) = f \circ \pi_i(X)$. On note $P_x(\dx{x}) = p(x)\dx{x}$.
	\[\begin{array}{l}
		\mathbb{E} f(X_i) = \mathbb{E} f \circ \pi_i(X) = \int_{\mathbb{R}^d} f \circ \pi_i (x) p(x) \dx{x}\\
		= \int_\mathbb{R} \dots \int_\mathbb{R} f(x_i) p(x_1, \dots, x_d) \dx{x_1} \dots \dx{x_d}\\
		=^{Fubini-Tonnelli} \int_\mathbb{R} f(x_i) \left(\int_{\mathbb{R}^{d-1}} p(x_1, \dots,x_d) \dx{x_1} \dots \dx{x_{i - 1}} \dx{x_{i + 1}} \dots \dx{x_d} \right) x_i\\
		\int_{\mathbb{R}^{d-1}} p(x_1, \dots,x_d) \dx{x_1} \dots \dx{x_{i - 1}} \dx{x_{i + 1}} \dots \dx{x_d} \equiv  q_i (x_c)
	\end{array}\]
	Donc la loi de $X_i$ est absolument continue par rapport à la \emph{mesure de Lebesgue}, de densité $q_i(x), x \in \mathbb{R}$. Loi de $X_i$ s'appelle loi marginale à $i^{eme}$ coordonnée.
\end{example}

\section{Lois}

Lois discrètes:
\begin{enumerate}
	\item[(a)] Loi uniforme.
	
	Soit $E$ un ensemble fini, $card(E) = n$, une variable aléatoire $X$ est de loi uniforme sur $E$.
	
	Si $\mathbb{P}(X = x) = \frac{1}{n} \Leftrightarrow P_x = \frac{1}{n} \sum_{x \in E} \delta_x$, $\forall x \in E$.
	\item[(b)] Loi de Bernoulli de paramètre $p \in [0,1]$.
	
	C'est une variable aléatoire $X$ à valeur dans $\{0,1\}$ avec $\mathbb{P}(X = 1) = p$ et $\mathbb{P}(X = 0) = 1 - p$.
	
	i.e. $P_x = p \delta_1 + (1 - p) \delta_0$.
	
	\[\mathbb{E}(x) = p\]
	\item[(c)] Loi binomiale $B(n,p), n \geq 1, 0 \leq p \leq 1$.
	
	C'est une variable aléatoire $X$ à valeur dans $\{0,1,2, \dots,n\}$ avec $\mathbb{P}(X = k) = C_n^k p^k (1 - p)^{n - k}, 0 \leq k \leq n$.
	
	\[\begin{array}{l}
		\mathbb{E}X = \int x \dx{P_x(x)}\\
		= \sum_{k = 0}^n k \mathbb{P}(x = k)\\
		= \sum_{k = 0}^n k C_n^k p^k (1 - p)^{n - k}\\
		= n p
	\end{array}\]
	\item[(d)] Loi géométrique de paramètre $p \in ]0,1[$.
	
	C'est la loi d'une variable aléatoire $X$ à valeur dans $\mathbb{N}$, avec $\mathbb{P}(X = n) = (1 - p) p^n, n \geq 0$.
	\[\begin{array}{l}
		\sum_{n = 0}^\infty \mathbb{P} (X = n) = \sum_{n = 0}^\infty (1 - p) p^n = 1\\
		\mathbb{E}(X) = \sum_{k = 0}^\infty k \mathbb{P}(X = k) = \sum_{k = 0}^\infty k (1 - p) p^k\\
		= (1 - p) p \frac{1}{(1-p)^2} = \frac{p}{1 - p}\\
		\text{Or } \sum_{k = 0}^\infty p^k = \frac{1}{1 - p}, 0 < p < 1 \text{ on dérive par rapport à $p$}\\
		\sum_{k = 0}^\infty k p^{k - 1} = \frac{1}{(1 - p)^2}
	\end{array}\]
	\item[(e)] Loi de Poisson de paramètre $\lambda > 0$.
	
	C'est la loi d'une variable aléatoire $X$ à valeur dans $\mathbb{N}$ avec $\mathbb{P}(X = n) = \exp^{-\lambda} \frac{\lambda^n}{n!}, n \geq 0$.
	\[\begin{array}{l}
		\sum_{n = 0}^\infty \mathbb{P} (X = n) = 1\\
		\text{car } \exp^\lambda = \sum_{n = 0}^\infty \frac{\lambda^n}{n!}\\
		\mathbb{E}(X) = \sum_{n = 0}^\infty n \exp^{-\lambda} \frac{\lambda^n}{n!}\\
		= \exp^{-\lambda} \sum_{n = 1}^\infty n \frac{\lambda^n}{n!}\\
		= \lambda
	\end{array}\]
\end{enumerate}


Lois continues:
\begin{enumerate}
	\item[(a)] Loi uniforme sur $[a,b], a < b$.
	
	C'est la loi d'une variable aléatoire $X$ de densité $p(x) = \frac{1}{b - a} 1_{\{a \leq x \leq b\}}$.
	\[\begin{array}{l}
		\int p(x) \dx{x} = 1 \text{ car } \int_a^b \frac{1}{b - a} \dx{x} = 1\\
		\mathbb{E}(X) = \int x p(x) \dx{x} = \frac{1}{b - a} \int_a^b x \dx{x} = \frac{1}{2}(b + a)
	\end{array}\]
	\item[(b)] Loi exponentielle de paramètre $\lambda > 0$.
	
	C'est la loi d'une variable aléatoire $X$ à valeur dans $\mathbb{R}_+$ de densité $p(x) = \lambda \exp^{-\lambda x} 1_{(x > 0)}$.
	\[\begin{array}{l}
		\int p(x) \dx{x} = \int_0^\infty \lambda \exp^{-\lambda x} \dx{x} = 1\\
		\mathbb{E}(X) = \int_0^\infty x \lambda \exp^{-\lambda x} \dx{x}\\
		=^{\lambda x = y} \int_0^\infty y \exp^{-y} \frac{\dx{y}}{\lambda} = \frac{1}{\lambda}\\
		\text{Notation: } X \sim exp(\lambda)\\
		\mathbb{P}(X > b) = \mathbb{E} 1_{]b,\infty[}(x)\\
		= 
		int_\mathbb{R} 1_{]b,\infty[} (x) p(x) \dx{x}\\
		= \int_b^\infty p(x) \dx{x}\\
		= \int_b^\infty \lambda \exp^{-\lambda x} \dx{x}\\
		= \exp^{-\lambda b}\\
		\mathbb{P}(A|B) = \frac{\mathbb{P}(A \cap B)}{\mathbb{P}(B)}\\
		\text{Si } \mathbb{P}(B) > 0, \forall a,b > 0\\
		\mathbb{P}(x > a + b | x > b) = \frac{\mathbb{P}(x > a + b)}{\mathbb{P}(x > b)}\\
		= \exp^{-\lambda a} = \mathbb{P}(x >a)		
	\end{array}\]
	i.e. conditionné à $\{x > b\}, x - b$ suit la même lui que $X$. C'est une propriété "perte de mémoire".
	\item[(c)] Loi gaussienne (loi normale) $\sim \mathcal{N}(m, \sigma^2)$. 
	
	C'est la loi d'une variable aléatoire $X$ réelle de densité $\frac{1}{\sqrt{2 \pi \sigma^2}} \exp^{-\frac{(x - m)^2}{2 \sigma^2}}, x \in \mathbb{R}$.
	
	$m$ sera l'espérance de $X$ et $\sigma$ sera la variance de $X$.
	\[\begin{array}{l}
		\sigma^2 := var(x) = \mathbb{E}[(X - \mathbb{E}X)^2] = \mathbb{E}(X^2)-(\mathbb{E}X)^2\\
		\int_{-\infty}^\infty \frac{1}{\sqrt{2 \pi \sigma^2}} \exp^{-\frac{(x - m)^2}{2 \sigma^2}} \dx{x} =^? 1\\
		\int_{-\infty}^\infty \frac{1}{\sqrt{2 \pi \sigma^2}} \exp^{-\frac{(x - m)^2}{2 \sigma^2}} \dx{x} =_{\dx{x} = \sigma \dx{y}}^{x = m + y \sigma} \int_{-\infty}^\infty \frac{1}{\sqrt{2 \pi}} \exp^{-\frac{y^2}{2}} \dx{y}\\
		\mathbb{E}X = \int_{-\infty}^\infty x \frac{1}{\sqrt{2 \pi \sigma^2}} \exp^{-\frac{(x - m)^2}{2 \sigma^2}} \dx{x} \\
		=^{x = y \sigma + m} \int_{-\infty}^\infty (m + y \sigma) \frac{1}{\sqrt{2 \pi}} \exp^{-\frac{y^2}{2}} \dx{y}\\
		= m \int_{-\infty}^\infty \frac{1}{\sqrt{2 \pi}} \exp^{-\frac{y^2}{2}} \dx{y} + \sigma \int_{-\infty}^\infty y \frac{1}{\sqrt{2 \pi}}\\
		= m\\
		X \sim \mathcal{N}(m, \sigma^2)\\
		\text{Soit } a > 0, b \neq 0\\
		Y = a X + b\\
	\end{array}\] 
	QUESTION - Quelle est la loi de $Y$?
	\[\begin{array}{l}
		Y \sim \mathcal{N}(a m + b, a^2 \sigma^2)\\
		f:\text{borélienne} \geq 0\\
		\mathbb{E} f(Y) = \dots = \int f(x) p(x) \dx{x}\\
		\mathbb{E} f(a X + b) = \int_{-\infty}^\infty f(a X + b) \frac{1}{\sqrt{2 \pi \sigma^2}} \exp^{-\frac{(x - m)^2}{2 \sigma^2}} \dx{x}\\
		=_{y = a x + b} \int_{-\infty}^\infty f(y) \frac{1}{\sqrt{2 \pi \sigma^2 a^2}} \exp^{-\frac{(y - (b + a m))^2}{2 \sigma^2 a^2}} \dx{y}
	\end{array}\]

	\begin{corollary}
		Si $\sim \mathcal{N}(m, \sigma^2)$ alors $\frac{X - m}{\sigma} \sim \mathcal{N}(0,1)$.
		
		$Y \sim \mathcal{N}(0,1)$ alors $m + \sigma Y \sim \mathcal{N}(m,\sigma^2)$
	\end{corollary}
\end{enumerate}


\section{Fonction de répartition d'une variable aléatoire réelle}

Soit $X$ une variable aléatoire réelle.

\begin{definition}
	La fonction de répartition de $X$ est la fonction $F_x:\mathbb{R} \rightarrow [0,1]$.
	\[F_x(t) := \mathbb{P}(x \leq t) = P_x(]-\infty,t[), \forall t \in \mathbb{R}\]
\begin{enumerate}
	\item $F_x(t)$ est croissante en $t$.
	\item $\lim_{t \rightarrow \infty} F_x(t) =^{\text{convergence monotone}} P_x(]-\infty,\infty) = 1$.
	
	$\lim_{t \rightarrow -\infty} F_x(t) = P_x(\emptyset) = 0$.
	\item $F_x$ est continue à droite: $\forall t_n \searrow_{t_n > t} t, F_x(t_n) \rightarrow_{n \rightarrow \infty} F_x(t)$ car $]-\infty,t_n] \searrow ]\infty,t]$.
	\item Si $t_n \uparrow t, t_n < t, \lim_{n \rightarrow \infty} F_x(t_n) = ?$.
	\[\begin{array}{l}
		F_x(t_n) = P_x(]-\infty,t_n]) \rightarrow P_x(]-\infty,t[)\\
		\text{Or } (]-\infty,t_n])  \nearrow_{n \rightarrow \infty} (]-\infty,t[)\\
		\text{i.e. $F_x$ a une limite à gauche: }\lim_{t_n \nearrow t, t_n < t}F_x(t) \equiv F_x(t-) = P_x(]-\infty,t[)\\
		\text{Donc } F_x(t) - F_x(t-) = P_x(\{t\}) = \mathbb{P}(X = t) 
	\end{array}\]
	\item 
	\[\begin{array}{l}
		\forall a < b\\
		\mathbb{P}(a \leq X \leq b) = F_x(b) - F_x(a-)\\
		\mathbb{P}(a < X \leq b) = F_x(b) - F_x(a)\\
		\mathbb{P}(a < X < b) = F_x(b-) - F_x(a)
	\end{array}\]
\end{enumerate}
\end{definition}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Insert here....maybe         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Loi discrete with figures      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bernoulli, binomiale, exponentielle %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Tribu engendrée par une variable aléatoire}

Soit $X$ une variable aléatoire dans $(E, \mathcal{E})$ la tribu engendrée par $X$, notée $\sigma (X)$, est par définition la plus petite tribu qui rend $X$ mesurable.
\[
\sigma (X) = \{X^{-1}(B): B \in \mathcal{E}\}
\]

\begin{remark}
	On peut généraliser cette définition à une famille quelconque $(X_i)_{i \in I}$.
	\[
		\sigma (X_i, i \in I) := \sigma (X_i^{-1}(B_i), B_i \in \mathcal{E}_i, i \in I)
	\]
\end{remark}

\begin{proposition}
	Soit $Y$ une variable aléatoire réelle. Alors sont équivalentes:
	\begin{enumerate}
		\item[(i)] $Y$ est $\sigma$-mesurable.
		\item[(ii)] $\exists$ fonction mesurable $f:E \rightarrow \mathbb{R}$ telle que $Y = f(X)$.
	\end{enumerate}
\end{proposition}

\begin{proof}
	$(ii) \Rightarrow (i)$ immédiate.
	
	Composition des fonctions mesurables reste mesurable.
	
	$(i) \Rightarrow (ii)$

	En écrivant $Y = Y^+ - Y^-$. On se ramène au cas $Y > 0$.
	
	On sait que $Y$ est la limite d'une suite croissante de variables aléatoires étagées.
	
	Si $Y = \sum_1^n \lambda_i 1_{A_i}$.
	
	$A_i \in \sigma(X), \exists B_i$ telle que $A_i = X^{-1}(B_i)$.
	
	Donc $Y = \sum_{i = 1}^n \lambda_i 1_{B_i}(x) = f(x)$ avec $f(x) = \sum_1^n \lambda_i 1_{B_i}(x)$.
\end{proof}

% kate: default-dictionary fr_FR;
