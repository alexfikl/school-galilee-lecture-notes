% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\documentclass[a4paper,twocolumn]{report}

% {{{ packages

\usepackage{xltxtra}
\usepackage{polyglossia}

% math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{mathrsfs}
\usepackage{cases}

% formatting
\usepackage[
    top=0.5in, bottom=0.5in, left=0.5in, right=0.5in
    ]{geometry}
\usepackage[shortlabels]{enumitem}

% }}}

% {{{ formatting

\renewcommand{\thesection}{\arabic{section}}
\renewcommand{\arraystretch}{1.5}

% }}}

% {{{ commands

\NewDocumentCommand \dx { m } {\,\mathrm{d} #1}
\NewDocumentCommand \pd { m m } { \dfrac{\partial #1}{\partial #2} }
\NewDocumentCommand \abs { m } { \left\lvert\, #1\, \right\rvert }

\NewDocumentCommand \ndist { m m } {\mathcal{N}(#1, #2)}
\NewDocumentCommand \E { m } {\mathbb{E}\left[#1\right]}
\NewDocumentCommand \V { m } {\mathbb{V}\left[#1\right]}

\newtheorem{shorthm}{T}

% }}}

\begin{document}
\small
\section{Chaînes de Markov}

\begin{shorthm}
Un processus $(X_n)_{n \geq 0}$ est appelé une chaîne de Markov si, $\forall n \geq 1,
\forall i_k \in \mathbb{E} $, on a:
\[
\mathbb{P}(X_{n + 1} = i_{n + 1} | X_n = i_n, \dots, X_0 = i_0) = \mathbb{P}(X_{n + 1} = i_{n + 1} | X_n = i_n)
\]

$X_{kn}$ et $X_{n + k}$ sont aussi des chaînes de Markov. Si $i_{n + 1}$ et $i_n$
ne dépend pas de $n$, le chaîne est homogène.
\end{shorthm}

\begin{shorthm}
Soient P et Q deux noyaux (ou matrices, ou probabilités) de transition sur
$\mathbb{E}$, $f: \mathbb{E} \to \mathbb{R}_+$ une fonction, $\mu$ un mesure sur $\mathbb{E}$,
$\nu$ la loi initiale de $X_0$, $\mathbb{P}_\nu$ la loi de la
chaîne de loi initiale $\nu$ et $A \in \mathcal{F}$. Alors, on a:

\begin{itemize}
    \item $\mathbb{P}(X_{n + 1} = j | X_n = i) = P_{ij}$
    \item $(Pf)_i = \sum_{j \in \mathbb{E}} P_{ij} f_j$
    \item $(\mu P)_i = \sum_{j \in \mathbb{E}} \mu_j P_{j, i} (\mu P(\mathbb{E}) = \mu(\mathbb{E}) = \sum_i \mu_i)$
    \item $\mathbb{P}_\nu(A) = \sum_{x \in \mathbb{E}} \nu(x) \mathbb{P}_x(A)$
    \item $\mathbb{E}_x[f(X_n)] = P^n f(x)$
    \item $\mathbb{E}_\nu[f(X_n)] = \sum_x \nu(x)P^nf(x)$
    \item $\mathbb{P}_x(X_n = y) = P^n(x, y)$
    \item $\mathbb{P}(X_n = i_n, \dots, X_0 = i_0) = \mathbb{P}(X_0 = i_0) P_{i_0, i_1} \times \dots \times P_{i_{n - 1}, i_n}$
\end{itemize}
\end{shorthm}

\begin{shorthm}[Propriété de Markov simple]
Soient $f:\mathbb{E}^N \to \mathbb{R}_+$ bornée, $\nu$ une loi sur $\mathbb{E}$ et $H_n \in \mathcal{F}_n$:
\[
\mathbb{E}_\nu[f(X_n, X_{n + 1}, \dots) 1_{H_n}] = \mathbb{E}[F(X_n) 1_{H_n}]
\]
où $F(x) = \mathbb{E}_x[f(X_0, \dots)], \forall x \in \mathbb{E}$.

On peut dire aussi:
\[
\begin{array}{l}
\mathbb{P}_\nu(A \cap \{X_n = x\} \cap \{X_{n + 1} = a_1, \dots, X_{n + k} = a_k\}) = \\
\mathbb{P}_\nu(A \cap \{X_n = x\}) \cdot \mathbb{P}_x(\{X_1 = a_1, \dots, X_k = a_k\})
\end{array}
\]
\end{shorthm}

\section{Classification des états}

\begin{shorthm}
Soit $A \subset \mathbb{E}$.
\begin{description}
    \item[Temps d'entrée] $T_A = \inf\{ n \geq 0 \,|\, X_n \in A\}$
    \item[Temps de retour] $\tau_A = \inf\{n \geq 1 | X_n \in A\}$; $X_n \notin A \Rightarrow T_A = \tau_A$
    \item[Nombre de visites] $N_A = \sum_{n = 0}^\infty 1_{\{X_n \in A\}}$
    \item[Fonction de Green] $G(x, y) = \mathbb{E}_x[N_y] = \sum_{n = 0}^\infty P^n(x, y)$
    \item[État récurrent] $\mathbb{P}_x(\tau_x < \infty) = 1 \Leftrightarrow N_x = \infty \Leftrightarrow G(x, x) = \infty$
    \item[État transient] $\mathbb{P}_x(\tau_x < \infty) < 1 \Leftrightarrow N_x < \infty \Leftrightarrow G(x, x) < \infty$
    \item[État absorbant] $P_{x, x} = 1$
    \item[État accessible] $x \rightarrow y$ si $\exists n \geq 1$ t.q. $P^n(x, y) > 0$
    \item[États communicants] $x \rightarrow y$ et $y \rightarrow x \Leftrightarrow x \leftrightarrow y$
    \item[État non-retour] $P^n(x, x) = 0, \forall n \geq 1$
\end{description}
\end{shorthm}

\begin{shorthm}
Classe fermé: $\forall x \in \mathcal{C}, \forall y \notin \mathcal{C} \implies x \nrightarrow y$ et $P(x, y) = 0$.
\end{shorthm}

\begin{shorthm}[Probabilité d'absorption]
Soient $C_i$ des classes récurrentes et $T$ une classe transient. On a:
\begin{enumerate}[(i)]
    \item $x \in C_i, q_i(x) = 1$
    \item $x \in C_j, q_i(x) = 0, j \neq i$
    \item $x \in T, q_i(x) = \sum_{y \in C_i} P_{x, y} + \sum_{z \in T} P_{x, z} q_i(z)$
\end{enumerate}
\end{shorthm}

\begin{shorthm}
Une mesure (ou probabilité) $\pi$ est dite invariante si $\pi P = \pi \Leftrightarrow P^T \pi^T = \pi^T$

Si $X_n$ est une chaîne de Markov récurrent positive, il existe une mesure invariante
$\pi$ et $\forall x \in \E, \mathbb{E}_x[\tau_x] < \infty$ et
$\mathbb{E}_x[\tau_x] = \frac{1}{\pi(x)}$.
\end{shorthm}

\begin{shorthm}[Période]
La période d'un état est $d(x) = pgcd\{ n \geq 1 | P^n > 0 \}$. Si $x \leftrightarrow y$
alors $d(x) = d(y)$.
\end{shorthm}

\begin{shorthm}
Si $X_n$ est irréductible (tous les états communiquent), récurrent positive
($P^{n \to \infty}(x, y) > 0$) et apériodique ($d(x) = 1$):
\[
\begin{array}{l}
    \mathbb{P}_\nu(X_n = x) \xrightarrow[]{n \to \infty} \pi(x) \\
    \mathbb{E}_\nu[f(X_n)] \xrightarrow[]{n \to \infty} \int f \dx{\pi}
\end{array}
\]
\end{shorthm}

\begin{shorthm}[Théorème ergodique]
Soit $m$ une mesure sur $\mathbb{E}$, $m(x) = \mathbb{E}_x[\sum_{k = 0}^{\tau_x - 1} f(X_k)]$.
Alors, on a:
\[
\frac{\sum_{k = 0}^{n - 1}f(X_k)}{\sum_{k = 0}^{n - 1} g(X_k)}
\xrightarrow[]{n \to \infty} \frac{\int f(x) \dx{m}}{\int g(x) \dx{m}}
\]

Si $m$ est la mesure invariante, on a:
\[
\frac{1}{n}\sum_{k = 0}^{n - 1}f(X_k) \xrightarrow[]{n \to \infty} \int f \dx{m} = \sum_{x \in \mathbb{E}} f(x) m(x)
\]
\end{shorthm}

\section{Espérances conditionnelles}

Sur l'espace de probabilité $(\Omega, \mathcal{F}, \mathbb{P})$.
\begin{shorthm}[Espérance conditionnelle]
Pour toute $X$ v.a. positive ou intégrable, $\mathcal{G} \subset \mathcal{F}$,
il existe:
\begin{enumerate}[(i)]
    \item $\mathbb{E}[X|\mathcal{G}]$ $\mathcal{G}$-mesurable.
    \item $\forall A \in \mathcal{G}, \mathbb{E}[X 1_A] = \mathbb{E}[\mathbb{E}[X|\mathcal{G}] 1_A]$.
    \item[(ii)'] $\forall Z \,v.a\,  \mathcal{G}$-mesurable t.q $XZ \geq 0$ ou $XZ \in L^1$:
    \[\mathbb{E}[XZ] = \mathbb{E}[\mathbb{E}[X|\mathcal{G}]Z]\]
\end{enumerate}
\end{shorthm}

\begin{shorthm}
Toute v.a. réele $\sigma(\xi)$-mesurable est de forme $f(\xi)$:
\[\mathbb{E}[X|\sigma(\xi)] = f(\xi).\]
\end{shorthm}

\begin{shorthm}
Quelques propriétés:
\begin{enumerate}[a)]
    \item $\mathbb{E}[\mathbb{E}[X|\mathcal{G}]] = \mathbb{E}[X]$. ($A = \Omega$)
    \item $\forall c \in \mathbb{R}, \mathbb{E}[c|\mathcal{G}] = c$.
    \item $\mathbb{E}[X|\mathcal{G}] = X$ si $X$ est $\mathcal{G}$-mesurable.
    \item $\mathbb{E}[XY|\mathcal{G}] = X\mathbb{E}[Y|\mathcal{G}]$ si $X$ est
    $\mathcal{G}$-mesurable et $Y, XY \in L^1$.
    \item $\mathbb{E}[X|\mathcal{G}] = \mathbb{E}[X]$ si $X$ est indépendant de $\mathcal{G}$.
    \item $\mathbb{E}[aX + bY|\mathcal{G}] = a\mathbb{E}[X|\mathcal{G}] + b\mathbb{E}[Y|\mathcal{G}]$
    \item \emph{(Inégalité de Jensen)} $\mathbb{E}[\phi(X)|\mathcal{G}] \geq \phi(\mathbb{E}[X|\mathcal{G}])$.
    \item \emph{(Inégalité de Hölder)} Soient $p, q > 1$ et $\frac{1}{p} + \frac{1}{q} = 1$.
    Soient $X \in L^p$ et $Y \in L^q$. Alors on a:
    \[\mathbb{E}[XY|\mathcal{G}] \leq \mathbb{E}[|X|^p|\mathcal{G}]^{\frac{1}{p}} \mathbb{E}[|Y|^q|\mathcal{G}]^{\frac{1}{q}}\]
\end{enumerate}
\end{shorthm}

\begin{shorthm}
$\forall X_n \geq 0$.
\begin{description}
    \item[Lemme de Fatou] $\mathbb{E}[\liminf_{n \to \infty} X_n|\mathcal{G}] \leq \liminf_{n \to \infty}\mathbb{E}[X_n|\mathcal{G}]$.
    \item[TCM] $X_n$ croissante p.s.: $\mathbb{E}[\lim_n X_n|\mathcal{G}] = \lim_n \mathbb{E}[X_n|\mathcal{G}]$
    \item[TCD] $X_n \to X$ et $\sup_n |X_n| \in L^1$: $\mathbb{E}[X|\mathcal{G}] = \lim_n \mathbb{E}[X_n|\mathcal{G}]$
\end{description}
\end{shorthm}

\begin{shorthm}[Double conditionnement]
Soit $\mathcal{G}_1 \subset \mathcal{G}_2 \subset \mathcal{F}$ et $X \in L^1(\mathcal{F})$:
\[\mathbb{E}[\mathbb{E}[X|\mathcal{G}_1]|\mathcal{G}_2] = \mathbb{E}[X|\mathcal{G}_1]\]
\end{shorthm}

\begin{shorthm}
Soit $(X, Y)$ une couple de v.a avec la densité $f(x, y)$.
\begin{align*}
    \mathbb{E}[\varphi(X, Y)|Y=y] =& \int \varphi(x, y) f_{X|Y=y}(x, y) \dx{x} \\
    f_{X|Y=y}(x) =& \frac{f(x, y)}{f_Y(y)} \\
    f_Y(y) =& \int f(x, y) \dx{x}
\end{align*}
\end{shorthm}

\begin{shorthm}[Filtration]
Une filtration est une suite croissante de tribus $\mathcal{F}_n$, $\forall n \geq 0$,
$\mathcal{F}_n \subset \mathcal{F}_{n + 1}$ .
\end{shorthm}

\begin{shorthm}[Temps d'arrêt]
Un $\mathcal{F}_n$-temps d'arrêt est une v.a. $\tau: \Omega \to \mathbb{N}$ t.q
$\forall n \geq 0, \{\tau \leq n \} \in \mathcal{F}_n$.

$\mathcal{F}_\tau$ est la tribu engendrée par des événements antérieurs à $\tau$:
\[\mathcal{F}_\tau = \{ A \in \mathcal{F} \,|\, \forall n \geq 0, A \cap \{ \tau \geq n\} \in \mathcal{F}_n\}\]

En particulier, $\tau_1 \wedge \tau_2, \tau_1 \vee \tau_2, \tau \wedge n, \tau \vee n$ sont encore
des temps d'arrêts.
\end{shorthm}

\begin{shorthm}[Propriété de Markov forte]
Soient $\tau$ un $\mathcal{F}_n$-temps d'arrêt, $\nu$ loi initiale,
$f: \mathbb{E}^N \to \mathbb{R}$ positive ou bornée. Alors, on a $\mathbb{P}_\nu-p.s.$:
\[\mathbb{E}[f(X_\tau, X_{\tau + 1}, \dots)|\mathcal{F}_n] = \mathbb{E}_{X_\tau}[f(X_0, X_1, \dots)].\]
\end{shorthm}

\section{Martingale en temps discrets}

\begin{shorthm}Un processus $(M_n)_{n \geq 0}$ est dit une $\mathcal{F}_n$-surmartingale si:
\begin{enumerate}[(1)]
    \item $M_n$ est $\mathcal{F}_n$-adapté, c'est-à-dire que $\forall n \geq 0,  M_n$
    est $\mathcal{F}_n$-mesurable.
    \item $\forall n \geq 0,  M_n$ est intégrable (ou $M_n \geq 0$, ou $M_n \leq 0$ p.s.)
    \item $\forall n \geq 0, \mathbb{P}$-p.s. $\mathbb{E}[M_{n + 1}|\mathcal{F}_n] \leq M_n$.
\end{enumerate}
Si $\mathbb{E}[M_{n + 1}|\mathcal{F}_n] = M_n$, $M_n$ est une $\mathcal{F}_n$-martingale.

Si $\mathbb{E}[M_{n + 1}|\mathcal{F}_n] \geq M_n$, $M_n$ est une $\mathcal{F}_n$-sousmartingale.
\end{shorthm}

\begin{shorthm}
Quelques propriétés des martingales:
\begin{enumerate}[(1)]
    \item $\mathbb{E}[M_{n + p}|\mathcal{F}_n] \leq M_n$.
    \item $\mathbb{E}[M_{n + p}] \leq \mathbb{E}[M_n]$.
    \item si $M_n$ est une $\mathcal{F}_n$-sousmartingale t.q. $\mathbb{E}[M_n] = \mathbb{E}[M_0]$,
    alors $M_n$ est une $\mathcal{F}_n$-martingale.
    \item $\mathcal{G}_n \subset \mathcal{F}_n$, $M_n$ est aussi une $\mathcal{G}_n$-surmartingale.
    \item $\forall a, b \in \mathbb{R}$ et $M_n, N_n$ sont deux $\mathcal{F}_n$-
    sousmartingales: $aM_n + bN_n$ reste $\mathcal{F}_n$-sousmartingale.
    \item si $M_n, N_n$ sont deux $\mathcal{F}_n$-martingales: $\sup \{ M_n, N_n \}$
    est une $\mathcal{F}_n$-sousmartingale.
    \item si $M_n, N_n$ sont deux $\mathcal{F}_n$-martingales: $\inf \{ M_n, N_n \}$
    est une $\mathcal{F}_n$-surmartingale.
    \item si $M_n$ est une $\mathcal{F}_n$-martingale et $\phi: \mathbb{R} \to \mathbb{R}$
    convexe t.q. $\phi{M_n} \in L^1$. Alors, $\phi(M_n)$ est une $\mathcal{F}_n$-sousmartingale.
\end{enumerate}
\end{shorthm}

\begin{shorthm}
Un processus $A_n$ est dit $\mathcal{F}_n$-prévisible si:
\begin{itemize}
    \item $A_0$ est $\mathcal{F}_0$-mesurable
    \item $A_n$ est $\mathcal{F}_{n - 1}$-mesurable
\end{itemize}
\end{shorthm}

\begin{shorthm}[Décomposition de Doob] Toute $\mathcal{F}_n$-sousmartingale $X_n$
s'écrit comme: $X_n = M_n + A_n$, où $M_n$ est une $\mathcal{F}_n$-martingale et
$A_n$ est un processus croissant $\mathcal{F}_n$-prévisible et nul en $0$.
\end{shorthm}

\begin{shorthm}[Théorème d'arrêt] Soit $S$ et $T$ deux $\mathcal{F}_n$-temps d'arrêt
t.q $S \leq T \leq N \in \mathbb{N}$ ps. Soit $M_n$ une $\mathcal{F}_n$-surmartingale:
\[\mathbb{E}[M_T|\mathcal{F}_S] \leq M_S~p.s. \Leftrightarrow \mathbb{E}[M_T] \leq
\mathbb{E}[M_S] \leq \mathbb{E}[M_0]~p.s.\]
\end{shorthm}

\begin{shorthm}
Une famille $\{ X_k, k \geq 0 \}$ est uniformément intégrable si:
\[\sup_k \mathbb{E}[|X_k| 1_{\{|X_k| > \lambda\}}] \xrightarrow[]{\lambda \to \infty} 0.\]
\end{shorthm}

\begin{shorthm}[Inégalité maximale] Soit $X_n$ une sousmartingale positive. Alors,
$\forall n, \lambda > 0$, on a:
\[\mathbb{P}(\max_{0 \leq k \leq n} X_k \geq \lambda) \leq \frac{1}{\lambda}\mathbb{E}[X_n]\]

Si $X_n$ est de carré intégrable:
\[\mathbb{P}(\max_{0 \leq k \leq n} X_k \geq \lambda) \leq \frac{1}{\lambda^2}\mathbb{E}[X_n^2]\]
\end{shorthm}

\begin{shorthm}[Inégalité de Doob dans $L^p$]
Pour toute sous martingale positive $X_n$, on a:
\[\|\max_{0 \leq k \leq n} X_k\|_{L^p} \leq \frac{p}{p - 1} \|X_n\|_{L^p}\].
\end{shorthm}

\begin{shorthm}
$M_n$ converge dans $L^2$ si et seulement si: $\sup_{n \geq 1} \|M_n\|_{L^2} < \infty$
et $\lim_{n \to \infty} M_n = M_{\infty}$ et $M_n = \mathbb{E}[M_\infty|\mathcal{F}_n]$.
\end{shorthm}

\begin{shorthm} Convergence presque sûre:

\begin{itemize}
    \item Si $M_n$ converge dans $L^2$, alors $M_n$ converge p.s vers $M_\infty$
    \item Si $M_n$ est une sousmartingale t.q. $\sup_{n \geq 1} \mathbb{E}[M_n^+] < \infty$,
    alors $M_n$ converge p.s. vers une limite intégrable.
    \item toute sousmartingale positive converge presque sûrement.
\end{itemize}
\end{shorthm}

\begin{shorthm}
Soit $M_n$ une $\mathcal{F}_n$-martingale. Alors on a équivalence entre:
\begin{enumerate}[(1)]
    \item $\exists Y \in L^1$ t.q. $M_n = \mathbb{E}[Y|\mathcal{F}_n]$ (martingale régulière)
    \item $M_n$ est uniformément intégrable.
    \item $M_n$ converge vers $M_\infty$ dans $L^1$ et $Y = M_\infty$.
\end{enumerate}

\end{shorthm}

\end{document}

% kate: default-dictionary fr_FR;
