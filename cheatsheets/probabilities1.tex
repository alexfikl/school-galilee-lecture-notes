% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\documentclass[a4paper,8pt,twocolumn]{report}

% {{{ packages

\usepackage{xltxtra}
\usepackage{polyglossia}

% math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{mathrsfs}
\usepackage{cases}

% formatting
\usepackage[
    top=0.5in, bottom=0.5in, left=0.5in, right=0.5in
    ]{geometry}
\usepackage[shortlabels]{enumitem}

% }}}

% {{{ formatting

\renewcommand{\thesection}{\arabic{section}}
\renewcommand{\arraystretch}{1.5}

% }}}

% {{{ commands

\NewDocumentCommand \dx { m } {\,\mathrm{d} #1}
\NewDocumentCommand \pd { m m } { \dfrac{\partial #1}{\partial #2} }
\NewDocumentCommand \abs { m } { \left\lvert\, #1\, \right\rvert }

\NewDocumentCommand \ndist { m m } {\mathcal{N}(#1, #2)}
\NewDocumentCommand \E { m } {\mathbb{E}\left[#1\right]}
\NewDocumentCommand \V { m } {\mathbb{V}\left[#1\right]}

\newtheorem{shorthm}{T}

% }}}

\begin{document}
\small

\section{Intégration}

\begin{shorthm}[TCM]
    Soit $(f_n)$une suite croissant de fonctions mesurables et positives (à valeurs
    dans $[0, \infty]$). Soit $f = \lim_{n \to \infty} \uparrow f_n$, \textbf{alors} on a:
    $\int \lim_{n \to \infty} f \dx{\mu} = \lim_{n \to \infty} \int f_n \dx{\mu}$.
\end{shorthm}

\begin{shorthm}[TCD]
    Soit $(f_n)_{n \geq 1}$ une suite de fonctions réelles dans $\mathcal{L}^1(E,
    \mathcal{A}, \mu)$. On suppose que $\exists f$ mesurable tells que $\mu$-p.p.
    x, $\lim_{n \to \infty} f_n(x) = f(x)$ et $\exists g$ mesurable positive telle
    que $\int g \dx{\mu} < \infty$ et $\forall n$, $\abs{f_n(x)}{} \leq g(x),~
    \mu$-p.p. x. \textbf{Alors}: $\int \lim_{n \to \infty} f_n \dx{\mu} = \lim_{n \to \infty}
    \int f_n \dx{\mu}$.
\end{shorthm}

\begin{shorthm}[Continuité]
    Soit $u_0 \in U$. Supposons:
\begin{enumerate}[(i)]
    \item $\forall u \in U$, $x \mapsto f(u, x)$ est mesurable.
    \item $\mu(\!\dx{x})$-p.p. x, $u \mapsto f(u, x)$ est mesurable en $u_0$.
    \item $\exists g \geq 0$ mesurable tells que $\int g \dx{\mu} < \infty$ et
    $\forall u \in U$, $\abs{f(u, x)}{} \leq g(x)$, $\mu(\!\dx{x})$-p.p.
\end{enumerate}
    Alors $F(u) = \int f(u, x)~\mu(\!\dx{x})$ est bien définie sur $u \in U$ et $F$
    est continue en $u_0$.
\end{shorthm}

\begin{shorthm}[Dérivation]
    Soient $I$ un intervalle ouvert de $\mathbb{R}$. $u_0 \in I$ et
    $f : I \times E \to \mathbb{R}$. Supposons que:
\begin{enumerate}[(i)]
    \item $\forall u \in I$, $x \mapsto f(u, x) \in \mathcal{L}^1(E, \mathcal{A},
    \mu)$.

    \item $\mu(\!\dx{x})$-p.p., $u \mapsto f(u, x)$ est dérivable en $u_0$ de dérivée
    $\pd{f}{u}(u_0, x)$.

    \item $\exists g \geq 0$ tells que $\int g \dx{\mu} < \infty$ tells que:
\[
    \frac{\abs{f(u, x) - f(u_0, x)}{}}{\abs{u - u_0}{}} \leq g(x),~ \mu-p.p.
\]
\end{enumerate}

    Alors $F(u) = \int f(u, x) \dx{\mu(x)}$ est dérivable en $u_0 \in I$ et sa dérivée
    est $F'(u_0) = \int \pd{f}{u} \dx{\mu(x)}$.
\end{shorthm}

\begin{shorthm}[Fubini-Tonelli]
    Soient $\mu$ et $\nu$ 2 mesures, $\sigma$-finie. Soient $f:(E \times F,
    \mathcal{A} \otimes \mathcal{B}) \to [0, \infty]$ une fonction mesurable
    positive. Alors les propriétés suivantes sont équivalents:
\begin{enumerate}[(i)]
    \item $x \mapsto \int f(x,y)~\nu(\!\dx{y}) \text{ est } \mathcal{A}
    \text{-mesurable}$ et $y \mapsto \int f(x,y)~\mu(\!\dx{x})$ est $\mathcal{B}\!\!$
    -mesurable.
    \item $\int_{E \times F} f \dx{\mu \otimes \nu} = \int \left(\int f(x,y)~
    \mu(\!\dx{x})\right)\nu(\!\dx{y}) = \int \left(\int f(x,y)\right.$ $\left.
    \nu(\dx{y}) \right)\mu(\!\dx{x})$
\end{enumerate}
\end{shorthm}

\begin{shorthm}[Fubini-Lebesgue]
    Soient $f \in \mathcal{L}^1 (E \times F, \mathcal{A} \otimes \mathcal{B}, \mu
    \otimes \nu) \Leftrightarrow \int \abs{f} \dx{\mu \otimes \nu} < \infty$.
    Alors si:
\begin{enumerate}[a)]
    \item $\mu(\!\dx{x})$-p.p. les fonctions $y \mapsto f(x,y)$ et $x \mapsto f(x,y)$
    sont dans $\mathcal{L}^1(f, \mathcal{B}, \nu)$
    \item les fonctions $x \mapsto \int f(x,y)~\mu(\!\dx{y})$ et $y \mapsto \int f(x,y)
    ~\nu(\!\dx{x})$ sont dans $\mathcal{L}^1(f, \mathcal{B}, \nu)$.
\end{enumerate}
    On a:
{\tiny
\[
    \int_{E \times F} f \dx{\mu \otimes \nu} = \int_E \left( \int_F f(x,y)
    ~\nu(\!\dx{y}) \right)~\mu(\!\dx{x}) = \int_F \left( \int_E f(x,y)~\mu(\!\dx{x})
    \right)~\nu(\!\dx{y})
\]
}
\end{shorthm}

\begin{shorthm}[Changement de variables]
    Soit $\varphi: U \to D$difféomorphisme de classe $C^1$. Alors pour toute fonction
    borélienne $f: D \to \mathbb{R}_+$, on a $\int_D f(x) \dx{x} = \int_u
    f(\varphi(u)) |J_{\varphi}(u)| \dx{u}$ où $J_{\varphi}(u) = det(\varphi'(u))$ est le
    Jacobien de $\varphi$ en $u$.
\end{shorthm}

\begin{shorthm}[Intégration par partie]
    Soient $f$ et $g$ deux fonctions mesurables de $\mathbb{R}$ dans $\mathbb{R}$
    localement intégrables (i.e. intégrables sur tout compact pour la mesure de
    Lebesgue). On pose pour $x \in \mathbb{R}$: $F(x) = \int_0^x f(t) \dx{t}$ et
    $G(x) = \int_0^x g(t) \dx{t}$. Alors, pour tous $a < b$:
\[
    \int_a^b F(t) g(t) \dx{t} = F(b) G(b) - F(a) G(a) - \int_a^b G(t) f(t) \dx{t}
\]
\end{shorthm}

\begin{shorthm}[Inégalité de Hölden]
    Soient $p$ et $q$ conjugues, si $f:E \to \mathbb{R}$ et $g:E \to \mathbb{R}$ sont
    mesurables, $\int \abs{f} \abs{g} \dx{\mu} \leq \|f\|_{L^p} \|g\|_{L^q}$.

    En particulier, si $f \in L^p$ et $g \in L^q$ alors $f g \in L^1$.
\end{shorthm}

\begin{shorthm}[Inégalité de Jensen]
    Soit $\mu$ une \emph{mesure de probabilité} (masse total 1). Soit $\varphi:
    \mathbb{R} \to \mathbb{R}_+$ une fonction convexe.

    Alors $\int \varphi \circ f \dx{\mu} \geq \varphi(\int f \dx{\mu})$.
\end{shorthm}

\begin{shorthm}[Inégalité de Minkowski]
    $\forall 1 \leq p \leq \infty,~\forall f,g \in L^p$:
\[
    \|f + g\|_{L^p} \leq \|f\|_{L^p} + \|g\|_{L^p}.
\]
\end{shorthm}

\begin{shorthm}[Radon-Nikodym]
    Soient $\mu$ et $\nu$ deux mesures $\tau$-finies sur $(E,\mathcal{A})$.

    Alors $\mu <\!\!\!< \nu$ si et seulement s'il existe une fonction $h$ mesurable
    positive telle que $\forall A \in \mathcal{A}$, $\mu(A) = \int_A h \dx{\nu}$,
    $h$ est unique $\nu$-p.p.
\end{shorthm}

\section{Probabilités}

\begin{shorthm}[Inégalité de Markov]
    Si $X \geq 0$ et $\epsilon > 0$:
\[
    P(X \geq \epsilon) \leq \frac{1}{\epsilon} \E{X}.
\]
\end{shorthm}

\begin{shorthm}[Inégalité de Biennaymé-Tchebicheff]
    Si $X \in L^2(\Omega, \mathcal{A}, P)$ et $\epsilon > 0$:
\[
    P(|X - \E{X}| \geq \epsilon) \leq \frac{1}{\epsilon^2} \V{X}.
\]
\end{shorthm}

\begin{shorthm}
    $cov(X, Y) = \E{XY} - \E{X}\E{Y}$.
\end{shorthm}

\begin{shorthm}
    Si X est une variable aléatoire à valeurs dans $\mathbb{R}^d$, la
    fonction caractéristique de X est $\Phi_X(\xi) = \E{e^{i\xi X}}$.
\end{shorthm}

\begin{shorthm}
\[
    \E{X^k} = (-1)^k i^k \Phi_X^{(k)}(0).
\]
\end{shorthm}

\begin{shorthm}
    Soit X une v.a. à valeurs dans $\mathbb{N}$. La fonction génératrice
    de X est $g_X(r) = \E{r^X} = \sum P(X = n) r^n$
\end{shorthm}

\begin{shorthm}
    Soient A, B deux événements, alors $P(A|B) = \frac{P(A \cap B)}{P(B)}$. Si A et
    B sont indépendants alors $P(A \cap B) = P(A)P(B)$.
\end{shorthm}

\begin{shorthm}[Lemme de Borel-Cantelli]
    Soit $(A_n)_{n \in \mathbb{N}}$ une suite d'événements.
\begin{itemize}
    \item Si $\sum P(A_n) < \infty \implies P(\varlimsup A_n) = 0$, nous avons
    équivalence si les $A_n$ sont indépendants.
    \item Si $\sum P(A_n) < \infty \implies P(\varlimsup A_n) = 1$, nous avons
    équivalence si les $A_n$ sont indépendants.
\end{itemize}
\end{shorthm}

\begin{shorthm}[Loi de grands nombres]
    Soit $(X_n)_{n \in \mathbb{N}}$ une suite de v.a. réelles indépendants et de
    même loi. Si $\E{X_1^2} < \infty$ on a $\frac{1}{n}(X_1 + \dots + X_n)
    \xrightarrow[]{p.s.} \E{X_1}$.
\end{shorthm}

\begin{shorthm}[Convergence presque sure]
    On dit que $(X_n)$ converge p.s vers $X$ si $P(\{\omega \in \Omega:
    \lim_{n \to \infty} X_n(\omega) = X(\omega)\})$.

    Aussi, nous avons: $X_n \xrightarrow[]{p.s.} X \Leftrightarrow P(\varlimsup
    |X_n - X| > \epsilon) = 0$.
\end{shorthm}

\begin{shorthm}[Convergence en probabilité]
    On dit que $(X_n)$ converge en probabilité vers $X$ si $\lim_{n \to \infty}
    P(|X_n - X| > \epsilon) = 0$.
\end{shorthm}

\begin{shorthm}
\begin{itemize}
    \item Si $X_n \xrightarrow[]{p.s.} X \implies X_n \xrightarrow[]{(P)} X$.
    \item Si $X_n \xrightarrow[]{(P)} X \implies$ il existe une sous-suite
    $(X_{n_k})$ qui converge p.s. vers X.
\end{itemize}
\end{shorthm}

\begin{shorthm}[Convergence en loi]
    Une suite $(X_n)$ de v.a. converge en loi vers une v.a. X, notée $X_n
    \xrightarrow[]{(loi)} X$, si $F_{X_n} \to F_x$ (la fonction de répartition
    converge). Pour des v.a. dans $\mathbb{Z}^d$: $P(X_n = x) \to P(X = x)$.
\end{shorthm}

\begin{shorthm}
    Si $X_n \xrightarrow[]{(P)} X \implies X_n \xrightarrow[]{(loi)} X$
\end{shorthm}

\begin{shorthm}[Théorème central limite]
    Soit $(X_n)_{n \geq 1}$ une suite de v.a réelles indépendants et de même loi,
    dans $L^2$. Soit $\sigma^2 = \V{X_1}$ et $\mu = \E{X_1}$. Alors:
\[
    \frac{\sum X_i - n\mu}{\sigma \sqrt{n}} \xrightarrow[]{(loi)} \ndist{0}{1}.
\]
où $\ndist{0}{1}$ est la loi gaussienne standard.
\end{shorthm}

\section{Tableaux}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Discrete Distributions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
\begin{tabular}{l|c|c|c}
Loi                     & $P(X = k)$                            & $\E{X}$                & $\V{X}$                \\\hline
$\mathcal{U}(a, b)$     & $\frac{1}{n}$                         & $\frac{a + b}{2}$     & $\frac{n^2 - 1}{12}$  \\\hline
\emph{Bern}(p)          & p si k = 1, 1 - p si k = 0            & p                     & p(1 - p)              \\\hline
$\mathcal{B}(n, p)$     & $C_n^k p^k (1 - p)^{n - k}$           & np                    & np(1 - p)             \\\hline
\emph{Géométrique}      & $(1 - p)^{k - 1}p$                    & $\frac{1}{p}$         & $\frac{1 - p}{p^2}$   \\\hline
$\mathcal{P}(\lambda)$  & $\frac{\lambda^k}{k!}e^{-\lambda}$    & $\lambda$             & $\lambda$             \\\hline
\emph{Neg}(p, r)        & $C_{k - 1}^{r - 1}(1 - p)^{k - r}p^r$ & $\frac{r}{p}$         & $\frac{r(1 - p)}{p}$  \\
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Continuous Distributions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
\begin{tabular}{l|c|c|c|c|c}
Loi     & $f_X(x)$      & $F_X(x)$      & $\Phi_X(t)$   & $\E{X}$    & $\V{X}$
\\\hline
$\mathcal{U}(a, b)$ & $\frac{1}{b - a}$     & $\frac{x - a}{b - a}$&
$\frac{e^{itb} - e^{ita}}{it(b - a)}$ & $\frac{a + b}{2}$ & $\frac{(b - a)^2}{12}$
\\\hline
\emph{exp}($\lambda$) & $\lambda e^{-\lambda x}$ & $1 - e^{-\lambda x}$ &
$\frac{\lambda}{\lambda - it}$ & $\frac{1}{\lambda}$ & $\frac{1}{\lambda^2}$
\\\hline
\emph{Cauchy}(0, 1) & $\frac{1}{\pi(1 + x^2)}$ & $\frac{\arctan x}{\pi} +
\frac{1}{2}$ & $e^{|t|}$ & 0 & ?
\\\hline
$\ndist{0}{1}$ & $\frac{1}{\sqrt{2\pi}}e^{-\frac{x^2}{2}}$ & ? &
$e^{\frac{t^2}{2}}$ & 0 & 1
\\\hline
$\ndist{\mu}{\sigma^2}$ & $\frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x - \mu)^2}{2\sigma^2}}$ &
? & $e^{i\mu t - \frac{t^2\sigma^2}{2}}$ & $\mu$ & $\sigma^2$
\\\hline
$\gamma(p, \theta)$ & $\frac{\theta^p}{\Gamma(p)} e^{-\theta x} x^{p - 1}$ &
$\frac{1}{\Gamma(p)} \gamma(p, \theta x)$ & $(1 - \frac{it}{\theta})^{-p}$ &
$\frac{p}{\theta}$ & $\frac{p}{\theta^2}$
\\\hline
$\beta(\theta, p, q)$ & $\frac{\theta}{B(p, q)} x^{p - 1} (1 - x)^{q - 1}$ &
? & ? & ? & ?
\\\hline
$\chi^2(1)$ & $\frac{1}{\sqrt{2\pi x}} e^{-\frac{x^2}{2}}$ & ? &
$(1 - it)^{-\frac{1}{2}}$ & 1 & 2
\\\hline
$\chi^2(n)$ & $\gamma(\frac{n}{2}, \frac{1}{2})$ & ? & $(1 - it)^{-\frac{n}{2}}$ &
$n$ & $2n$\\
\end{tabular}
\end{center}

\end{document}

% kate: default-dictionary fr_FR;
