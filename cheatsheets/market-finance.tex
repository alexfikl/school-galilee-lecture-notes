% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\documentclass[a4paper,twocolumn]{report}

% {{{ packages

\usepackage{xltxtra}
\usepackage{polyglossia}

% math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{mathrsfs}
\usepackage{cases}

% formatting
\usepackage[shortlabels]{enumitem}
\usepackage[
    top=0.5in, bottom=0.5in, left=0.5in, right=0.5in
    ]{geometry}

% }}}

% {{{ formatting

\renewcommand{\thesection}{\arabic{section}}
\renewcommand{\arraystretch}{1.5}

% }}}

% {{{ commands

\NewDocumentCommand \dx { m } {\,\mathrm{d} #1}
\newtheorem{shorthm}{T}

% }}}

\begin{document}
\small

\section{Acteurs économiques}

\begin{shorthm}[L'état] Financements:
\begin{itemize}
    \item impôts et taxes: TVA, ISF (Impôt de solidarité sur la fortune), CSG
    (Contribution Sociale Généralisée), RDS (Remboursement de la dette sociale), TV
    \item emprunts: OAT (Obligations Assimilables au Trésor; 2-5 ans ), BTAN (Bons du Trésor
    à intérêt annuel; 5 - 30 ans), BTF (bons du trésor; < 1 an).
    \item privatisation: la vente ou cession par l'état à des investisseurs tout ou
    partie d'une société publique (e.g Air France, France Telecom, EDF/GDF).
    \item nationalisation: un transfert de la propriété privée à l'état.
\end{itemize}
\end{shorthm}

\begin{shorthm}[Les particuliers] Financements: Livret A, LDD, PEL, CEL,
    obligations, actions, immobilier, art, matières premières, crédit, etc.
\end{shorthm}

\begin{shorthm}[Les entreprises] Financements: autofinancements, crédit, private
equity, IPO, obligations, BMTN (bons à moyen terme négociable), BT (billets de trésorerie)
\end{shorthm}

\section{Marchés financiers}

\begin{shorthm} Produits Cash:
\begin{description}
    \item[Change] devises: EUR, USD, GBP, JPY, etc
    \item[Taux] monétaire: interbancaire (EONIA European OverNight Index Average,
    EURIBOR Euro Interbank Offered Rate, LIBOR London), TCN (BTF, CD, CP,
    BMTN, BTAN); obligatoire: obligation, OAT, GILTS
    \item[Actions] actions, etc.
    \item[Matières premières] pétrole, or, oranges, faucons maltais.
\end{description}
\end{shorthm}

\begin{shorthm} Produits Dérivés (fixés aujourd'hui et reçus dans le futur): fermés
(futures, FRA, Swaps) et conditionnels (call/put, caps/floors).
\end{shorthm}

\section{Salle des marchés}

\begin{description}
    \item[Front Office]: Trader, Sales, Broker, Economiste, Analyste
Quantitatif, Commando, Structuration, Support, Projets Informatiques
    \item[Middle Office]: fait la jonction entre front et back office, saisit
    sur sur une base de donnés toutes les transactions, chiffre P\&L, procédures
    de contrôle, etc.
    \item[Back Office]: administratif et comptable, enregistre les transactions,
    reporte à BF et au Trésor toutes les opérations, gère le versement des dividendes.
\end{description}

\begin{shorthm}[La couverture] L'objectif est de fixer les caractéristique d'une
opération. Un future est un contrat standardisé permettant de s'assurer ou de
s'engager sur un prix à une date future.
\end{shorthm}

\section{BCE}

\begin{itemize}
    \item maintenir l'inflation annuelle au dessous de 2\%.
    \item Directoire: mettre en œuvre la politique monétaire, donner les instructions
    aux BCN, etc
    \item Conseil des Gouverneurs: décisions pour les missions du Eurosystème, définir
    politique monétaire.
    \item Conseil Général: collecter les informations statistiques, rapports annuels,
    procédures comptables, emploi du personnel, etc.
\end{itemize}

\begin{shorthm}[Titrisation] est une technique financière qui permet à un cédant de
transférer la propriété ou de céder des actifs à un acquéreur.
\end{shorthm}

\section{Outils mathématiques}

\begin{shorthm}[Taux d'intérêts composés] Les intérêts composés sont payés périodiquement
et réinvestis. Soient $V_A$ la valeur actuelle, $V_F$ la valeur future, $DF$ le
discount factor, $r$ le taux d'intérêt annuel et $r_p$ le taux d'intérêt périodique
($p = 2$ pour semestre, $p = 12$ pour mois, etc):
\begin{enumerate}[a)]
    \item $1 + r = \left(1 + r_{p}\right)^p$.

\textbf{Taux marché monétaire (< 1 an)}
    \item $DF = \frac{1}{(1 + r \frac{n}{360})^n}$.
    \item $V_F = V_A \cdot DF$.

\textbf{Taux marché obligatoire (> 1 an)}
    \item $DF = \frac{1}{(1 + r)^{\frac{n}{365}}}$.
    \item $V_F = V_A \cdot DF$.
\end{enumerate}
\end{shorthm}

\section{Obligations}

\begin{shorthm} Une obligation est une valeur mobilière (titre de créance)
représentant la quote-part d'un emprunt émis par une entreprise, qui donne à son
porteur un droit de créance sur celle-ci.
\end{shorthm}

\begin{shorthm}[Taux actuariel] Le taux actuariel $r_a$ d'un emprunt est le taux annuel qui
égalise à ce taux et à intérêts composés les valeurs actuelles des montants à verser
et des montants à recevoir.
\[Prix = \sum_{i = 1}^N \frac{coupon \times Nominal}{(1 + r_a)^i} + \frac{Nominal}{(1 + r_a)^N}\]
\end{shorthm}

\begin{shorthm}[Zéro Coupon] Un instrument financier ``zéro-coupon'' $ZC$ est un instrument
qui ne donne lieu à aucune détachement de coupon intermédiaire.
\[Prix = \sum_{i = 1}^N \frac{coupon \times Nominal}{(1 + ZC_i)^i} + \frac{Nominal}{(1 + ZC_N)^N}\]
\[Prix\, dirty = \frac{C_1}{(1 + r)^a} + \frac{C_2}{(1 + r)^{1 + a}} + \dots + \frac{Nominal}{(1 + r)^{N + a}}\]
où $C_i$ est le coupon payé à la date $i$ et $a$ est la fraction de période entre
la date de jouissance et le prochain coupon.
\end{shorthm}

\begin{shorthm}[Coupon couru] Le coupon couru représente l'intérêt dû sur la période qui
s'est écoulée depuis la date du dernier coupon versé.
\[Prix\, dirty = Prix + \text{coupon couru}\]
\end{shorthm}

\begin{shorthm}[Courbe de taux Spot/Forward] Le taux forward $F$ déterminé en $t$,
démarrant en $x$ et d'échéance $y$:
\[F(t, x, y - x) = \left[\frac{(1 + ZC(t, y))^{y - t}}{(1 + ZC(x, t))^{x - t}}\right]^{\frac{1}{y - x}} - 1\]
\end{shorthm}

\begin{shorthm}[Duration] Concept: représenter le nombre moyen d'années pondérées
par la valeur des Cash Flows qu'elles génèrent, pour rentabiliser le prix initial
d'une obligation.
\[P = \sum_{t = t_0}^T \frac{CF_t}{(1 + r)^t};\: D = \frac{1}{P} \sum_{t = t_0}^T \frac{tCF_t}{(1 + r)^t}\]
où $CF_t$ sont des cash flows payés par l'obligation en $t$. La duration apparaît
donc comme le point de balance de tous les flux actualisés.
\end{shorthm}

\begin{shorthm}[Sensibilité] La sensibilité d'une obligation aux taux d'intérêt $r(t)$
est égale à la variation relative du prix $P$ suite à une variation très faible de son
taux actuariel.
\[\dx{P} = P(r(t) + \dx{r(t)}) - P(R(t)) = P(R(t))\dx{R(t)} + o_1(\dx{R(t)})\]
ou en variation relative:
\[\frac{\dx{P}}{P} = \frac{P'(R)}{P(R)}\dx{R} + o_2(\dx{R}) = -S \cdot \dx{R} + o_2(\dx{R})\]
On peut dire aussi que la sensibilité est la duration modifiée:
\[S = \frac{1}{1 + r} \sum_{t = t_0}^T \frac{1}{P} \frac{tCF_t}{(1 + r)^t}\]
On pourra calculer la variation de prix:
\[S = -\frac{\dx{P}}{P\dx{r}} \Leftrightarrow \dx{P} = -S \times P \times \dx{r}\]
\end{shorthm}

\begin{shorthm}[Convexité] La convexité est tout simplement la mesure de la variation
    de la sensibilité par rapport au taux d'intérêt. La fonction qui relie la
    sensibilité et taux d'intérêt est une fonction non linéaire $\implies$ la convexité.
\[\dx{P} = P'(r(t))\dx{r(t)} + \frac{P''(r(t))}{2} \dx{r^2(t)} + o(\dx{r^2(t)})\]
ou en variation relative:
\[\frac{\dx{P}}{P} = \frac{P'(r)}{P(r)}\dx{r} + \frac{P''(r)}{2P(r)} \dx{r^2} + o(\dx{r^2})\]
\[\frac{\dx{P}}{P} = -Sens \cdot \dx{r} + \frac{1}{2} \cdot Conv \cdot \dx{r^2} + o(\dx{r^2})\]
donc:
\[C = \frac{1}{P} \cdot \frac{\dx{^2 P}}{\dx{r^2}}\]
\[C = \frac{1}{P(1 + r)^2} \sum_{t = t_0}^T \frac{t(t + 1)}{(1 + r)} \times CF_t\]
\end{shorthm}

\section{Swaps}

\begin{shorthm} Le swap de taux d'intérêt est une opération de gré à gré dans laquelle
deux contreparties s'échangent deux séries de flux d'intérêt libellés dans la même devise.
Chaque série représentant pour chaque contrepartie un emprunt et un prêt sur une même
durée.
\end{shorthm}


\begin{shorthm}[Pricing] D'un IRS (Interest Rate Swap):
\[P_t = N \left[\sum_{i = 1}^n C \frac{T_{k, i} - T_{k, i - 1}}{360} DF(t, T_{k, i})
    - \sum_{j = 1}^m V_{j - 1} \frac{T_j - T_{j - 1}}{360} DF(t, T_j)\right]\]
où $C$ est le taux constant et $V_j$ sont des taux variables. On peut aussi
remplacer $V_j$ avec sa valeur forward $F(t, V_j)$:
\[F(t, V_j) = \left(\frac{DF(t, T_j)}{DF(t, T_{j - 1}} - 1) \frac{360}{T_j - T_{j - 1}}\right)\]

Donc le prix est:
\[P_t = N\left[\sum_{i = 1}^n C \frac{T_{k, i} - T_{k, i - 1}}{360} DF(t, T_{k, i})
    - (1 - DF(t, T_m))\right]\]
En général $(T_{k, i} - T_{k, i - 1}) / 360 = 1$
\end{shorthm}
\end{document}

% kate: default-dictionary fr_FR;
