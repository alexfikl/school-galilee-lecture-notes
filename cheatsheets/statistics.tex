% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\documentclass[a4paper,twocolumn]{report}

% {{{ packages

\usepackage{xltxtra}
\usepackage{polyglossia}

% math
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{mathrsfs}
\usepackage{cases}

% formatting
\usepackage[
    top=0.5in, bottom=0.5in, left=0.5in, right=0.5in
    ]{geometry}
\usepackage[shortlabels]{enumitem}

% }}}

% {{{ formatting

\renewcommand{\thesection}{\arabic{section}}
\renewcommand{\arraystretch}{1.5}

% }}}

% {{{ commands

\NewDocumentCommand \pd { m m } { \dfrac{\partial #1}{\partial #2} }
\NewDocumentCommand \estim { m } {\hat{\theta}_n^{#1}}
\NewDocumentCommand \ndist { m m } {\mathcal{N}(#1, #2)}
\NewDocumentCommand \E { m } {\mathbb{E}\left[#1\right]}
\NewDocumentCommand \V { m } {\mathbb{V}\left[#1\right]}

\newtheorem{shorthm}{T}

% }}}

\begin{document}
\small

\section{Estimation ponctuelle}

\begin{shorthm}[Loi de grands nombres]
    Soit $(X_n)_{n \in \mathbb{N}}$ une suite de v.a. réelles indépendants et de
    même loi. Si $\E{X_1^2} < \infty$ on a $\frac{1}{n}(X_1 + \dots + X_n)
    \xrightarrow[]{p.s.} \E{X_1}$.
\end{shorthm}

\begin{shorthm}[Théorème central limite]
    Soit $(X_n)_{n \geq 1}$ une suite de v.a réelles indépendants et de même loi,
    dans $L^2$. Soit $\sigma^2 = \V{X_1}$ et $\mu = \E{X_1}$. Alors:
\[
    \frac{\sum X_i - n\mu}{\sigma \sqrt{n}} \xrightarrow[]{(loi)} \ndist{0}{1}.
\]
où $\ndist{0}{1}$ est la loi gaussienne centrée réduite.
\end{shorthm}

\begin{shorthm}
Un estimateur est:
\begin{enumerate}
    \item \textbf{sans biais} si $\E{\estim{}} = \theta$, sinon il est
    biaisé.
    \item \textbf{asymptotiquement sans biais} si
    $\E[\estim{}] \xrightarrow[]{n \to \infty} \theta$.
    \item \textbf{convergent} (ou consistent) si $\estim{} \xrightarrow[]{p.s.} \theta$
    \item \textbf{asymptotiquement convergent} si:
\[
    \frac{(\estim{} - \theta)\sqrt{n}}{\sigma} \xrightarrow[]{(loi)} \ndist{0}{1}.
\]
    \item \textbf{efficace} si $\V{\estim{}} \geq \frac{1}{n
    I(\theta)}$.
    \item et \textbf{asymptotiquement efficace} s'il atteint la borne de Cramer-Rao
    à l'infinie.
\end{enumerate}
\end{shorthm}

\begin{shorthm}
    On appelle \textbf{risque quadratique}: $\mathbb{R}_{\estim{}} = \E{(\estim{} - \theta)^2}$.
    Si l'estimateur est sans biais alors $\mathbb{R}_{\estim{}} = \V{\estim{}}$.
\end{shorthm}

\begin{shorthm}
    Un estimateur $\estim{1}$ est préférable a un autre $\estim{2}$ si
    $\mathbb{R}_{\estim{1}} \leq \mathbb{R}_{\estim{2}}$.
\end{shorthm}

\begin{shorthm}[Estimateur des moments]
    On appelle le moment d'ordre $k$ d'une variable aléatoire $X$ la valeur
    $\mu_k = \E{X^k}$. On appelle le moment empirique d'ordre $k$:
    $\nu_k = \frac{\sum X_i^k}{n}$.

    L'estimateur des moments est la solution de l'équation $\mu_k = \nu_k$.
\end{shorthm}

\begin{shorthm}[Vraisemblance]
    La vraisemblance d'une statistique est:
    \begin{itemize}
        \item $L(x_1, \dots, x_n; \theta) = \prod_{i = 1}^n f_{\theta}(x_i)$, dans
        le cas continue.
        \item $L(x_1, \dots, x_n; \theta) = \prod_{i = 1}^n \mathbb{P}(X_i = x_i)$,
        dans le cas discrète.
    \end{itemize}

    La log-vraisemblance est la fonction $l(x_1, \dots, x_n; \theta) = \log L$.
\end{shorthm}

\begin{shorthm}[L'estimateur de maximum de vraisemblance]
    On appelle l'EMV le $\estim{}$ qui maximise $L(x_1, \dots, x_n; \theta)$, ou
    bien $l(x_1, \dots, x_n; \theta)$ parce que $\log$ est une fonction croissante.
\end{shorthm}

\begin{shorthm}[Information de Fischer]
    L'information de Fisher est définie par:
\[
  (I(\theta))_{ij} = \E{\pd{l(x, \theta)}{\theta_i} \cdot
  \pd{l(x, \theta)}{\theta_j}} = -\E{\frac{\partial^2 l(x, \theta)}{\partial
  \theta_i \partial \theta_j}}
\]
\end{shorthm}

\begin{shorthm}[Borne de Cramer-Rao]
    Soit $\estim{}$ un estimateur sans biais de $\theta$, alors:
\[
    \V{\estim{}} \geq \frac{1}{n I(\theta)}.
\]
\end{shorthm}

\begin{shorthm}[Modèle exponentielle]
    Une loi est de modèle exponentielle si sa vraisemblance s'écrit:
\[
    L(x; \theta) = h(x) e^{\gamma(\theta)T(x) - \varphi(\gamma(\theta))}
\]
\end{shorthm}

\begin{shorthm}[Théorème de factorisation]
    Une statistique $S = F(X_1, \dots, X_n)$ est exhaustive $\Leftrightarrow
    L(x_1, \dots, x_n; \theta) = \psi(F, \theta) \varphi(F)$.
\end{shorthm}

\begin{shorthm}
    Une statistique exhaustive $S$ est complète si $\forall f \in L^1$:
\[
    \E{f(S)} = 0 \implies f(S) = 0 \quad \mathbb{P}_{\theta}-p.s.
\]
\end{shorthm}

\begin{shorthm}[Théorème de Lehmann-Scheffe]
    Soit $\estim{}$ un estimateur sans biais et une fonction d'une statistique exhaustive et
    complète $S$, alors $\estim{}$ est de variance minimale.
\end{shorthm}

\section{Vecteurs gaussiens}
\begin{shorthm}
    Soit $X = (X_1, \dots, X_n)$ un vecteur gaussien:
\begin{itemize}
    \item sa fonction caractéristique est $\phi_X(t) = \exp(iu.\mu - \frac{1}{2}u
    \Gamma . u)$
    \item sa fonction de répartition est $f_X(x_1, \dots, x_n) = (2\pi|\Gamma|)^{-1}$
    $\exp(-\frac{1}{2}(x - \mu)\Gamma^{-1}.(x - \mu))$
\end{itemize}
ou $\mu$ est le vecteur espérance et $\Gamma_{ij} = Cov[X_i, X_j]$ est la matrice
de covariance.
\end{shorthm}

\begin{shorthm}
    Un vecteur $X = (X_1, \dots, X_n)$ est gaussien ssi $\sum \alpha_i X_i$ est
    une v.a. gaussienne $\forall \alpha_i$ ou ssi les $X_i$ sont i.i.d.

    Si $X_i$ sont i.i.d. $\implies Cov[X_i, X_j] = 0 \implies \Gamma$ est diagonale.
\end{shorthm}

\begin{shorthm}[Corollaire du théorème de Cohran]
    Soit $X_1, \dots, X_n$ des v.a. réelles i.i.d. gaussiennes avec la moyenne
    empirique $\overline{X}_n = \frac{1}{n} \sum X_i \sim \ndist{\mu}{\frac{\sigma^2}{n}}$
    et la variance empirique normalisée $S_n^2 = \frac{1}{n - 1}
    \sum (X_i - \overline{X}_n)^2 \sim \chi^2(n - 1)$. Alors:
\[
    \frac{\sqrt{n}(\overline{X}_n - \mu)}{S_n} \sim t_{n - 1} \text{ (loi de Student)}.
\]
\end{shorthm}

\begin{shorthm}
    $\sqrt(n)(\overline{X_n} - \mu) \to \ndist{0}{\Gamma}$.
\end{shorthm}

\section{Intervalles de confiance}

\begin{shorthm}
    Pour trouver l'intervalle de confiance pour une paramètre, nous considérons son
    estimateur:
\begin{itemize}
    \item pour $\mu$: $T_n = \frac{\overline{X}_n - \mu}{\sigma} \sqrt{n} \sim
    \ndist{0}{1})$ ($\sigma$ peut être remplacé par $S_n$),
    \item pour $\sigma^2$: $T_n = \frac{\sum (X_i - \mu)}{n} \sim
    \chi^2(n)$ ($\mu$ peut être remplacé par $\overline{X}_n$).
\end{itemize}
et nous trouvons $z_{\frac{\alpha}{2}}$ telle que $\mathbb{P}(T_n \in
[-z_{\frac{\alpha}{2}}, z_{\frac{\alpha}{2}}]) = 1 - \alpha$.
\end{shorthm}

\begin{shorthm}
    Dans un modèle gaussien nous avons quatre cas:
\begin{enumerate}
    \item $\theta = \mu$ et $\sigma^2$ connue:
\[
    \mathbb{P}(\theta \in \left[\overline{X}_n - z_{\frac{\alpha}{2}} \frac{\sigma}{
    \sqrt{n}}, \overline{X}_n + z_{\frac{\alpha}{2}} \frac{\sigma}{
    \sqrt{n}}\right]) = 1 - \alpha
\]
    \item $\theta = \mu$ et $\sigma^2$ inconnue:
\[
    \mathbb{P}(\theta \in \left[\overline{X}_n - t_{n - 1, \frac{\alpha}{2}} \frac{S_n}{
    \sqrt{n}}, \overline{X}_n + t_{n - 1, \frac{\alpha}{2}} \frac{S_n}{
    \sqrt{n}}\right]) = 1 - \alpha
\]
    \item $\theta = \sigma^2$ et $\mu$ connue:
\[
    \mathbb{P}(\theta \in \left[\frac{\sum (X_i - \mu)^2}{k_2}, \frac{\sum
    (X_i - \mu)^2}{k_1}\right]) = 1 - \alpha
\]

    \item $\theta = \sigma^2$ et $\mu$ inconnue:
\[
    \mathbb{P}(\theta \in \left[\frac{\sum (X_i - \overline{X}_n)^2}{k_2}, \frac{\sum
    (X_i - \overline{X}_n)^2}{k_1}\right]) = 1 - \alpha
\]
\end{enumerate}

    Dans un modèle quelconque la probabilité tend vers $1 - \alpha$ dans le
    première deux cas.
\end{shorthm}

\begin{shorthm}
    Différence de deux moyennes $\mu_A - \mu_B$ dans le modèle gaussien:
\begin{itemize}
    \item estimateur: $T_n = \frac{\overline{X}_{n_A} - \overline{Y}_{n_B} -
   (\mu_A - \mu_B)}{\sqrt{\frac{\sigma_A^2}{n_A} + \frac{\sigma_B^2}{n_B}}}$
    \item intervalle:
{\tiny
\[
    \mathbb{P}((\mu_A - \mu_B) \in \left[ \overline{X}_{n_A} - \overline{Y}_{n_B}
    - z_{\frac{\alpha}{2}} \sqrt{\frac{\sigma_A^2}{n_A} + \frac{\sigma_B^2}{n_B}},
    \overline{X}_{n_A} - \overline{Y}_{n_B} + z_{\frac{\alpha}{2}} \sqrt{
    \frac{\sigma_A^2}{n_A} + \frac{\sigma_B^2}{n_B}}\right]) = 1 - \alpha.
\]
}
\end{itemize}
\end{shorthm}

\section{Tests d'hypothèse}

\begin{shorthm}[Test de conformité] Le test de conformité est un test destiné à
vérifier si un échantillon peut être considéré comme extrait d'une population donnée
vis-à-vis d'un paramètre.
\begin{itemize}
    \item $H_0: \mu = \mu_0$
    \item $H_1: \mu \neq \mu_0$
    \item estimateur sous $H_0$: $T = \frac{\overline{X}_n - \mu}{\sigma} \sqrt{n} \sim
    \ndist{0}{1})$ si $\sigma^2$ est connue et $T = \frac{\overline{X}_n - \mu
    }{S_n} \sqrt{n} \sim t_{n - 1}$ sinon.
    \item région critique: $W = \{ |T| > z_{\frac{\alpha}{2}}\}$ telle que
    $\mathbb{P}_{H_0}(|T| > z_{\frac{\alpha}{2}}) = \alpha$
    \item décision: si $T^{obs} \in W$ on rejette $H_0$, sinon on accepte $H_0$
\end{itemize}
\end{shorthm}

\begin{shorthm}[Test d'ajustement à une loi] Le test d'ajustement à une loi vérifie
si un échantillon peut être considéré comme extrait d'une population par rapport à la
loi observée de la population.
\begin{itemize}
    \item $H_0: p_i = p_i^0, \forall i$
    \item $H_1: \exists i, p_i \neq p_i^0$
    \item estimateur sous $H_0$: $T = n\frac{\sum (\hat{p}_i - p_i^0)^2}{p_i^0} \sim \chi^2{k - 1}$.
    $\hat{p}_i = \frac{1}{n}\sum 1_{X_k = x_i}$.
    \item région critique: $W = \{T > k_{\alpha}\}$
    \item décision: pareil.
\end{itemize}
\end{shorthm}

\begin{shorthm}[Test d'égalité] Le test d'égalité compare plusieurs populations
vis-à-vis d'un paramètre.
\begin{itemize}
    \item $H_0: \mu_A = \mu_B$
    \item $H_1: \mu_A \neq \mu_B$
    \item estimateur sous $H_0$: $T = (\overline{X}_{n_A} - \overline{Y}_{n_B})
    (\sqrt{\frac{\sigma_A^2}{n_A} + \frac{\sigma_B^2}{n_B}})^{-1} \sim \ndist{0}{1}$ si
    $\sigma^2$ est connue et $T = (\overline{X}_{n_A} - \overline{Y}_{n_B})
    (S_{XY}\sqrt{\frac{1}{n_A} + \frac{1}{n_B}})^{-1} \sim t_{n_A + n_B - 2}$ sinon.
    \item région critique: $W = \{ |T| > z_{\frac{\alpha}{2}}\}$.
    \item décision: pareil.
\end{itemize}
\end{shorthm}

\begin{shorthm}[Test d'indépendance] Le test d'indépendance vérifie l'indépendance
entre deux caractères d'une population. Soient $Y_1: \Omega \to \{b_1, \dots, b_d\}$
de loi $q_j = \mathbb{P}(Y_1 = b_j)$, $Z_1: \Omega \to \{c_1, \dots, c_m\}$
de loi $r_l = \mathbb{P}(Z_1 = c_l)$ et $X_1: \Omega \to \{b_1, \dots, b_d\} \times
\{c_1, \dots, c_m\}$ de loi $P_{jl}\mathbb{P}(Y_1 = b_j, Z_1 = c_l)$.
\begin{itemize}
    \item $H_0: P_{jl} = q_j r_l$
    \item $H_1: \exists i, j, P_{jl} \neq q_j r_l$
    \item statistique du test: $T = n \sum_j \sum_l \frac{(\hat{P}_{jl} -
    \hat{q}_j\hat{r}_l)}{\hat{q}_j\hat{r}_l} \sim \chi^2((d - 1)(m - 1))$
    \item région critique: $W = \{ T > k_{\alpha}\}$
    \item décision: pareil.
\end{itemize}
\end{shorthm}

\begin{shorthm}
    Test de niveau $1 - \alpha$ ou $100(1 - \alpha)\%$:
\begin{itemize}
    \item $\alpha = \mathbb{P}$(rejeter $H_0 | H_0$ est vraie) = $\mathbb{P}($ accepter
    $H_0 | H_0$ est fausse) s'appelle \textbf{l'erreur de premier espèce}.
    \item $\beta = \mathbb{P}($accepter $H_0 | H_0$ est fausse) $= \mathbb{P}($ rejeter
    $H_0 | H_0$ est vraie) s'appelle \textbf{l'erreur de deuxième espèce}.
    \item $1 - \beta$ est la puissance du test.
    \item p-valeur: $\mathbb{P}(|T| > T^{obs}||) =$ p-valeur. On rejette $H_0$ si la
    p-valeur est plus petite que $\alpha$, sinon on accepte $H_0$.
    \item moyenne empirique: $\overline{X}_n = \frac{1}{n}\sum X_i$
    \item variance empirique: $S_n^2 = \frac{1}{n - 1}\sum(X_i - \overline{X}_n)^2$
    \item $S_{XY} = \frac{\sum (X_i - \overline{X}_{n_A})^2 + \sum (Y_i -
    \overline{Y}_{n_B})^2}{n_A + n_B - 2}$
\end{itemize}
\end{shorthm}

\end{document}

% kate: default-dictionary fr_FR;

