% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Espaces de Hilbert}

\section{Espaces préhilbertiens}

\begin{definition}[Espace préhilbertien]
$H$ est un espace préhilbertien si $H$ est un espace vectoriel sur $\mathbb{R}$ (ou
$\mathbb{C}$) muni d'un produit scalaire $(\cdot, \cdot)$ qui vérifie:
\[
\deffunction{(\cdot, \cdot)}{H \times H}{\mathbb{R} \text{ ou } \mathbb{C}}{(u, v)}{(u, v)}
\]

Avec les propriétés suivantes:
\begin{itemize}
    \item $\forall u, v \in H, u \mapsto (u, v)$ est linéaire.
\begin{align*}
    (\lambda u, v) & = \lambda (u, v) \\
    (u_1 + u_2, v) & = (u_1, v) + (u_2, v).
\end{align*}
    \item $\forall u, v \in H, v \mapsto (u, v)$ est anti-linéaire:
\begin{align*}
    (u, \lambda v) & = \overline{\lambda}(u, v) \\
    (u, v_1 + v_2) & = (u, v_1) + (u, v_2).
\end{align*}
    \item $\forall u, v \in H, (u, v) = \overline{(v, u)}$
    ($\Leftrightarrow (u, u)$ est réel).
    \item $\forall u \in H, (u, u) \geq 0$ et $(u, u) = 0
    \Leftrightarrow u = 0$.
\end{itemize}
\end{definition}

\begin{example}
Dans $\mathbb{R}^d$, le produit scalaire est définit $\forall z, w \in \mathbb{R}^d$ par:
\[(z, w) = \sum^d z_i \overline{w}_i.\]

Dans $L^2(]a, b[)$, on a:
\[(f, g)_{L^2} = \int_a^b f(s)\overline{g(s)} \dx[s].\]

Dans $H^1_0(]a, b[)$, on a:
\[(u, v)_{H^1_0} = \int_a^b u(s)\overline{v(s)}\dx[s] +
                   \int_a^b u'(s)\overline{v'(s)} \dx[s] \]
\end{example}

\begin{proposition}
Soient $H$ un espace préhilbertien et $u \in H$. Alors, la fonction:
\begin{align*}
\|\cdot\| : H & \to \mathbb{R}^+ \\
u & \mapsto \sqrt{(u, u)}.
\end{align*}
est une norme. Cette norme est appelé norme qui découle du produit scalaire.
\end{proposition}

\begin{proposition}[L'inégalité de Cauchy-Schwartz]
    Soient $H$ un espace préhilbertien et $u, v \in H$. L'inégalité
\[
|(u, v)| \leq \sqrt{(u, u)} \cdot \sqrt{(v, v)}
\]
est toujours vraie. En utilisant la norme, on peut la réécrire comme:
\[
|(u, v)| \leq \|u\| \|v\|
\]
\end{proposition}

\begin{proposition}[La règle du parallélogramme]
    Soient $H$ un espace préhilbertien et $u, v \in H$. L'égalité
\[
\|u + v\|^2 + \|u - v\|^2 = 2(\|u\|^2 + \|v\|^2)
\]
est toujours vraie.
\end{proposition}

\begin{proof}
On développe l'égalité:
\[
\begin{aligned}
\|u + v\|^2 + \|u - v\|^2 & = (u + v, u + v) + (u - v, u - v) \\
& = (u, u) + 2(u, v) + (v, v) + (u, u) - 2(u, v) + (v, v) \\
& = 2(u, u) + 2(v, v) \\
& = 2(\|u\|^2 + \|v\|^2)
\end{aligned}
\]
\end{proof}

\begin{proposition}[La règle de la médiane]
Soient $H$ un espace préhilbertien et $u, v \in H$. L'égalité
\[
\|u\|^2 + \|v\|^2 = \frac{1}{2}(\|u + v\|^2 + \|u - v\|^2)
\]
est toujours vraie. On peut la réécrire comme:
\[
\|u\|^2 + \|v\|^2 = 2\left(\left\|\frac{u + v}{2}\right\|^2 +
                           \left\|\frac{u - v}{2}\right\|^2\right)
\]
\end{proposition}

\begin{definition}[Orthogonalité]
Soient $H$ un espace préhilbertien et $u, v \in H$. $u \bot v$ ($u$
orthogonale à $v$) si et seulement si $(u, v) = 0$.
\end{definition}

\begin{definition}[L'orthogonal d'un sous espace]
Soient $A$ une sous espace de $H$, $A \subset H$. Alors, l'orthogonale de
$A$ est:
\[
A^{\bot} = \{u \in H \, | \, \forall a \in A,\, (u, a) = 0\}.
\]
\end{definition}

\begin{example}
Soient $u, v \in L^1(]-1, 1[)$ où $u$ est une fonction paire et $v$ est
une fonction impaire. Montrer que $u \bot v$.
\[
\begin{aligned}
\|u v\|_2^2 & = \int_{-1}^1 u(s)\overline{v(s)} \dx[s] \\
& = \int_{-1}^0 u(s)\overline{v(s)} \dx[s] +
      \int_0^1 u(s)\overline{v(s)} \dx[s] \\
& = -\int_{-1}^1 u(s)\overline{v(s)} \dx[s]
\end{aligned}
\]
Donc forcement $\|u v\| = 0$.
\end{example}

\begin{example}
Soient $p, q \in \mathbb{Z}, p \neq q$. Montrer que $e^{ip\theta} \bot e^{iq\theta}$
dans $\ell^2(]-\pi, \pi[)$.
\[
\begin{aligned}
\int_{-\pi}^\pi e^{ip\theta} \overline{e^{iq\theta}}\dx[\theta] & =
    \int_{-\pi}^\pi e^{i(p - q)\theta} \dx[\theta] \\
& = \frac{1}{i(p - q)} \left[e^{i(p - q)\theta}\right]_{-\pi}^\pi \\
& = 0
\end{aligned}
\]
\end{example}

\begin{definition}
Soit $A$ une partie de $H$ ($A \subset H$). On définit l'orthogonal de $A$ par:
\[
A^{\bot} = \{u \in H \mid \forall a \in A, (u, a) = 0\}
\]
\end{definition}

\begin{theorem}
$A^{\bot}$ est un sous espace vectoriel fermé de $H$.
\end{theorem}

\begin{remark}
Soient $(u_n)_{n \in \mathbb{N}}$ une suite dans $H$, $u_n \xrightarrow{H} u$ et
$\|u_n\| \xrightarrow{\mathbb{R}_+} \|u\|$. Alors:
\begin{itemize}
    \item $\forall v \in H, \, (u_n, v) \xrightarrow{\mathbb{C}} (u, v)$.
    \item $\forall v \in H, \, (v, u_n) \xrightarrow{\mathbb{C}} (v, u)$.
\end{itemize}
\end{remark}

\begin{definition}
Soit $A$ un ensemble. $[A]$ est le plus petit espace vectoriel qui contient tous
les éléments de $A$.
\[
[A] = \left\{\sum_{i = 0}^N \lambda_i a_i \,\middle|\,
    N \text{ fini},
    a_i \in A,
    \lambda_i \in \mathbb{C}\right\}.
\]
\end{definition}

\begin{theorem}
On a toujours $\forall A \subset H$:
\[
A^{\bot} = [A]^{\bot} = \overline{[A]}^{\bot}
\].
\end{theorem}

\begin{theorem}
Soient $A, B \subset H$. Si $B \subset A$ on a $A^{\bot} \subset B^{\bot}$.
\end{theorem}

\begin{remark}
Si $E$ est un sous espace vectoriel de $H$, alors $\overline{E}$ est un sous
espace vectoriel de $H$.
\end{remark}

\section{Espaces de Hilbert}

\begin{definition}
$H$ est un espace de Hilbert si et seulement si $H$ est préhilbertien et $H$
est un Banach (complet pour la norme associée à le produit scalaire).
\end{definition}

\begin{example}
Les espaces $\mathbb{C}^d, L^2$ et $H^1_0$ sont des espaces de Hilbert.
\end{example}

\begin{theorem}[Théorème de projection]
Soient $H$ un Hilbert et $E$ un sous espace vectoriel fermé de $H$. Alors:
\begin{itemize}
    \item $\forall u \in H, \exists! u_E \in E$ telle que $\|u - u_E\| =
    \inf_{v \in E} \|u - v\|$.
    \item $u_E$ est caractérisé par:
\[
w = u_E \Leftrightarrow
\left\{
\begin{array}{l}
    u - w \in E^{\bot} \\
    w \in E
\end{array}
\right.
\]
    \item L'application:
\[
\deffunction{P_E}{H}{H}{u}{u_E}
\]
est linéaire continue.
\end{itemize}
\end{theorem}

\begin{notation}
$P_E$ est le projecteur orthogonal sur $E$.
\end{notation}

\begin{remark}
Soit $E \subset H$. Si $ E \neq \emptyset$, alors $\|P_E\| = 1$.
\end{remark}

\begin{theorem}[Théorème de Pythagore]
Soient $H$ préhilberien, $a_i \in H, i \in \llbracket 1, N \rrbracket$ avec
$a_i \bot a_j$ si $i \neq j$. Alors:
\[
\left\|\sum_{i = 1}^N a_i\right\|^2 = \sum_{i = 1}^N \|a_i\|^2.
\]
\end{theorem}

\begin{remark}
Si $E_1, E_2 \in H$ sont supplémentaire, c'est-à-dire $E_1 \cap E_2 = \emptyset$
et $H = E_1 + E_2$ (notée $H = E_1 \oplus E_2$) on sait définir un projecteur
sur $E_1$ parallèlement à $E_2$.
\end{remark}

\begin{example}
Soit $E$ un sous espace de $H$. On sait définir le projecteur sur $E$
parallèlement à $E^\bot$. $E$ et $E^\bot$ sont supplémentaire, i.e on a:
\[
H = E \oplus E^\bot
\]

Pour tout $u \in E$, on a:
\[
u = \underbrace{P_E u}_{ = u_E \in E} +
    \underbrace{(u - P_E u)}_{ = (u - u_E) \in E^\bot}
\]
\end{example}

\begin{remark}
Si $E$ est un sous espace vectoriel fermé de $H$, alors $H = E \oplus E^{\bot}$.
\end{remark}

\begin{proposition}
Si $E$ est un sous espace vectoriel fermé de $H$, alors:
\begin{itemize}
    \item $E^\bot$ est aussi un sous espace vectoriel fermé de $H$.
    \item $P_{E^\bot} = \mathrm{Id} - P_E$.
\end{itemize}
\end{proposition}

\begin{example}
Soit $H$ un espace de Hilbert. Soit $E$ un sous espace de $H$ de dimension 1
(i.e $E = \mathbb{C} f = \{\lambda f \,|\, \lambda \in \mathbb{C}\}$). Si $E$ est un sous
espace de dimension 1 dans $H$, $E$ est fermé.

Pour tout $u \in H$:
\[
u_E = P_Eu = \left(u, \frac{f}{\|f\|}\right) \frac{f}{\|f\|}.
\]
On vérifie les caractéristiques de $u_E$:
\begin{itemize}
    \item $u_E \in E$.
\[
u_E = \underbrace{\left(u, \frac{f}{\|f\|}\right)}_{ \in \mathbb{R}} \frac{f}{\|f\|} \in E
\]
    \item $u - u_E \in E^\bot$. Il existe un $v \in E$ tel que $(u - u_E, v)
    = 0$. Si $v \in E \implies v = \lambda f$. Donc:
\[
\begin{aligned}
\left(u - \left(u, \frac{f}{\|f\|}\right)\frac{f}{\|f\|}, \lambda f\right) & =
\overline{\lambda}(u, f) - \overline{\lambda}\left(u, \frac{f}{\|f\|}\right)
                                             \left(\frac{f}{\|f\|}, f\right) \\
& = \overline{\lambda}(u, f) - \frac{\overline{\lambda}}{\|f\|^2}
    (u, f) (f, f) \\
& = \overline{\lambda}(u, f) - \overline{\lambda}(u, f) \\
& = 0
\end{aligned}
\]
\end{itemize}
\end{example}

\begin{example}
Soient $H = L^2(]-1, 1[)$ et $E = \{u \in H \,|\, u(-x) = u(x)\}$ un sous
espace de $H$. Le projecteur orthogonal est défini par:
\[P_Eu = \frac{u(x) + u(-x)}{2}\]
\end{example}

\section{Espace dual}

\begin{definition}[Espace dual]
Soit $H$ un Hilbert sur $\mathbb{R}$ (ou $\mathbb{C}$). Le dual de $H$ est:
\[
  H' = \left\{
  \begin{array}{ll}
    \mathcal{L}(H, \mathbb{C}) & \quad \text{si H est un espace vectoriel sur $\mathbb{C}$ }\\
    \mathcal{L}(H, \mathbb{R}) & \quad \text{si H est un espace vectoriel sur $\mathbb{R}$ }\\
  \end{array} \right.
\]

Un élément $l$ de $H'$ est une application linéaire continue définie par:
\[
\deffunction{l}{H}{\mathbb{C}}{u}{l(u)}
\]
\end{definition}

\begin{proposition}
$H' = \mathcal{L}(H, \mathbb{C})$ est un espace vectoriel complet muni de la norme:
\[
\|f\|_{\mathcal{L}(H, \mathbb{C})} = \sup_{u \in H^*}
    \frac{\|f u\|_{H}}{\|u\|_{\mathbb{C}}}.
\]
\end{proposition}

\begin{theorem}
Si $l \in H' \implies \ker l$ est un:
\begin{itemize}
    \item sous espace vectoriel fermé,
    \item hyperplan si $l \neq 0$ (il admet une supplémentaire de dimension 1).
\end{itemize}
\end{theorem}

\begin{theorem}
Si $E$ est un hyperplan fermé de $H \implies \exists l \in H'$ telle que
$E = \ker l$.
\end{theorem}

\section{Théorème de représentation}
\begin{definition}[Isométrie]
Une application $\phi$ est une isométrie si et seulement si $\forall l \in H'$:
\[
\|\phi(l)\| = \|l\|.
\]
\end{definition}

\begin{theorem}[La théorème de représentation de Reisz]
Soient $H$ un Hilbert et $H'$ son dual (de Banach). Alors:
\begin{itemize}
    \item $\forall l \in H', \exists! a_l \in H, \forall u \in H$ telle que
$l(u) = (u, a_l)$.
    \item L'application:
\[
\deffunction{\Phi}{H'}{H}{l}{a_l}
\]
est continue et anti-linéaire.
    \item $\Phi$ est une isométrie inversible, d'inverse continue.
\end{itemize}
\end{theorem}

\begin{remark}
$\forall a \in H$, il existe $l_a : H \to \mathbb{C}$ telle que $l_a \in H'$.
\end{remark}

\begin{proof}
~\begin{itemize}[$\blacksquare$]
    \item Existence de $a_l$.

Pour toute $l \in H', \ker l$ est un hyperplan fermé donc $H = \ker l \oplus
(\ker l)^\bot$. $\ker l = \mathbb{C} e, e \in H$. Donc $\forall u \in H$:
\[
u = u_0 + \lambda(u) e
\implies
l(u) = l(u_0) + \lambda(u) l(e) = \lambda(u) l(e).
\]
Aussi, on a:
\[
(u, e) = (u_0, e) + \lambda(u)(e, e) = \lambda(u) \|e\|^2
\]
Donc $\lambda(u) = (u, \frac{e}{\|e\|^2})$ et:
\[
l(u) = \left(u, \frac{l(e)}{\|e\|^2}e\right)
\]
D'où on a que:
\[
a_l = \frac{l(e)}{\|e\|^2}e.
\]
    \item Unicité de $a_l$.

Démonstration par l'absurde: on suppose qu'il existe $a_l$ et $a_l'$ telle que
$\forall u \in H$:
\[
\begin{cases}
l(u) = (u, a_l) \\
l(u) = (u, a_l')
\end{cases}
\]
Donc $\forall u \in H$ on a $(u, a_l - a_l') = 0$. Si on prend $u = a_l - a_l'$,
on obtient $\|a_l - a_l'\| = 0 \Leftrightarrow a_l - a_l' = 0 \Leftrightarrow a_l = a_l'$.

    \item Anti-linéarité de $\Phi$.

Soit $l, l' \in H'$. Par linéarité on a:
\[
\begin{aligned}
& (l + l')(u) = l(u) + l'(u), \forall u \in H\\
\Leftrightarrow~ & (u, a_{l + l'}) = (u, a_l) + (u, a_{l'}) \\
\Leftrightarrow~ & (u, a_{l + l'}) = (u, a_l + a_{l'}) \\
\Leftrightarrow~ & a_{l + l'} = a_l + a_{l'} \\
\Leftrightarrow~ & \Phi(l + l') = \Phi(l) + \Phi(l')
\end{aligned}
\]

De même:
\[
\begin{aligned}
& l(\lambda u) = \lambda l(u), \forall u \in H \\
\Leftrightarrow~ & (u, a_{\lambda l}) = \lambda (u, a_l) \\
\Leftrightarrow~ & (u, a_{\lambda l}) = (u, \overline{\lambda}a_l) \\
\Leftrightarrow~ & a_{\lambda l} = \overline{\lambda} a_l \\
\Leftrightarrow~ & \Phi(\lambda l) = \overline{\lambda} \Phi(l)
\end{aligned}
\]

Donc $\Phi$ est une application anti-linéaire.

    \item Continuité de $\Phi$.

On a:
\[
\begin{aligned}
& \frac{l^2(a_l)}{\|a_l\|^2} \leq
  \sup_{v \in H} \frac{|l(v)|^2}{\|v\|^2} \\
\Leftrightarrow & l(a_l) \leq \frac{\|a_l\|^2}{l(a_l)}
                       \sup_{v \in H} \frac{|l(v)|^2}{\|v\|^2} \\
\end{aligned}
\]
Si on prend $c^2 = \frac{\|a_l\|^2}{l(a_l)} = 1$, on a:
\[
\begin{aligned}
&l(a_l) \leq c^2 \sup_{v \in H} \frac{|l(v)|^2}{\|v\|^2} \\
\Leftrightarrow~ & \sqrt{l(a_l)} \leq c \sup_{v \in H} \frac{|l(v)|}{\|v\|} \\
\Leftrightarrow~ & \sqrt{(a_l, a_l)} \leq c \sup_{v \in H} \frac{|l(v)|}{\|v\|} \\
\Leftrightarrow~ & \|a_l\|_H \leq c \sup_{v \in H} \frac{|l(v)|}{\|v\|} \\
\Leftrightarrow~ & \|\Phi(l)\|_H \leq c \|l\|_{H'}
\end{aligned}
\]
Donc $\Phi$ est continue.

    \item $\Phi$ est une isométrie inversible.

On veut monter que $\|\Phi\|_H = \|l\|_{H'}$. On a déjà montrer que
$\|\Phi\|_H \leq \|l\|_{H'}$ (continuité de $\Phi$). Il reste à montrer
l'autre sens.
\[
\begin{aligned}
\|l\|_{H'} & = \sup_{v \in H} \frac{|l(v)|}{\|v\|_H} \\
& = \sup_{v \in H} \frac{|(v, a_l)|}{\|v\|_H} \\
& \leq \sup_{v \in H} \frac{\|v\| \|a_l\|_H}{\|v\|_H} \\
& = \|a_l\|_H \\
& = \|\Phi\|_H
\end{aligned}
\]
Donc $\|\Phi\|_H \geq \|l\|_{H'}$. Donc forcement $\|\Phi\|_H =
\|l\|_{H'}$.

Comme $\|\Phi\|_H \geq \|l\|_{H'}$ il existe une application $\Psi$
qui est l'inverse de $\Phi$.

    \item $\Phi$ est d'inverse continue.

Soit l'application:
\[
\deffunction{\Psi}{H}{H'}{a}{l_a}
\]
telle que:
\[
\begin{aligned}
(\Phi \circ \Psi)(v) = \Phi(l_v) = v = \mathrm{Id}_H \\
(\Psi \circ \Phi)(l) = \Psi(v_l) = l = \mathrm{Id}_{H'}
\end{aligned}
\]

$\Psi$ est continue si:
\[
\|\Psi(a)\|_{H'} \leq c \|a\|_H
\]
Mais
\[
\|\Phi(l)\|_H = \|l\|_{H'} \Leftrightarrow \|a_l\|_H = \|\Psi(a)\|_{H'}
\]
Donc $\Psi$ est continue.
\end{itemize}
\end{proof}

\begin{example}
$(\!( \cdot, \cdot )\!)$ est équivalent à le produit scalaire usuel de $H^1_0$
et est défini par:
\[
(\!( u, v )\!)= \int u'(x) v'(x) \dx.
\]
\end{example}

\section{Bases hilbertiennes}
\begin{definition}
Soit $H$ un Hilbert. Une suite $(e_n)_{n \in \mathbb{N}}$ de $H$ est orthonormée si et
seulement si:
\begin{itemize}
    \item $\|e_n\| = 1$,
    \item $(e_n, e_m) = 0$ si $n \neq m$.
\end{itemize}
\end{definition}

\begin{theorem}[Théorème de Bessel]
Soient $H$ un espace de Hilbert, $(e_n)_{n \in \mathbb{N}}$ une suite orthonormée de
$H, u \in H$ et $\xi_n = (u, e_n)$ ($\xi_n$ est la composant de $u$ sur
$e_n$). Alors:
\begin{itemize}
    \item $\sum |\xi_n|^2$ est convergente et $\sum |\xi_n|^2 \leq \|u\|^2$.
    \item $\sum \xi_n e_n$ est convergente dans $H$ et $\sum \xi_n e_n$ et
    la projection orthogonale de $u$ sur $\overline{[(e_n)]}$.
\end{itemize}
\end{theorem}

\begin{proof}
~\begin{itemize}[$\blacksquare$]
    \item Montrons $\sum |\xi_n|^2$ est convergente et $\sum |\xi_n|^2 \leq
    \|u\|^2$.

Soient $u \in H$ et $N \in N$. On a:
\[u = \sum^N \xi_n e_n + \left(u - \sum^N \xi_n e_n\right).\]
Mais:
\[
\begin{aligned}
& \left(\sum^N \xi_n e_n, u - \sum^N \xi_n e_n\right) \\
= & \sum^N \xi_n (e_n, u) - \sum^N \sum^N \xi_n \overline{\xi_m} (e_n, e_m) \\
= & \sum^N \xi_n \overline{\xi_n} - \sum^N \xi_n \xi_n \\
= & 0
\end{aligned}
\]

Donc
\[
\begin{aligned}
\|u\|^2 & = \left\|\sum^N \xi_n e_n\right\|^2 + \left\|u - \sum^N \xi_n e_n\right\|^2 \\
& \geq \left\|\sum^N \xi_n e_n\right\|^2 \\
& = \sum^N \|\xi_n e_n\|^2 \\
& = \sum^N |\xi_n|^2
\end{aligned}
\]

Donc la somme partielle $\sum |\xi_n|^2 \leq \|u\|^2$ est majorée
$\implies \sum |\xi_n|^2$ converge et on a bien:
\[
\sum^\infty |\xi_n|^2 \leq \|u\|^2
\]

    \item $\sum \xi_n e_n$ converge dans $H$.

Soit $S_N = \sum^N \xi_n e_n$. On veut monter que $S_N$ est de Cauchy et
donc converge dans $H$.
\[
\|S_M - S_N\|^2 = \left\|\sum^M \xi_n e_n - \sum^N \xi_n e_n\right\|
                   = \left\|\sum_{N + 1}^M \xi_n e_n\right\|
                   = \sum_{N + 1}^M |\xi_n|^2 \leq \epsilon^2
\]
pour $N, M$ assez grand. Donc $S_N$ est de Cauchy.

Soit $v_n = \sum^\infty \xi_n e_n$. Or, $\forall i$ on a:
\[
\begin{aligned}
(e_i, u - v_n) & = (e_i, u - \sum^\infty \xi_n e_n) \\
& = (e_i, u) - (e_i, \sum^\infty \xi_n e_n) \\
& = \overline{\xi_i} - \overline{\xi_i} \\
& = 0
\end{aligned}
\]
Donc $u - v_n \in (e_i)^\bot = [(e_i)]^\bot = \overline{[(e_i)]}^\bot$. Mais
\[
v_n = \sum^\infty \xi_n e_n = \lim_N \sum^N \xi_n e_n \in \overline{[(e_i)]}
\]
Donc $v_n \in \overline{[(e_i)]}$. Donc, par la caractérisation de l'orthogonal
on a que $v_n$ est la projection orthogonale de $u$ dans $\overline{[(e_i)]}$.
\end{itemize}

\end{proof}

\begin{theorem}[Base hilbertienne]
    Soit $H$ un Hilbert. $(e_n)_{n \in \mathbb{N}}$ est une base hilbertienne de $H$ si:
\begin{itemize}
    \item $(e_n)_{n \in \mathbb{N}}$ est orthonormée,
    \item $(e_n)_{n \in \mathbb{N}}$ est totale (c'est-à-dire si $[(e_n)]$ est dense
    dans $H \Leftrightarrow \overline{[(e_n)]} = H)$.
\end{itemize}
\end{theorem}

\begin{theorem}[Théorème de Parseval]
Soient $H$ un espace de Hilbert, $(e_n)_{n \in \mathbb{N}}$ une base hilbertienne
de $H$, $u \in H$ et $\xi_n = (u, e_n)$. Alors:
\begin{itemize}
    \item $\sum |\xi_n|^2 = \|u\|^2$,
    \item $\sum \xi_n e_n$ est convergente dans $H$ et $\sum \xi_n e_n = u$.
\end{itemize}
\end{theorem}

\begin{example}[Les polynômes de Legendre]
Soit $(P_n)$ une suite des polynômes définie par:
\[
P_n(x) = \frac{n!}{(2n)!} \frac{\mathrm{d}^n}{\mathrm{d} x^n} (x^2 - 1)^n
\]
$P_n$ est un polynôme de dégrée $n$, car $(1 - x^2)^n = (1 - x)^n(1 + x)^n$,
donc:
\[
\frac{\mathrm{d}^k}{\mathrm{d} x^k} (x^2 - 1)^n|_{x = 1} = 0
\]
si $k < n$.

Aussi, on a:
\[
(P_n, P_m) =
\begin{cases}
\frac{(-1)^n(n!)^2}{(2n)!}I_n & \quad si\ n = m \\
0 & \quad \text{sinon}
\end{cases}
\]
Où $I_n$ est défini par:
\[
I_n = -\frac{2n}{2n + 1}I_{n - 1}
\]

On peut calculer $I_n$ et on trouve:
\[
I_n = \frac{(-1)^n 2^{n + 1} (n!)^2}{(2n + 1)!}
\]
Donc, si $n = m$, on a:
\[
\|P_n\|^2 = (P_n, P_n) = \frac{2^{n + 1} (n!)^4}{((2n)!)^2} \frac{2}{2n + 1}
\]

On définit le polynôme de Legendre par:
\[
L_n(x) = \frac{P_n}{\|P_n\|} = \sqrt{n + \frac{1}{2}} \frac{1}{2^n n!}
    \frac{\mathrm{d}^n}{\mathrm{d}x^n}(x^2 - 1)^n
\]
\end{example}

\begin{theorem}[Théorème de Legendre]
$(L_n)$ est une base hilbertienne de $L^2(]-1, 1[)$.
\end{theorem}

\begin{theorem}[Théorème de Fourier]
$\displaystyle \left(\frac{e^{in\theta}}{\sqrt{2\pi}}\right)$ est une base
hilbertienne de $L^2(]-\pi, \pi[)$.
\end{theorem}

% TODO: solving the laplacian on a disk

% kate: default-dictionary fr_FR;
