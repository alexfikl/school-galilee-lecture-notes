% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Espaces vectoriels normés}

\section{Introduction}

Un \textbf{espace vectoriel} est un ensemble muni d'un structure permettant
d'effectuer des combinaisons linéaires.

Étant donné un corps $K$, un espace vectoriel $E$ sur $K$ est un groupe
commutatif (dont la loi est notée $+$). Les éléments de $E$ sont appelés des
vecteurs, et les éléments de $K$ des scalaires.

Un espace vectoriel $E$ est muni de deux lois:
\begin{itemize}
    \item une loi interne: $+ : E \times E \to E$ appelée \textbf{somme}.
    \item une loi externe: $\cdot: K \times E \to E$ appelée
    \textbf{multiplication}.
\end{itemize}
Un \textbf{espace vectoriel normé} est une structure mathématique qui développe
des propriétés géométriques de distance compatibles avec les opérations de
l'algèbre linéaire.

\begin{definition}[Norme]
Soit $E$ un espace vectoriel normé. Sa norme $\|\cdot\|$ est définie
par:
\[
\deffunction{\|\cdot\|}{E}{\mathbb{R}^+}{x}{\|x\|}
\]

Une telle application satisfait les hypothèses suivantes:
\begin{itemize}
    \item $\|x\| \geq 0\, (\|x\| = 0_{\mathbb{R}} \Leftrightarrow x = 0_E)$
    \hfill (définie positive)
    \item $\|\lambda x\| = |\lambda| \|x\|$
    \hfill (multiplication par un scalaire)
    \item $\|x + y\| \leq \|x\| + \|y\|$
    \hfill (inégalité triangulaire)
\end{itemize}
\end{definition}

\begin{definition}[Distance]
La distance dans un espace vectoriel est définie par:
\[
\deffunction{d}{E \times E}{\mathbb{R}^+}{(x, y)}{\|x - y\|}
\]

Avec les propriétés:
\begin{itemize}
    \item $d(x, y) \geq 0 \text{ } (d(x, y) = 0 \Leftrightarrow x = y)$ \hfill
(d\'efinie positive)
    \item $d(x, y) = d(y, x)$ \hfill (symétrie)
    \item $d(x, y) \leq d(x, z) + d(z, y)$ \hfill (inégalité triangulaire)
\end{itemize}
\end{definition}

\begin{example}
Des exemples d'espaces vectoriels normés:
\begin{itemize}
    \item $\mathbb{R}^d$, muni par les normes équivalentes:
\[
\begin{aligned}
\|x\|_{L^\infty} & = \max_{i \in \llbracket 1, d \rrbracket} |x_i| \\
\|x\|_{L^2} & =
    \left(\sum_{i = 1}^d |x_i|^2\right)^{\sfrac{1}{2}} \\
\|x\|_{L^1} & = \sum_{i = 1}^d |x_i|
\end{aligned}
\]
    \item $L^1(]a, b[)$:
\[
\begin{aligned}
L^1(]a, b[) & = \{ u(x) \,|\, x \in ]a, b[, \int^b_a |u(x)| \dx < \infty\} \\
\|u\|_{L^1(]a, b[)} & = \int^b_a |u(x)| \dx.
\end{aligned}
\]
    \item $L^2(]a, b[)$:
\[
\begin{aligned}
L^2(]a, b[) & = \{ u(x) \,|\, x \in ]a, b[, \int^b_a |u(x)|^2 \dx < \infty\} \\
\|u\|_{L^2(]a, b[)} & = \int^b_a |u(x)|^2 \dx.
\end{aligned}
\]

    \item $L^\infty(]a, b[)$:
\[
\begin{aligned}
L^\infty(]a, b[) & = \{ u(x) \,|\, u(x) \text{ est bornée sur }]a, b[\} \\
\|u\|_{L^\infty(]a, b[)} & = \sup_{x \in ]a, b[} |u(x)|.
\end{aligned}
\]

    \item $\mathcal{C}([a, b])$:
\[
\begin{aligned}
\mathcal{C}([a, b]) & = \{ u(x) \,|\, u(x) \text{ est bornée
    et continue sur }]a, b[\} \\
\|u\|_{\mathcal{C}([a, b])} & = \sup_{x \in [a, b]} |u(x)|.
\end{aligned}
\]
    \item $H^1(]a, b[)$:
\[
\begin{aligned}
H^1(]a, b[) & = \{ u(x) \,|\, x \in ]a, b[, \int^b_a |u(x)|^2 \dx < \infty
    \text{ et } \int^b_a |u'(x)|^2 \dx < \infty \} \\
\|u\|_{H^1(]a, b[)} & = \int^b_a |u(x)|^2 \dx + \int^b_a |u'(x)|^2 \dx.
\end{aligned}
\]

    \item $H^1_0(]a, b[)$:
\[
\begin{aligned}
H^1_0(]a, b[) & = \{ u(x) \,|\, u \in H^1 \text{ et } u(a) = u(b) = 0 \} \\
\|u\|_{H^1_0(]a, b[)} & = \|u\|_{H^1(]a, b[)}.
\end{aligned}
\]
\end{itemize}
\end{example}

\begin{proposition}
Propriétés des espaces:
\begin{itemize}
    \item $H^1_0(]0, 1[) \subset L^\infty(]0, 1[)$.
    \item $H^1_0(]0, 1[) \subset C([0, 1])$.
    \item on a toujours $\|u\|_{C([0, 1])} \leq \|u\|_{H^1_0(]0, 1[)}$.
\end{itemize}
\end{proposition}

\begin{definition}[Normes équivalentes]
Soit $E$ un espace vectoriel normé. Soient $|\cdot|$ et $\|\cdot\|$ deux
normes sur $E$. On dit que ces deux normes sont équivalentes si $\exists M, m
> 0, \forall u \in E$ telles que:
\[
m |u| \leq \|u\| \leq M |u|
\]
\end{definition}

\begin{theorem}
En dimension finie, toute les normes sont équivalentes.
\end{theorem}

\begin{definition}[Boule ouverte et fermée]
Soit $E$ un espace vectoriel normé, muni de la norme $\|\cdot\|$. On définit
une boule ouvert, respectivement fermé, dans $E$ par:
\[
\begin{aligned}
\mathcal{B}(u_0, r) & = \{u \in E \,|\, \|u_0 - u\| < r\}, \\
\overline{\mathcal{B}(u_0, r)} & = \{u \in E \,|\, \|u_0 - u\| \leq r\} \\
\end{aligned}
\]
\end{definition}

\begin{definition}[Ensemble ouvert]
Soit $E$ un espace vectoriel normé, muni de la norme $\|\cdot\|$. Soit
$\Omega \subset E$. $\Omega$ est ouvert si et seulement si:
\[
\forall u_0 \in \Omega, \exists r > 0, \mathcal{B}(u_0, r) \subset \Omega.
\]
\end{definition}

% TODO: the first property(?) makes no sense
\begin{theorem}
Soit $E$ un espace vectoriel normé ouvert.
\begin{enumerate}
    \item $\emptyset$ est ouvert.
    \item Soit $\Omega_i \subset E$ une suite de sous-espaces ouverts de $E$.
    Alors $\cap \Omega_i$ est ouvert.
    \item Soit $(\Omega_i)_{i \in I} \subset E$ une suite de sous-espaces
    ouverts de $E$. Alors $\cup \Omega_i$ est ouvert.
\end{enumerate}
\end{theorem}

\begin{theorem}
Soit $E$ un espace vectoriel normé fermé.
\begin{enumerate}
    \item $\emptyset$ est fermé.
    \item Soit $F_i$ une suite de sous-espaces fermés de $E$. Alors
    $\cup F_i$ est fermé.
    \item Soit $(F_i)_{i \in I}$ une suite de sous-espaces fermés de $E$. Alors
    $\cap F_i$ est fermé.
\end{enumerate}
\end{theorem}

\begin{definition}[La fermeture d'un ensemble]
Soit $E$ un espace vectoriel normé, muni de la norme $\|\cdot\|$. Soit
$A \subset E$. La fermeture de $A$ est définie par:
\[
\overline{A} = \bigcap_{\stackrel{F\, ferme}{A \subset F}} F
\]
\end{definition}

\begin{remark}
La fermeture de $A, \overline{A}$, est le plus petit fermé contenant $A$.
\end{remark}

\begin{definition}[L'intérieur d'un ensemble]
Soit $E$ un espace vectoriel normé, muni de la norme $\|\cdot\|$. Soit
$A \subset E$. L'intérieur de $A$ est défini par:
\[
\mathring{A} = \bigcup_{\stackrel{\Omega\, ouvert}{\Omega \subset A}} \Omega
\]
\end{definition}

\section{Convergence des suites}

\begin{theorem}
Soient $E$ un espace vectoriel normé et $(u_n)$ une suite convergente de $E$.
Alors $u_n$ est bornée et $\exists c, \forall n$ telle que $\|u_n\| \leq c$.
\end{theorem}

\begin{definition}[Convergence]
Soient $E$ un espace vectoriel normé, muni de la norme $\|\cdot\|$, et
$(u_n)_{n \in \mathbb{N}}$ une suite de $E$ et $u \in E$. Alors:
\[
u_n \xrightarrow{E} u \Leftrightarrow \|u_n - u\| \xrightarrow[n \to \infty]{} 0.
\]
\end{definition}

\begin{example}
$u_n(x) = \frac{1}{\sqrt{x + \sfrac{1}{n}}}$ ne converge pas dans $L^2(]0, 1[)$.

$u_n(x)$ est continue sur $]0, 1[ \implies$ bornée $\implies \exists M$ telle
que $u_n(x) \leq M$ donc:
\[
\int_0^1 |u_n(x)|^2 \dx \leq M^2 \leq \infty \implies u_n(x) \in L^2(]0, 1[)
\]

Si $u_n(x) \xrightarrow{L^2(]0, 1[)} v(x) \in L^2(]0, 1[) \subset
L^\infty(]0, 1[)$, on a:
\[
0 \leftarrow \|u_n - v\|_{L^\infty} \leq \|u_n - v\|_{L^2} \rightarrow 0
\]
Donc $u_n \rightarrow v \implies v = \sfrac{1}{\sqrt{x}}$, mais
$\sfrac{1}{\sqrt{x}} \notin L^2(]0, 1[)$. Donc $u_n(x)$ ne converge pas dans
$L^2(]0, 1[)$.
\end{example}

\begin{proposition}
Soit $E$ un espace vectoriel normé, muni de la norme $\|\cdot\|$. Alors:
\[
u_n \xrightarrow{ E } u \Leftrightarrow \forall \epsilon > 0, \exists N, \forall n > N
\text{ telle que } u_n \in \mathcal{B}(u, \epsilon).
\]
\end{proposition}

\begin{example}
Soit l'espace vectoriel normé $L^1(]0, 1[)$, muni de la norme $\|\cdot\|_1$.
Soit $u_n$ une fonction définie par:
\[
u_n(x) = n^\alpha e^{-n x}
\]

Donc:
\begin{itemize}
    \item si $\alpha < 1, u_n$ converge vers $0$.
\[
\begin{aligned}
\|u_n - 0\|_1 & = \int_0^1 |n^\alpha e^{-n x} - 0|\dx \\
& = \int_0^1 n^\alpha e^{-nx} \dx \\
& = n^\alpha \left[-\frac{e^{-nx}}{n}\right]_0^1 \\
& = n^{\alpha - 1}(1 - e^{-n}) \xrightarrow[n \to \infty]{} 0
\end{aligned}
\]
    \item si $\alpha > 1, u_n$ ne converge pas.
    \item si $\alpha = 1, u_n$ ne converge pas.
\end{itemize}
\end{example}

\begin{theorem}
Si $u_n(x) \xrightarrow{L^1} u(x)$, alors on n'a pas $\forall x, u_n(x) \to u$.

Mais il existe une sous suite $u_{n_k}$ telle que pour presque tous les $x$,
on a:
\[
u_{n_k}(x) \xrightarrow[k \to \infty]{} u(x).
\]
\end{theorem}

\begin{example}
Soit $u_n(x) = \sqrt{n}e^{-nx} \xrightarrow{L^1} 0$, mais $u_n(0) = \sqrt{n}
\to \infty$.
\end{example}

\begin{proposition}
Soit $]a, b[$ fini, on a:
\[
\|u\|_{L^1(]a, b[)} \leq \sqrt{b - a}\|u\|_{L^2(]a, b[)}
\]
\end{proposition}

\begin{remark}
Soit $]a, b[$ borné.
\[
\left\{
\begin{array}{l}
    u_n \in L^2(]a, b[), \\
    u \in L^2(]a, b[), \\
    u_n \xrightarrow{L^2(]a, b[)} u
\end{array}
\right.
\Longrightarrow
\left\{
\begin{array}{l}
    u_n \in L^1(]a, b[), \\
    u \in L^1(]a, b[), \\
    u_n \xrightarrow{L^1(]a, b[)} u.
\end{array}
\right.
\]
\end{remark}

\begin{remark}
Soit $]a, b[$ borné.
\[
\left\{
\begin{array}{l}
    u_n \in L^{\infty}(]a, b[), \\
    u \in L^{\infty}(]a, b[), \\
    u_n \xrightarrow{L^{\infty}(]a, b[)} u
\end{array}
\right.
\Longrightarrow \left\{
\begin{array}{l}
    u_n \in L^{\text{1 ou 2}}(]a, b[), \\
    u \in L^{\text{1 ou 2}}(]a, b[), \\
    u_n \xrightarrow{L^{\text{1 ou 2}}(]a, b[)} u.
\end{array}
\right.
\]
\end{remark}

\begin{remark}
Soit $]a, b[$ borné.
\[
u_n \xrightarrow{H^1(]a, b[)} u \Longrightarrow u_n \xrightarrow{L^2(]a, b[)} u.
\]
\end{remark}

L'espace vectoriel $E$ est muni d'une base $\{e_1, \dots, e_n\}$. Soit une suite
$u_n$ où on note $u_n(i)$ la i\textsuperscript{ème} composant de $u_n$ dans
la base de $E$. Il est alors équivalent de dire que la suite $(u_n)$ converge
vers $u$ que de dire que pour tout $i$, $u_n(i)$ converge vers $u(i)$.
\[
|u_n(i) - u(i)| \leq \sum_i |u_n(i) - u(i)| = \|u_n(i) - u(i)|
\]

Donc si $\|u_n(i) - u(i)\|$ converge vers $0$, il est de même pour
$|u_n(i) - u(i)|$. Réciproquement, si chaque $|u_n(i) - u(i)|$ converge
vers $0$, il est de même pour la norme.

Si $\sdim{E} < \infty$ alors la convergence ne dépend pas de la norme. Soient
$N_1, N_2$ deux normes de dimension finie. Alors $\exists \alpha, \beta > 0$
telle que $\alpha N_1 \leq N_2 \leq \beta N_1$ et:
\[
N_1(u_n - u) \xrightarrow[n \to \infty]{} 0
\implies
N_2(u_n - u) \xrightarrow[n \to \infty]{} 0
\]

\begin{remark}
Soit $E$ un espace vectoriel normé avec deux normes équivalentes $|\cdot|$
et $\|\cdot\|{}$. Alors, on a:
\[
u_n \xrightarrow[|\cdot|]{E} u \Leftrightarrow u_n \xrightarrow[\|\cdot\|]{E} u
\]
\end{remark}

% kate: default-dictionary fr_FR;
