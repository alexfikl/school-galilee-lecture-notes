% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Calcul différentiel}

\section{Calcul différentiel en dimension finie}

Soit $f:~]a, b[ \to \mathbb{R}$ une fonction continue et continûment dérivable.

\begin{theorem}[Théorème des accroissements finis]\label{thm:acc1}
Soient $a, b \in ]\alpha, \beta[$ et $f$ une fonction continûment dérivable
sur $]\alpha, \beta[$. Il existe $c \in ]a, b[$ telle que:
\[
f(b) - f(a) = (b - a)f'(c).
\]
\end{theorem}

\begin{theorem}[Théorème des accroissements finis]\label{thm:acc2}
Soit $f$ une fonction continûment dérivable sur $]\alpha, \beta[$. Pour tout
$x \in ]\alpha, \beta[$ on a $|f'(x)| \leq M$. Alors:
\[
|f(b) - f(a)| \leq M |b - a|.
\]
\end{theorem}

\begin{example}
Soit $\Omega = ]\alpha, \beta[ \times ]\alpha', \beta'[$. Soit une fonction $f$:
\[
\deffunction{f}{\Omega \subset \mathbb{R}^2}{\mathbb{R}^2}{(x, y)}{f(x, y)}
\]

On a:
\[
\pd{f}{x}(x, y) \text{ et } \pd{f}{y}(x, y).
\]

En dimension 2, le théorème~\ref{thm:acc1} n'est plus vrai, mais le théorème
~\ref{thm:acc2} reste vrai.
\[
\left.
\begin{aligned}
\left|\pd{f}{x}(x, y)\right| \leq M \\
\left|\pd{f}{y}(x, y)\right| \leq M
\end{aligned}
\right\}
\implies \|f(b) - f(a)\| \leq |b - a| M.
\]
\end{example}

En dimension quelconque (mais finie) on définit une fonction $f$ par:
\[
\deffunction{f}{\mathbb{R}^n}{\mathbb{R}^m}{(x_1, \dots, x_n)}{(y_1, \dots, y_m)}
\]
où:
\[
\left\{
\begin{aligned}
y_1 & = & f^1(x_1, \dots, x_n) \\
& \vdots & \\
y_m & = & f^m(x_1, \dots, x_n)
\end{aligned}
\right.
\]

\begin{definition}
Pour $j \in \llbracket 1, m\rrbracket$ on dit que $f$ est continue si et seulement si
$\forall x^0 = (x_1^0, \dots, x_n^0). f$ est continue en $x^0$. Ceci est
équivalent à dire que $\forall x^0, \forall \epsilon > 0, \exists \alpha$,
on a:
\[
\|x - x^0\| < \alpha \implies |f^j(x) - f^j(x^0)| < \epsilon.
\]
\end{definition}

\begin{definition}
On dit que $f^j, \forall j \in \llbracket 1, m\rrbracket$ admet des dérivées partielles
en $x^0$ si $\forall i \in \llbracket 1, n \rrbracket$ l'application:
\[
\deffunction{f_j}{\mathbb{R}}{\mathbb{R}}{x_i}{f_j(x_1^0, \dots, x_i^0, \dots x_n^0)}
\]
est dérivable.
\end{definition}

\begin{definition}
On dit que $f^j, \forall j \in \llbracket 1, m\rrbracket$ admet des dérivées partielles si
et seulement si $f^j$ admet des dérivées partielles pour tout $x^0$.
\end{definition}

\begin{definition}
On dit que $f$ admet des dérivées partielles si et seulement si $f^j$ admet
des dérivées partielles pour tout $j$.
\end{definition}

\begin{definition}
$f$ admet des dérivées partielles continues si $\forall i \in \llbracket 1, n \rrbracket,
\forall j \in \llbracket 1, m \rrbracket$ l'application:
\[
(x_1, \dots, x_n) \longmapsto \pd{f^j}{x_i}(x_1, \dots, x_n)
\text{ est continue.}
\]
\end{definition}

\begin{theorem}
Soit $x^0 \in \Omega$. $f$ est différentiable en $x^0$ si et seulement s'il
existe une matrice $A_{x^0} \in \mathcal{M}_{m, n}$ telle que $\forall h
\in R^n, x^0 + h \in \Omega$ on a:
\[
\|f(x^0 + h) - f(x^0) - A_{x^0} h\| = \|h\| \epsilon(\|h\|)
\]
avec $\epsilon(\|h\|) \to 0$ quand $\|h\| \to 0$.
\end{theorem}

\begin{remark}
L'application:
\[
\deffunction{d_{x^0}}{\mathbb{R}^n}{\mathbb{R}^m}{h}{A_{x^0} h}
\]
est linéaire. Elle s'appelle la différentielle de $f$ en $x^0$ et on se
note $d_{x^0}f$.
\end{remark}

\begin{theorem}
Si $n = m = 1$ et $f$ est continûment dérivable sur $\Omega = ]\alpha, \beta[$,
on a:
\begin{itemize}
    \item $\forall x^0 \in \Omega, f$ est différentiable en $x^0$.
    \item $d_{x^0}f = f'$ (la matrice $A_{x^0}$ est une matrice $1 \times 1$).
\end{itemize}
\end{theorem}

\begin{proof}
$\forall h \in \mathbb{R}$ on a:
\[
\begin{aligned}
\|f(x^0 + h) - f(x^0) - (d_{x^0}f) h\| & =
    |f(x^0 + h) - f(x^0) - (d_{x^0}f)h| \\
& = |(x^0 + h) - f(x^0) - hf'(x^0)| \\
& = |hf'(x^0 + th) - hf'(x^0)| \\
& = |h| \underbrace{|f'(x^0 + th) - f'(x^0)|}_{ \,=\, \epsilon(h) \,\to\, 0}
\end{aligned}
\]
\end{proof}

\begin{theorem}
Si $f$ admet des dérivées partielles continue en tout point $x^0 \in \Omega$,
alors $f$ est différentiable en tout $x^0$ et:
\[
d_{x^0}f =
\begin{bmatrix}
\pd{f^1}{x_1}(x^0) & \cdots  & \cdots         & \cdots  & \pd{f^1}{x_n}(x^0) \\
\vdots             & \ddots  & \vdots             & \ddots  & \vdots \\
\vdots             & \cdots  & \pd{f^j}{x_i}(x^0) & \cdots  & \vdots \\
\vdots             & \ddots  & \cdots             & \ddots  & \vdots \\
\pd{f^m}{x_1}(x^0) & \cdots  & \cdots         & \cdots  & \pd{f^m}{x_n}(x^0)
\end{bmatrix}
\]
\end{theorem}

\begin{definition}
Soit $f$ différentiable en $x^0$. Soit l'application:
\[
\deffunction{J}{\mathbb{R}^n}{\mathbb{R}^m}{x^0}{d_{x^0} f}
\]

$f$ est continûment différentiable si $J$ est continue.
\end{definition}

\begin{theorem}[Théorème des accroissements finis]\label{thm:acc3}
Soit $f: \Omega \in \mathbb{R}^n \to \mathbb{R}^m$. On suppose que $\Omega$ est convexe et que
$f$ est continûment différentiable, c'est-à-dire:
\[
\sup_{x^0 \Omega} \|d_{x^0} f\|_{\mathcal{L}(\mathbb{R}^n, \mathbb{R}^m)} = M < \infty.
\]
Alors on a $\forall a, b \in \Omega$:
\[
\|f(b) - f(a)\|_{\mathbb{R}^m} \leq M \|b - a\|_{\mathbb{R}^n}
\]
\end{theorem}

\section{Calcul différentiel en dimension infinie}

On veux généraliser le calcul différentiel sur les fonctions $f: \mathbb{R}^n \to \mathbb{R}^m$
au cas ou $\mathbb{R}^n$ et $\mathbb{R}^m$ sont remplacés par des espaces vectoriels
quelconques (infinis).

La formule de Taylor a comme but de approcher une fonction régulière par des
fonctions simples (polynômes d'ordre 1 où fonctions affines d'ordre 1) au
voisinage d'un point.
\[
f(x) = f(x_0) + f'(x)(x - x_0) + (x - x_0)\epsilon_{x_0}(x)
\]
avec $\epsilon_{x_0}(x) \xrightarrow[x \to x_0]{} 0$.

Pour ce type d'approximation on a besoin d'une structure vectorielle et d'une
norme, donc d'un espace vectoriel normé.

\subsection{Différentielle}
Soit:
\begin{itemize}
    \item $E$ un espace vectoriel normé, muni de la norme $\|\cdot\|_{E}$.
    \item $F$ un espace vectoriel normé, muni de la norme $\|\cdot\|_{F}$.
    \item $\mathcal{B}(x_0, r) = \{x \in E, \|x - x_0\| < r\}$ une
    boule ouverte.
    \item $f: U \subset E \to F$ une fonction.
\end{itemize}

\begin{definition}
$f$ est différentiable en $x_0 \in U$ s'il existe une application linéaire
continue $L_{x_0}$ et une application $\epsilon_{x_0}$ telles que:
\[
f(x_0 + h) = f(x_0) + L_{x_0}(h) + \|h\| \epsilon_{x_0}(h)
\]
pour tout $h \in E$ telle que $x_0 + h \in U$ avec:
\[
\lim_{\|h\|_E \to 0} \|\epsilon_{x_0}(h)\|_F = 0
\]
\end{definition}

De manière équivalente, $\exists L_{x_0}$ linéaire continue telle que:
\[
\lim_{h \to 0} \frac{\|f(x_0 + h) - f(x_0) - L_{x_0}(h)\|_{F}}{\|h\|_{E}} = 0
\]

\begin{remark}
~\begin{itemize}
    \item Comme $U$ est ouvert, $\mathcal{B}(x_0, r) \subset U,
    \|h\|_{E} < r \implies x_0 + h \in \mathcal{B}(x_0, r) \subset U$.
    \item $f$ différentiable en $x_0 \implies f$ continue en $x_0$.
    \item En dimension infinie, la notion de différentiabilité est liée
    au choix de la norme.
\end{itemize}
\end{remark}

\begin{lemma}
Soit $f$ différentiable en $x_0$. Alors, $L_{x_0}$ est unique.
\end{lemma}

\begin{proof}
Supposons qu'il y ait deux applications linéaires $L$ et $M$. Alors:
\[
\begin{cases}
f(x_0 + h) = f(x_0) + M(h) + \|h\|_E \epsilon_1(h) \\
f(x_0 + h) = f(x_0) + L(h) + \|h\|_E \epsilon_2(h)
\end{cases}
\]
D'où $(M - L)(h) = \|h\|_E (\epsilon_1 - \epsilon_2)(h)$. On choisit $h = tv$
avec $v \in E, t \in \mathbb{R}$ petit. Alors:
\[
t(M - L)(v) = t \|v\|_E (\epsilon_1 - \epsilon_2)(tv)
\]
On simplifie et on fait tendre $t \to 0$:
\[
(M - L)(x) = 0 \implies M = L
\]
\end{proof}

\begin{definition}
On appelle $L_{x_0}$ la différentielle de $f$ en $x_0$ et on la note:
\[
\deffunction{D f}{E}{F}{h}{D f(x_0) h}
\]
Autres notations: $D_{x_0}f, d_{x_0}f, df(x_0), f'(x_0)$.
\end{definition}

\begin{example}
Soient $E = \mathbb{R}, F = \mathbb{R}$.
\[
f(x_0 + h) = f(x_0) + f'(x_0)h + o(h).
\]

Où $f'(x_0)h = (Df(x_0))(h)$.
\end{example}

\begin{example}
Soient $E = \mathbb{R}^n, F = \mathbb{R}^m$.
\[
(Df(x_0))(h) =
\begin{pmatrix}
\pd{f_1}{x_1}     & \pd{f_1}{x_2} & \cdots & \pd{f_1}{x_n} \\[1em]
\pd{f_2}{x_1}     & \pd{f_1}{x_2} & \cdots & \pd{f_2}{x_n} \\[1em]
\vdots            & \vdots        & \ddots & \vdots  \\[1em]
\pd{f_m}{x_1}     & \pd{f_p}{x_2} & \cdots & \pd{f_m}{x_n}
\end{pmatrix}
\]
\end{example}

\begin{definition}
$f$ est différentiable sur $U$ si $f$ est différentiable pour tout $x_0 \in U$.
On dit que $f$ est de classe $\mathcal{C}^1(\Omega)$ si:
\[
\deffunction{D_f}{U}{\mathcal{L}(E, F)}{x_0}{D f(x_0)}
\]
est continue en $x_0$. $\mathcal{L}(E, F)$ est un espace vectoriel muni de
la norme:
\[
\|Df(x_0)\|_{\mathcal{L}(E, F)} = \sup_{\|h\|_E = 1} \|Df(x_0)(h)\|_{F}
\]
\end{definition}

\begin{definition}
On dit que $f:U \to E$ est dérivable en $x_0$ dans la direction $v \in E
\setminus \{0\}$, s'il existe un vecteur $V_{x_0, v} \in F$ telle que:
\[
f(x_0 + tv) = f(x_0) + t V_{x_0, v} + t\epsilon(t)
\]
pour $t \in \mathbb{R}$ tel que $x_0 + tv \in U$.
\end{definition}

\begin{remark}
$V_{x_0, v}$ est unique.
\end{remark}

Si $f$ est différentiable en $x_0$, alors $f$ est dérivable en $x_0$ en toute
direction $v$ et $(Df(x_0))(v) = V_{x_0, v}$. La réciproque est fausse
(même en dimension finie).

\subsection{Différentielle partielle}

Soit $f: U \times V \to G$, où $U \times V \subset E \times F$.

Applications partielles:
\[
\deffunction{f_{x_0}}{V}{G}{y}{f(x_0, y)}
\qquad
\text{ et }
\qquad
\deffunction{f_{y_0}}{U}{G}{x}{f(x, y_0)}
\]

$f$ admet des dérivées partielles en $(x_0, y_0) \in U \times V$ si
$f_{x_0}$ et $f_{y_0}$ sont différentiable en $x_0$ et $y_0$, i.e.:
\begin{align*}
    f(x_0, y_0 + k) & = f(x_0, y_0) + (D_yf(x_0, y_0))(k) + o(k) \\
    f(x_0 + h, y_0) & = f(x_0, y_0) + (D_xf(x_0, y_0))(h) + o(h)
\end{align*}

Si $f$ est différentiable en $(x_0, y_0)$, alors $f$ admet des dérivées
partielles en $(x_0, y_0)$ et:
\begin{align*}
    (D_yf(x_0, y_0))(k) = (D_yf(x_0, y_0))(0, k) \\
    (D_xf(x_0, y_0))(h) = (D_yf(x_0, y_0))(h, 0)
\end{align*}
La réciproque est fausse.

\subsubsection{Composition}

Soient $f: \mathbb{R} \to \mathbb{R}$ et $g: \mathbb{R} \to \mathbb{R}$, alors:
\[
(g \circ f)'(x) = (g' \circ f)(x) f'(x).
\]

\begin{theorem}
Soient $f: U \to F$ et $g: V \to G$, avec $U \in E, V \in F, f(U) \in V$,
différentiables en $x_0 \in U$, respectivement en $y_0 = f(x_0) \in V$, alors
$g \circ f: U \to G$ est différentiable et:
\[
D(g \circ f)(x_0) = Dg(y_0) \circ Df(x_0).
\]
\end{theorem}

\subsubsection{Inégalité des accroissements finis}

Soit $f: U \to E$, avec $U \subset E$ convexe et ouvert. Soient $a, b \in U$
et $[a, b] = \{x = (1 - t)a + tb: t \in [0, 1]\}$. Alors, on a:
\[
|f(b) - f(a)| \leq \sup_{[a, b]} |f'| |b - a|.
\]

\begin{theorem}
Soit $f: U \to E$ différentiable sur $U$, avec $U \subset E$ convexe et ouvert.
Alors,
\[
\|f(b) - f(a)\|_F \leq \sup_{x \in [a, b]} \|Df(x)\|_{\mathcal{L}(E, F)}
                                              \|b - a\|_{E}
\]
\end{theorem}

\section{Extrema}

Soit $f: I \to \mathbb{R}$ une fonction de classe $\mathcal{C}^1(\Omega)$ avec $I$ un intervalle
de $\mathbb{R}$.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.6\linewidth]{./img/extrema.jpg}
    \caption{Maximum local}
\end{figure}

Si $f$ admet une maximum local en $t_0$ alors $f'(t_0) = 0$. On dit que
$t_0$ est un point critique de $f$. \emph{La réciproque est fausse}.

Par contre, si $f$ admet un maximum local en $t_0$ alors $f'(t_0) = 0$ et
$f''(t_0) \leq 0$. En effet, on a:
\[
f(t_0) \geq f(t_0 + h) = f(t_0) + f'(t_0)h + \frac{1}{2} f''(t_0)h^2
\]
si $h$ est assez petit. Donc:
\[
\frac{1}{2}f''(t_0)h^2 + h^2 \epsilon(h) \leq 0
\]
En faisant $h$ tendre vers $0$ on obtient que $f''(t_0) \leq 0$.

\begin{remark}
On obtient de la même manière que $t_0$ est un point critique si $f$ admet
un maximum local en $t_0$.
\end{remark}

\begin{proof}
On a:
\[
f(t_0) \geq f(t_0 + h) = f(t_0) + f'(t_0)h + h \epsilon(h)
\implies
hf'(t_0) + h\epsilon(h) \leq 0.
\]
Si on divise par $h$ avec $h$ positif et négatif, on obtient:
\[
\begin{cases}
f'(t_0) + \epsilon(h) \leq 0 \\
f'(t_0) + \epsilon(h) \geq 0
\end{cases}
\]
En faisant $h$ tendre vers $0$ on a $f'(t_0) \leq 0$ et $f'(t_0) \geq 0$. Donc
forcement $f'(t_0) = 0$.
\end{proof}

\begin{remark}
Si $f''(t_0) \leq 0$ et $f'(t_0) = 0$ alors $f$ admet un maximum local en $t_0$.
\end{remark}

\subsection{Différentielles secondes}

\begin{definition}
Soit $f:U \to F$ une fonction différentiable sur $U$, où $U$ est un ouvert de
$E$. On dit que $f$ est deux fois différentiable en $x_0$ si l'application:
\[
\deffunction{D_f}{U}{\mathcal{L}(E, F)}{x_0}{D f(x_0)}
\]
est différentiable en $x_0$, i.e.:
\[
Df(x_0 + h) = Df(x_0) + D'(Df)(x_0)(h) + o(\|h\|_E)
\]
\end{definition}

\begin{definition}
$f$ est deux fois différentiable sur $U$ si et seulement si $f$ est deux
fois différentiable en tout point $x_0 \in U$.
\end{definition}

\begin{definition}
$f$ est de classe $\mathcal{C}^2$ sur $U$ si et seulement si $f$ est deux fois
différentiable sur $U$ et:
\[
\deffunction{D^2_f}{U}{\mathcal{B}(E, F)}{x_0}{D^2 f(x_0)}
\]
est continue sur $U$. $\mathcal{B}(E, F)$ est l'espace des applications
bilinéaires continues.
\end{definition}

\begin{proposition}
Soit $f$ de classe $\mathcal{C}^2$. Alors:
\[
f(x_0 + h) = f(x_0) + Df(x_0)(h) + \frac{1}{2}D^2f(x_0)(h, h) +
             \|h\|^2 \epsilon(h)
\]
où $\|\epsilon(h)\|_F \to 0$ quand $\|h\|_E \to 0$.
\end{proposition}

\begin{theorem}[Théorème de Schwarz]
Si $f: U \to F$ est de classe $\mathcal{C}^2$ alors $D^2f(x)$ est une forme
bilinéaire symétrique:
\[
D^2f(x)(h, k) = D^2f(x)(k, h).
\]
\end{theorem}

\subsection{Extrema}

On considère des applications à valeurs réelles $f: U \to \mathbb{R}$.

\begin{definition}
On dit que $f$ admet un maximum local (respectivement un minimum local) en
$x_0 \in U$ s'il existe une boule $\mathcal{B}(x_0, r) \subset U$ telle que:
\[
\begin{aligned}
& f(x) \leq f(x_0), \forall x \in \mathcal{B}(x_0, r) \\
\text{(respectivement)} \qquad & f(x) \geq f(x_0), \forall x \in \mathcal{B}(x_0, r)
\end{aligned}
\]
\end{definition}

\begin{theorem}
Si $f: U \to \mathbb{R}$ différentiable sur $U$ admet un extremum local en $x_0$
alors $x_0$ est un point critique de $f$, i.e. $Df(x_0) = 0$ (donc
$Df(x_0)(v) = 0, \forall v \in E$).

Si de plus $f$ est de classe $\mathcal{C}^2$ sur $U$ et si $f$ admet un maximum
(respectivement un minimum) local en $x_0 \in U$, alors: $Df(x_0) = 0$ et
$D^2f(x_0)(h, h) \leq 0, \forall h \in E$ (respectivement $D^2f(x_0)(h, h)
\geq 0$).
\end{theorem}

\begin{proposition}
Soient $f: U \to \mathbb{R}$ avec $Df(x_0) = 0$ et $D^2f(x_0)$ une forme quadratique
définie négative (i.e. $D^2f(x_0)(h, h) < 0, \forall h \in E^*$) alors
$f$ admet un maximum local.
\end{proposition}

\begin{example}
Soit $D$ le disque unité sur $\mathbb{R}^2$ et $E = H^2(D)$. On définit:
\[
J(u) = \iint_D |\nabla u|^2 \dx[x] \dx[y]
     = \iint_D (\partial_x u)^2 + (\partial_y u)^2 \dx[x] \dx[y] \geq 0
\]
qui représente une énergie.

On essaye de minimiser $J$ sur $H^2(D)$. Donc, on va calculer la différentielle
de $J$ et on va trouver le(s) point(s) critique(s).
\[
\begin{aligned}
J(u + h) & = \iint_D |\nabla u + \nabla h|^2 \\
& = \iint_D |\nabla u|^2 + |\nabla h|^2 + 2 \nabla u \cdot \nabla h \\
& = J(u) + J(h) + 2\iint_D \nabla u \cdot \nabla h
\end{aligned}
\]
Donc:
\[
DJ(u)(h) = 2 \iint_D \nabla u \cdot \nabla h
\]
est bien une application linéaire continue. On a aussi:
\[
D^2J(u) = J(h) \geq 0
\]
Donc $J$ admet un minimum local en $u_0$ et $DJ(u_0)(h) = 0, \forall h \in E$.
Donc $u_0 = 0$.
\end{example}

% kate: default-dictionary fr_FR;
