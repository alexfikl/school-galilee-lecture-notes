% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Applications continues}

\section{Applications continues}

Soit:
\begin{itemize}
    \item $E$ un espace vectoriel normé, muni par $\|\cdot\|$.
    \item $F$ un espace vectoriel normé, muni par $|\cdot|$.
    \item $J: E \to F$ une application continue.
    \item $u_0 \in E$.
\end{itemize}

\begin{definition}[Continuité en $u_0$]
    $J$ est continue en $u_0$ si et seulement si:
\[
\begin{aligned}
& \forall u_n \xrightarrow[\|\cdot\|]{E} u_0 \implies
      J(u_n) \xrightarrow[|\cdot|]{F} J(u_0). \\
& \text{ ou } \\
& \forall \epsilon > 0, \exists \alpha > 0 \text{ telle que }
    \|u - u_0\| < \alpha \implies |J(u) - J(u_0)| < \epsilon.
\end{aligned}
\]
\end{definition}

\begin{definition}[Continuité]
$J$ est continue si et seulement si $J$ est continue en $u_0$, pour tout $u_0$
dans $E$.
\end{definition}

\begin{theorem}[Théorème de convergence dominée]
Soit $f \in L^1(]a, b[)$. On a:
\[
\lim_{\epsilon \to 0} \int_a^{b - \epsilon} f(t)\dx[t] = \int_a^b f(t)\dx[t]
\]
\end{theorem}

\begin{proposition}
Soit $\phi: \mathbb{R} \to \mathbb{R}$ une fonction dérivable et bornée (i.e. $\exists c > 0,
\forall x, |\phi(x)| \leq C$). Alors l'application:
\[
\deffunction{J}{H^1_0(]0, 1[}{L^\infty(]0, 1[)}{u(x)}{\phi(u(x))}
\]
est continue.
\end{proposition}

\section{Applications linéaires continues}

Soit:
\begin{itemize}
    \item $E$ un espace vectoriel normé, muni par $\|\cdot\|$.
    \item $F$ un espace vectoriel normé, muni par $|\cdot|$.
    \item $A: E \to F$ une application linéaire continue.
\end{itemize}

\begin{definition}[Linéarité]
$A$ est linéaire si et seulement si $\forall \lambda \in \mathbb{K}$ et
$\forall u, v \in E$ on a:
\begin{itemize}
    \item $A(\lambda u) = \lambda A(u)$ et
    \item $A(u + v) = A(u) + A(v)$;
    \item ou bien, si $\forall a, b \in \mathbb{K},\ A(a u + b v) =
    aA(u) + bA(v).$
\end{itemize}
\end{definition}

\begin{notation}
Une application linéaire $A$ est notée par $Au$.
\end{notation}

\begin{theorem}
\label{cont:equiv}
    Il est équivalent à dire:
\begin{enumerate}
    \item $A$ est continue dans $E$.
    \item $A$ est continue en $0_E$.
    \item $\exists c > 0, \forall u \in E \text{ telle que } |Au|_{E}
    \leq c \|u\|_{F}$
\end{enumerate}
\end{theorem}

\begin{proof}
    Théorème~\eqref{cont:equiv}
\begin{itemize}
    \item $1 \implies 2$

$E$ est un espace vectoriel donc $0_E \in E \implies$ A est continue en $0_E$.

    \item $2 \implies 3$

La continuité en $0$ implique:
\[
\forall \epsilon > 0, \exists \alpha \text{ telle que }
        \|u\| < \alpha \implies |Au| < \epsilon
\]

Pour tout $u \in E$, on prend:
\[
\begin{aligned}
\left|A\left(\frac{\alpha}{2\|u\|} u\right)\right| < \epsilon & \implies
    \frac{\alpha}{2\|u\|} |Au| < \epsilon \\
& \implies |Au| < \frac{2 \epsilon}{\alpha} \|u\| \\
& \implies |Au| \leq c \|u\|
\end{aligned}
\]
où $c = \sfrac{2\epsilon}{\alpha}$. On a pris:
\[
u = \frac{\alpha}{2\|u\|} u
\]
parce que $\|u\| = \sfrac{\alpha}{2} < \alpha$.

    \item $3 \implies 1$

Soient $u, u_0 \in E$. $\forall \epsilon > 0, \exists \alpha\, (=
\frac{\epsilon}{c})$ telle que $\|u - u_0\| < \alpha \implies |Au - Au_0|
< \epsilon$.

Par linéarité et continuité en $0_E$, on a:
\[
|A(u - u_0)| < c \|u - u_0\| < c\alpha = c \frac{\epsilon}{c} = \epsilon
\]

Donc $A$ est continue sur $E$.
\end{itemize}
\end{proof}

\begin{example}
Soit l'équation différentielle suivante:
\[
\begin{cases}
-u''(x) = f \in L^2(]0, 1[) \\
u(0) = u(1) = 0.
\end{cases}
\]

\begin{enumerate}
    \item Montrons que la solution est unique dans $H^1_0(]0, 1[)$.

Preuve par l'absurde. On suppose:
\begin{itemize}
    \item[] $u \in H^1_0$ est solution $\implies -u''(x) = f(x)$ et
    $u(0) = u(1) = 0$.
    \item[] $v \in H^1_0$ est solution $\implies -v''(x) = f(x)$ et
    $v(0) = v(1) = 0$.
\end{itemize}
Donc:
\[
u'' - v'' = 0
\]

On multiplie avec $(u(x) - v(x))$ et on intègre:
\[
\int_0^1 (u - v)(u'' - v'') \dx = 0
\]
Par intégration par partie, on a:
\[
-\int_0^1 (u' - v')^2\dx + \underbrace{
    \left[(u - v)(u' - v')\right]_0^1}_{ = 0} \dx = 0
    \]
Donc:
\[
\|u'(x) - v'(x)\| = 0
\]
D'où $u'(x) = v'(x) \implies u(x) = v(x) + c$ et avec les conditions aux limites
on trouve que $u(x) = v(x)$.

    \item La solution est donnée par:
\[
u(x) = (1 - x)\int_0^x sf(s) \dx[s] + x \int_x^1 (1 -s)f(s) \dx[s]
\]

En dérivant, on a:
\[
u'(x) = -\int_0^x sf(s) \dx[s] + (1 - x)xf(x) +
         \int_x^1 (1 - s)f(s) \dx[s] - (1 - x)xf(x)
\]
Et la dérivée deuxième:
\[
\begin{aligned}
u''(x) & = -xf(x) - (1 - x)f(x) \\
& = -f(x)
\end{aligned}
\]
Donc $-u''(x) = f(x)$.

    \item La solution est donnée par:
\[
u(x) = \int_0^1 k(x, s) u(s) \dx[s]
\]
où:
\[
k(x, s) =
\left\{
\begin{aligned}
s(1 - x) & \quad 0 \leq x \leq s \leq 1 \\
x(1 - s) & \quad 0 \leq s \leq x \leq 1
\end{aligned}
\right.
\]

On a:
\[
\begin{aligned}
u(x) & = (1 - x)\int_0^x sf(s) \dx[s] + x \int_x^1 (1 -s)f(s) \dx[s] \\
& = \underbrace{\int_0^x (1 - x)sf(s) \dx[s]}_{0 \leq x \leq s} +
    \underbrace{\int_x^1 x(1 - s)f(s) \dx[s]}_{s \leq x \leq 1} \\
& = \int_0^x k(x, s)f(s) \dx[s] + \int_x^1 k(x, s)f(s) \dx[s] \\
& = \int_0^1 k(x, s)f(s) \dx[s]
\end{aligned}
\]

    \item $k(x, s)$ est continue.
    \item L'application:
\[
\deffunction{J}{L^2(]0, 1[)}{L^2(]0, 1[)}{f(x)}{u(x)}
\]
est continue.

    \item L'application:
\[
\deffunction{J}{L^2(]0, 1[)}{H^1_0(]0, 1[)}{f(x)}{u(x)}
\]
est continue.
\end{enumerate}
\end{example}


\section{Norme des applications linéaires continues}

\begin{definition}
$\mathcal{L}(E, F)$ est l'ensemble des applications linéaires continues de $E$
dans $F$.
\end{definition}

\begin{definition}[Norme des application linéaires continues]
Soit $A: E \to F$ une application linéaire continue de $E$ dans $F$. On dit que
la norme de $A$ dans l'espace $\mathcal{L}(E, F)$ est:
\[
\|A\|_{\mathcal{L}(E, F)} = \sup_{u \in E^*} \frac{|Au|_F}{\|u\|_{E}}.
\]
\end{definition}

\begin{theorem}
Propriétés de $\mathcal{L}(E, F)$:
\begin{itemize}
    \item $\mathcal{L}(E, F)$ est un espace vectoriel.
    \item $\|\cdot\|_{\mathcal{L}(E, F)}$ est une norme sur
    $\mathcal{L}(E, F)$.
\end{itemize}
\end{theorem}

\begin{proof}
Pour montrer que $\|\cdot\|_{\mathcal{L}(E, F)}$ est une norme sur
$\mathcal{L}(E, F)$, on montre que:
\[
\sup_{u \in E^*} \frac{|Au|}{\|u\|} < \infty
\]

Si $A \in \mathcal{L}(E, F), A$ est continue donc:
\[
\exists c > 0, \forall u \in E,\, |Au| \leq c \|u\|
\]
Donc:
\[
\frac{|Au|}{\|u\|} \leq c \implies \sup_{u \in E^*}
    \frac{|Au|}{\|u\|} < \infty
\]

On montre aussi les propriétés d'une norme:
\begin{itemize}
    \item $\|A\|_{\mathcal{L}(E, F)} = 0 \Leftrightarrow A = 0$.

Si $A = 0$:
\[
\begin{aligned}
\|0\|_{\mathcal{L}(E, F)} & = \sup_{u \neq 0} \frac{|0u|}{\|u\|} \\
& = \sup_{u \neq 0} 0\\
& = 0
\end{aligned}
\]

Si $\|A\|_{\mathcal{L}(E, F)} = 0, \forall u \in E$ on a:
\[
\sup_{u \neq 0} \frac{|Au|}{\|u\|} = 0
\implies
\frac{|Au|}{\|u\|} = 0
\implies
Au = 0.
\]

Donc $A = 0$.

    \item $\|\lambda A\|_{\mathcal{L}(E, F)} =
                 |\lambda| \|A\|_{\mathcal{L}(E, F)}$.

On a:
\[
\begin{aligned}
\|\lambda A\|_{\mathcal{L}(E, F)} & =
    \sup_{u \in E^*} \frac{|\lambda Au|}{\|u\|} \\
& = |\lambda| \sup_{u \in E^*} \frac{|Au|}{\|u\|}\\
& = |\lambda| \|A\|_{\mathcal{L}(E, F)}
\end{aligned}
\]

    \item $\|A + B\|_{\mathcal{L}(E, F)} \leq
            \|A\|_{\mathcal{L}(E, F)} + \|B\|_{\mathcal{L}(E, F)}$.

Pour tout $u \in E$, on a:
\[
\begin{aligned}
\frac{|Au + Bu|}{\|u\|} & \leq \frac{|Au|}{\|u\|} +
                                    \frac{|Bu|}{\|u\|} \\
& \leq \sup_{u \neq 0} \frac{|Au|}{\|u\|} +
       \sup_{u \neq 0} \frac{|Bu|}{\|u\|}
\end{aligned}
\]
Donc:
\[
\sup_{u \neq 0} \frac{|Au + Bu|}{\|u\|} \leq
    \sup_{u \neq 0} \frac{|Au|}{\|u\|} +
    \sup_{u \neq 0} \frac{|Bu|}{\|u\|}
\]

Qui est équivalent à:
\[
\|A + B\|_{\mathcal{L}(E, F)} \leq \|A\|_{\mathcal{L}(E, F)} +
                                   \|B\|_{\mathcal{L}(E, F)}
\]
\end{itemize}

Donc $\|A\|_{\mathcal{L}(E, F)}$ est bien une norme sur $\mathcal{L}(E, F)$.
\end{proof}

\begin{definition}
On appelle opérateur à noyau intégral, l'application:
\[
K: u(x) \longmapsto (Ku)(x) = \int_a^b k(x, s) u(s) \dx[s]
\]
\end{definition}

\begin{notation}
$k(x, s)$ est appelé le noyau intégral de $K$.
\end{notation}

\begin{remark}
On considère l'équation différentielle précédente:
\[
-\Delta_D u = f
\]
avec les conditions aux limites de Dirichlet. Elle a comme solution:
\[
u(x) = \int^1_0 k_D(x, s) f(s) \dx[s] = (K_Df)(s)
\]
Donc on a:
\[
u = (K_Df) \Leftrightarrow K_D = (-\Delta_D)^{-1}.
\]
\end{remark}

\begin{notation}
On note $\mathcal{L}(E, E) = \mathcal{L}(E)$.
\end{notation}

\begin{theorem}
On suppose:
\[
\|k(x, s)\|_{L^2(]a, b[^2)} =
    \iint_{]a, b[^2} |k(x, s)| \dx\dx[s] \leq \infty.
\]

Alors:
\begin{enumerate}
    \item $K$ est une application linéaire continue de $L^2(]a, b[)$ dans
    $L^2(]a, b[)$.
    \item $\|K\|_{\mathcal{L}(L^2(]a, b[))} \leq \|k\|_{L^2(]a, b[^2)}$.
\end{enumerate}
\end{theorem}

\begin{proof}
~\begin{itemize}[$\blacksquare$]
    \item $K$ est une application linéaire continue de $L^2(]a, b[)$ dans
    $L^2(]a, b[)$.

La linéarité est évidente, donc on montre que la continuité:
\[
\exists c > 0, \|K\|_{\mathcal{L}(L^2(]a, b[))} \leq
               c \|u\|_{L^2(]a, b[)}
\]
On a:
\[
\begin{aligned}
\|K\|_{\mathcal{L}(L^2(]a, b[))} & =
    \int_a^b |(Ku)(x)|^2 \dx \\
& = \int_a^b \left|\int_a^b k(x, s) u(s) \dx[s] \right| \dx \\
& \leq \int_a^b \left(\sqrt{\int_a^b |k(x, s)|^2 \dx[s]}\right)
                  \left(\sqrt{\int_a^b |u(s)|^2 \dx[s]}\right) \dx \\
& = \sqrt{\int_a^b |u(s)|^2 \dx[s]}
      \left(\int_a^b \sqrt{\int_a^b |k(x, s)|^2 \dx[s]} \dx \right) \\
& = c \|u\|_{L^2(]a, b[)}
\end{aligned}
\]

Donc $K$ est une application linéaire continue.

    \item $\|K\|_{\mathcal{L}(L^2(]a, b[))} \leq \|k\|_{L^2(]a, b[^2)}$.
\[
\begin{aligned}
\|K\|_{\mathcal{L}(L^2(]a, b[))} & =
    \sup_{u \neq 0}
    \frac{\|Ku\|_{L^2}}{\|u\|_{L^2}} \\
& \leq \sup_{u \neq 0} \frac{1}{\|u\|_{L^2}}
    \left(\sqrt{\iint_{]a, b[^2} |k(x, s)|^2 \dx \dx[s]} \|u\|_{L^2}\right) \\
& = \sqrt{\iint_{]a, b[^2} |k(x, s)|^2 \dx \dx[s]} \\
& = \|k\|_{L^2(]a, b[^2)}
\end{aligned}
\]
\end{itemize}
\end{proof}

% kate: default-dictionary fr_FR;
