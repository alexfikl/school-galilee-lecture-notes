% SPDX-FileCopyrightText: 2012-2024 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: CC0-1.0

\chapter{Espaces de Banach}

Soit $E$ un espace vectoriel normé, muni par la norme $\|\cdot\|$.

\begin{definition}[Suite de Cauchy]
$(u_n)_{n \in \mathbb{N}}$ est une suite de Cauchy dans $E$ si et seulement si:
\[
\forall \epsilon > 0, \exists N, \text{ telle que }
        \forall p, q > N:\,\, \|u_p - u_q\| < \epsilon.
\]
\end{definition}

\begin{theorem}
Si $(u_n)_{n \in \mathbb{N}}$ est une suite convergente dans $E$, alors
$(u_n)_{n \in \mathbb{N}}$ est de Cauchy.
\end{theorem}

\begin{definition}[Sous suite]
Soient $(u_n)_{n \in \mathbb{N}}$ une suite de $E$ et $(v_m)_{m \in \mathbb{N}}$ une
sous-suite de $(u_n)_{n \in \mathbb{N}}$. Alors:
\begin{itemize}
    \item $v_m = u_{\psi(m)}$.
    \item la fonction $\psi : \mathbb{N} \to \mathbb{N}$ est croissante.
    \item $\psi(n) \geq n$.
\end{itemize}
\end{definition}

\begin{theorem}
Soient $(u_n)_{n \in \mathbb{N}}$ une suite de Cauchy et $\psi : \mathbb{N} \to \mathbb{N}$
une fonction croissant. Si $(u_{\psi(n)})$ est convergente, alors $(u_n)$ est
convergente.
\end{theorem}

\begin{remark}
Les espaces vectoriels:
\begin{itemize}
    \item $(\mathbb{R}^d, \|\cdot\|)$,
    \item $(L^1(]a, b[), \|\cdot\|_1)$ pour $]a, b[$ fini ou infini,
    \item $(L^2(]a, b[), \|\cdot\|_2)$,
    \item $(H^1_0(]a, b[), \|\cdot\|_{H^1_0})$ pour $]a, b[$ fini ou infini,
    \item et $(\mathcal{C}([a, b]), \|\cdot\|_{\mathcal{C}}$ pour $[a, b]$
    fini
\end{itemize}
sont complets.
\end{remark}

\begin{definition}
Soit $E$ un espace vectoriel normé. La série de terme général $(u_n)_{n \in
\mathbb{N}}$, notée $\sum u_n$, est convergente si et seulement si la suite de terme
général $S_n = \sum_{p = 1}^n u_p$ (somme partielle) est convergente.

Par définition, la série $\sum u_n$ est la limite de $(S_n)$.
\end{definition}

\begin{definition}[Espace complet]
Soit $E$ un espace vectoriel normé. $E$ est complet (de Banach) si et seulement
si toute suite de Cauchy est convergente.
\end{definition}

\begin{definition}
Soient $E$ un espace vectoriel normé, muni de la norme $\|\cdot\|$, et
$\sum u_n$ une série de $E$. $\sum u_n$ est absolument convergente si et
seulement si $\sum \|u_n\|$  est convergente.
\end{definition}

\begin{definition}
Soit $E$ un espace vectoriel normé. $E$ est de Banach si et seulement si toute
série absolument convergente est convergente.
\end{definition}

\begin{theorem}
Soient $E$ un espace vectoriel normé et $F$ de Banach, alors
$\mathcal{L}(E, F)$ est un Banach.
\end{theorem}

\begin{remark}
Soient $A \in \mathcal{L}(E, F)$ et $u \in E$. On a:
\[
|Au|_F \leq \|A\|_{\mathcal{L}(E, F)} \cdot \|u\|_{E}.
\]
$\|A\|_{\mathcal{L}(E, F)}$ est la plus petite constante $c$ telle que
$|Au|_F \leq c \cdot \|u\|_{E}.$
\end{remark}

\begin{remark}
Soient $A: F \to G$ et $B: E \to F$. On a:
\[
\|AB\|_{\mathcal{L}(E, G)} \leq
    \|A\|_{\mathcal{L}(F, G)} \cdot \|B\|_{\mathcal{L}(E, F)}.
\]

Si $E = F = G$, on a:
\[
\|AB\|_{\mathcal{L}(E)} \leq
    \|A\|_{\mathcal{L}(E)} \cdot \|B\|_{\mathcal{L}(E)}.
\]
\end{remark}

\begin{theorem}[Théorème du point fixe]
Soient $E$ un Banach et $J: E \to E$ une application contractante ($\exists
\rho < 1, \|J(u) - J(v)\| \leq \rho \|u - v\|$ ). Alors, $J$ admet un
unique point fixe:
\[\exists !u_0 \in E \text{ telle que } J(u_0) = u_0.\]
\end{theorem}

\begin{proof}
~\begin{itemize}[$\blacksquare$]
    \item Unicité. Démonstration par l'absurde.

Il existe $u_0$ et $u_1$ telle que $J(u_0) = u_0$ et $J(u_1) = u_1$. Donc
\[
J(u_0) - J(u_1) = u_0 - u_1
\]
mais $J$ est continue, donc il existe $\rho > 0$ telle que:
\[
\begin{aligned}
& \|u_0 - u_1\| = \|J(u_0) - J(u_1)\| \leq \rho \|u_0 - u_1\| \\
\Leftrightarrow &
    \underbrace{(1 - \rho)}_{ < 0}
    \underbrace{\|u_0 - u_1\|}_{ \geq 0} \geq 0 \\
\end{aligned}
\]

Donc forcement $\|u_0 - u_1\| = 0 \implies u_0 - u_1 = 0 \implies u_0 = u_1$.

    \item Existence.

Soit $v_0 \in E$ quelconque. On définit une suite $v_n$ par:
$v_n = J(v_{n -  1})$.
\[
\begin{aligned}
\|v_n - v_{n - 1}\| & = \|J(v_{n - 1}) - J(v_{n - 2})\| \\
& \leq \rho \|v_{n - 1} - v_{n - 2}\| \\
& \leq \rho^2 \|v_{n - 2} - v_{n - 3}\| \\
& \vdots \\
& \leq \rho^{n - 1} \|v_1 - v_0\|
\end{aligned}
\]
Donc $\|v_n - v_{n - 1}\| \leq \rho^{n - 1} \|v_1 - v_0\|$.

Soit $p, q \in \mathbb{N}, p \geq q$:
\[
\begin{aligned}
\|v_p - v_q\| & \leq
    \|v_p - v_{p - 1}\| + \cdots + \|v_{q + 1} - v_q\| \\
& \leq (\rho^{p - 1} + \cdots + \rho^q) \|v_1 - v_0\| \\
& = \rho^q (1 + \rho + \cdots + \rho^{p - 1 - q}) \|v_1 - v_0\| \\
& \leq \rho^q (1 + \rho + \rho^2 + \cdots) \|v_1 - v_0\|
\end{aligned}
\]
Donc:
\[\|v_p - v_q\| \leq \frac{\rho^q}{1 - \rho} \|v_1 - v_0\|\]
Donc $(v_n)$ est une suite de Cauchy et $\lim v_n = v_0$.

Comme $J$ est continue, on a:
\[
\begin{aligned}
v_n & \quad = & J(v_{n - 1}) \\
\downarrow & & \downarrow ~~\\
v_0 & & J(v_0)
\end{aligned}
\]
Donc $J(v_0) = v_0$, d'où l'existence de $v_0$.
\end{itemize}

\end{proof}

\begin{theorem}[Théorème de Neumann]
Soient $E$ un Banach et $A \in \mathcal{L}(E)$ une application linéaire
continue avec $\|A\|_{\mathcal{L}(E)} < 1$. Alors:
\begin{itemize}
    \item $(I - A)$ est inversible.
    \item la série $\sum_{n = 0}^{\infty} A^n$ converge et on a:
\[
(I - A)^{-1} = \displaystyle \sum^{\infty}_{n = 0} A^n.
\]
    \item On a:
\[
\|(I - A)^{-1}\|_{\mathcal{L}(E)} \leq
    \frac{1}{1 - \|A\|_{\mathcal{L}(E)}}.
\]
\end{itemize}
\end{theorem}

\begin{example}
Soit l'équation différentielle suivante:
\[
\begin{cases}
-u''(x) + \alpha u(x) = f(x) \\
u(0) = u(1) = 0
\end{cases}
\]

On peut la réécrire comme:
\[
-u''(x) = f(x) - \alpha u(x)
\]
d'où:
\[
u(x) = \int_0^1 k(x, s)(f(s) - \alpha u(s))\dx[s] = (K(f - \alpha u))(x)
\]
Donc:
\[
u(x) + \alpha \int_0^1 k(x, s)u(s)\dx[s] = \int_0^1 k(x, s) f(s) \dx[s]
\]

Cette équation permet une solution unique si et seulement si la rigidité
$\alpha$ vérifie:
\[
0 < \alpha < \frac{1}{\|k\|_2}.
\]
\end{example}

% kate: default-dictionary fr_FR;
