# Sup Galilée Lecture Notes

These are a few lecture notes from my time at Sup Galilée at Paris 13 University.
They're largely incomplete and kept for prosperity.

## Lecture Notes

* **Functional Analysis**: Introduction to Functional Analysis.
* **Distributions**: Introduction to Distributions.
* **Probabilities**: Introduction to Probabilities.

## Cheatsheets

Some exam cheatsheets with a random assortment of formulas and theorems.

* **Finance de marché**: A small introduction to some finance vocabulary.
* **Probabilité I**: Integration theory, probabilities, distributions etc.
* **Probabilité II**: Markov chains, martingales, etc.
* **Statistique**: Estimators, confidance intervals, hypothesis tests, etc.

## Exams

* **Distributions**: Both the midterm and the final exam.
* **Finite Volumes**: Final exam.

## License

Not sure to what extent I'm allowed to put a license on these things, but as
far as I'm concerned, they're public under the `CC0-1.0` license that can be
found in the repository.
